package com.moneydance.modules.features.filedisplay;

import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.moneydance.awt.AwtUtil;
import com.moneydance.awt.JDateField;
import com.moneydance.util.CustomDateFormat;

/** Window used for Account List interface
  ------------------------------------------------------------------------
*/

public class FileDisplayWindow 
  extends JFrame
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Main extension;
    private JDateField txtStartDate;
  private JDateField txtEndDate;
  private JCheckBox includeBankAccounts;
  private JCheckBox includeInvestments;
  private JCheckBox includeAssets;
  private JCheckBox includeCreditCards;
  private JCheckBox includeLiabilities;
  private JCheckBox includeLoans;
  private JCheckBox includeSecurities;
  private JCheckBox includeIncomecat;
  private JCheckBox includeExpensecat;
  private JCheckBox includeAddressBook;
  private JCheckBox includeAccountBook;
  private JCheckBox includeBudgets;
  private JCheckBox includeCurrencies;
  private JCheckBox includeMemorizedItems;
  private JCheckBox includeReminders;
  private JRadioButton includeNoTrans;
  private JRadioButton includeAllTrans;
  private JRadioButton includeTransbyAccounts; 
  private JButton generateButton;
  private JButton closeButton;
  private JPanel panApp;
  private JButton transButton;
  private JPanel panSelect;
  private JPanel panTree;
  private JPanel panButtons;
  private DetailedFileDisplayWindow objDFDW;

  public FileDisplayWindow(Main extension) {
    super("File Display");
    setResizable(false);
    this.extension = extension;
    GridBagConstraints con = new GridBagConstraints();
    panApp = new JPanel(new BorderLayout());

    panSelect = new JPanel(new GridBagLayout());
    
   
    int row = 0;
    con.gridx=0;
    con.gridy = row;
    con.gridwidth=1;
    con.gridheight=1;
    con.fill = GridBagConstraints.HORIZONTAL;
    con.anchor = GridBagConstraints.LINE_START;
    con.insets = new Insets(0,0,0,0);
    // Accounts 
    JLabel lblAccountsName = new JLabel("Accounts to include:");
    panSelect.add(lblAccountsName,con);
    row++;

    // Include Bank Accounts
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncBank = new JLabel("Bank Accounts");
    panSelect.add(lblIncBank,con);
    con.gridx = 2;
    includeBankAccounts = new JCheckBox();
    panSelect.add(includeBankAccounts,con);
    row++;
    // Include Investments Accounts
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncInv = new JLabel("Investment Accounts");
    panSelect.add(lblIncInv,con);
    con.gridx = 2;
    includeInvestments = new JCheckBox();
    panSelect.add(includeInvestments,con);
    row++;
    // Include Assets
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncAsset= new JLabel("Assets");
    panSelect.add(lblIncAsset,con);
    con.gridx = 2;
    includeAssets = new JCheckBox();
    panSelect.add(includeAssets,con);
    row++;
    // Include Credit Cards
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncCCard = new JLabel("Credit Cards");
    panSelect.add(lblIncCCard,con);
    con.gridx = 2;
    includeCreditCards = new JCheckBox();
    panSelect.add(includeCreditCards,con);
    row++;
    // Include Liabilities
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncLiab = new JLabel("Liabilties");
    panSelect.add(lblIncLiab,con);
    con.gridx = 2;
    includeLiabilities = new JCheckBox();
    panSelect.add(includeLiabilities,con);
    row++;

    // Include Loans
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncLoan = new JLabel("Loans");
    panSelect.add(lblIncLoan,con);
    con.gridx = 2;
    includeLoans = new JCheckBox();
    panSelect.add(includeLoans,con);
    row++;
     
    // Categories 
    con.gridx=0;
    con.gridy = row;
    JLabel lblCategoryName = new JLabel("Categories to include:");
    panSelect.add(lblCategoryName,con);
    row++;

    // Include Income
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncInc = new JLabel("Income");
    panSelect.add(lblIncInc,con);
    con.gridx = 2;
    includeIncomecat = new JCheckBox();
    panSelect.add(includeIncomecat,con);
    row++;
    // Include Expense
    con.gridx = 1;
    con.gridy = row;
    JLabel lblIncExp = new JLabel("Expenses");
    panSelect.add(lblIncExp,con);
    con.gridx = 2;
    includeExpensecat = new JCheckBox();
    panSelect.add(includeExpensecat,con);
    row++;
    // Extras
    row = 0;
    con.gridx = 3;
    con.gridy = row;
    JLabel lblExtrasName = new JLabel("Extras to include:");
    panSelect.add(lblExtrasName,con);
    row++;

    // Include Address Book
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncAddress = new JLabel("Address Book");
    panSelect.add(lblIncAddress,con);
    con.gridx = 5;
    includeAddressBook = new JCheckBox();
    panSelect.add(includeAddressBook,con);
    row++;

    // Include Account Book
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncAccountBook = new JLabel("Account Book");
    panSelect.add(lblIncAccountBook,con);
    con.gridx = 5;
    includeAccountBook = new JCheckBox();
    panSelect.add(includeAccountBook,con);
    row++;

    // Include Budgets
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncBudget = new JLabel("Budgets");
    panSelect.add(lblIncBudget,con);
    con.gridx = 5;
    includeBudgets = new JCheckBox();
    panSelect.add(includeBudgets,con);
    row++;

    // Include Currencies
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncCurrency = new JLabel("Currencies");
    panSelect.add(lblIncCurrency,con);
    con.gridx = 5;
    includeCurrencies = new JCheckBox();
    panSelect.add(includeCurrencies,con);
    row++;

    // Include Securities
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncSec= new JLabel("Securities");
    panSelect.add(lblIncSec,con);
    con.gridx = 5;
    includeSecurities = new JCheckBox();
    panSelect.add(includeSecurities,con);
    row++;

    // Include Memorable items
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncMemory = new JLabel("Memorized Items");
    panSelect.add(lblIncMemory,con);
    con.gridx = 5;
    includeMemorizedItems = new JCheckBox();
    panSelect.add(includeMemorizedItems,con);
    row++;
    // Include Reminders
    con.gridx = 4;
    con.gridy = row;
    JLabel lblIncRemind = new JLabel("Reminders");
    panSelect.add(lblIncRemind,con);
    con.gridx = 5;
    includeReminders = new JCheckBox();
    panSelect.add(includeReminders,con);
    row++;
    // Transactions
    row = 0;
    con.gridx = 6;
    con.gridy = row;
    JLabel lblTransName = new JLabel("Include Transactions:");
    panSelect.add(lblTransName,con);
    row++;
    // Include none
    con.gridx = 7;
    con.gridy = row;
    includeNoTrans = new JRadioButton("None",true);
    panSelect.add(includeNoTrans,con);
    row++;
    // Include By File
    con.gridx = 7;
    con.gridy = row;
    includeAllTrans = new JRadioButton("By File");
    panSelect.add(includeAllTrans,con);
    row++;
    // Include By Account
    con.gridx = 7;
    con.gridy = row;
    includeTransbyAccounts = new JRadioButton("By Account");
    panSelect.add(includeTransbyAccounts,con);
    row++;  
    ButtonGroup group=new ButtonGroup ();
    group.add (includeNoTrans);
    group.add (includeAllTrans);
    group.add(includeTransbyAccounts);
    
    CustomDateFormat cdf = new CustomDateFormat("dd/MM/yyyy");
    // Trans Start Date
    con.gridx = 7;
    con.gridy = row;
    JLabel lblStartDate = new JLabel("Start Date:");
    panSelect.add(lblStartDate,con);
    txtStartDate = new JDateField(cdf);
    txtStartDate.gotoFirstDayInMonth();
    txtStartDate.decrementDate();
    txtStartDate.setEditable(true);
    txtStartDate.gotoFirstDayInMonth();
    txtStartDate.setDisabledTextColor(Color.BLACK);
    con.gridx = 8;
    panSelect.add(txtStartDate,con);
    row++;

    // End Date
    con.gridx = 7;
    con.gridy = row;
    JLabel lblEndDate = new JLabel("End Date:");
    panSelect.add(lblEndDate,con);
    txtEndDate = new com.moneydance.awt.JDateField(cdf);
    txtEndDate.setDisabledTextColor(Color.BLACK);
    txtEndDate.setEditable(true);
    con.gridx = 8;
    panSelect.add(txtEndDate,con);
    row++;
    con.gridx = 7;
    con.gridy = row;
    row++;
    // Buttons
    generateButton = new JButton("Generate");
    generateButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			generate();
		}
	});
    panSelect.add(generateButton, con);
    panSelect.setPreferredSize(new Dimension(1000,300));
    panApp.add(panSelect,BorderLayout.PAGE_START);
    /*
     * Tree pane
     */
    panTree = new JPanel(new BorderLayout());
    objDFDW = new DetailedFileDisplayWindow(
			this.extension,
			includeBankAccounts.isSelected(),
			includeInvestments.isSelected(),
			includeAssets.isSelected(),
			includeCreditCards.isSelected(),
			includeLiabilities.isSelected(),
			includeLoans.isSelected(),
			includeSecurities.isSelected(),
			includeIncomecat.isSelected(),
			includeExpensecat.isSelected(),
			includeAddressBook.isSelected(),
		    includeAccountBook.isSelected(),
		    includeBudgets.isSelected(),
		    includeCurrencies.isSelected(),
		    includeMemorizedItems.isSelected(),
		    includeReminders.isSelected(),
		    includeNoTrans.isSelected(),
		    includeAllTrans.isSelected(),
		    includeTransbyAccounts.isSelected(),
			txtStartDate.getDateInt(),
			txtEndDate.getDateInt()
			);
    panTree.add(objDFDW);
    panTree.setPreferredSize(new Dimension(1000,400));
    panTree.setMinimumSize(new Dimension(800,300));
    panApp.add(panTree,BorderLayout.CENTER);
    /*
     * Button pane
     */
    
    con.gridx = 1;
    con.gridy = 0;
    con.insets = new Insets(5,5,5,5);
    panButtons = new JPanel(new GridBagLayout());
    closeButton = new JButton("Close");
    closeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			FileDisplayWindow.this.extension.closeConsole();
		}
	});
    con.gridx = 1;
    con.gridy = row;
    panButtons.add(closeButton, con);
    transButton = new JButton("View Transactions");
    transButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			objDFDW.viewTrans();
		}
	});
    con.gridx = 2;
    con.gridy = row;
    panButtons.add(transButton, con);
    row++;
    panApp.add(panButtons,BorderLayout.PAGE_END);
    /*
     * Add main panel
     * 
     */
    setSize(new Dimension(1000,800));
    getContentPane().add(panApp);

    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    enableEvents(WindowEvent.WINDOW_CLOSING);
        
    AwtUtil.centerWindow(this);
        
    
  }
  
  /* 
   * Display the detail based on selected options
   */
  protected void generate() {
      //Schedule a job for the event dispatch thread:
      //creating and showing this application's GUI.
	  objDFDW.reset(includeBankAccounts.isSelected(),
			includeInvestments.isSelected(),
			includeAssets.isSelected(),
			includeCreditCards.isSelected(),
			includeLiabilities.isSelected(),
			includeLoans.isSelected(),
			includeSecurities.isSelected(),
			includeIncomecat.isSelected(),
			includeExpensecat.isSelected(),
			includeAddressBook.isSelected(),
		    includeAccountBook.isSelected(),
		    includeBudgets.isSelected(),
		    includeCurrencies.isSelected(),
		    includeMemorizedItems.isSelected(),
		    includeReminders.isSelected(),
		    includeNoTrans.isSelected(),
		    includeAllTrans.isSelected(),
		    includeTransbyAccounts.isSelected(),
			txtStartDate.getDateInt(),
			txtEndDate.getDateInt());
	  panApp.repaint();
  }


  public final void processEvent(AWTEvent evt) {
    if(evt.getID()==WindowEvent.WINDOW_CLOSING) {
      extension.closeConsole();
      return;
    }
    if(evt.getID()==WindowEvent.WINDOW_OPENED) {
    }
    super.processEvent(evt);
  }
  


  void goAway() {
    setVisible(false);
    dispose();
  }
}
