package com.moneydance.modules.features.filedisplay;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionListener;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.event.TreeSelectionEvent;

import com.moneydance.apps.md.model.AbstractTxn;
import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.AccountBook;
import com.moneydance.apps.md.model.AccountBookModel;
import com.moneydance.apps.md.model.AddressBook;
import com.moneydance.apps.md.model.Budget;
import com.moneydance.apps.md.model.BudgetItem;
import com.moneydance.apps.md.model.BudgetList;
import com.moneydance.apps.md.model.CurrencyTable;
import com.moneydance.apps.md.model.CurrencyType;
import com.moneydance.apps.md.model.MemorizedItemManager;
import com.moneydance.apps.md.model.ParentTxn;
import com.moneydance.apps.md.model.Reminder;
import com.moneydance.apps.md.model.ReminderSet;
import com.moneydance.apps.md.model.RootAccount;
import com.moneydance.apps.md.model.Tag;
import com.moneydance.apps.md.model.TagSet;
import com.moneydance.apps.md.model.TransactionSet;
import com.moneydance.apps.md.model.TxnSet;
import com.moneydance.util.CustomDateFormat;
import com.moneydance.util.StreamTable;



/** File Detail.
 * Creates a tree with the chosen options split into three groups
 * The first level of each group is added to the tree
 * A Tree Selection Listener is used to add entries when the user clicks on a leaf
 * 
 * */
public class DetailedFileDisplayWindow extends JPanel
			implements TreeSelectionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    private Main extension;
    private JTree tree;
    private DefaultMutableTreeNode nodeTop;
    private JScrollPane treeView;
    private int startDate;
    private int endDate;
	private boolean includeBankAccounts;
	private boolean includeInvestments;
	private boolean includeAssets;
	private boolean includeCreditCards;
	private boolean includeLiabilities;
	private boolean includeLoans;
	private boolean includeSecurities;
	private boolean includeIncomecat;
	private boolean includeExpensecat;
	private boolean includeAddressBook;
    private boolean includeAccountBook;
    private boolean includeBudgets;
    private boolean includeCurrencies;
    private boolean includeMemorizedItems;
    private boolean includeReminders;
    private boolean includeAllTrans; 
    private boolean includeTransbyAccounts;
    private Account lastAcct = null;
    JScrollPane detailpane = null;
    MyTable detailtable = null;
    DefaultTableModel detailmodel = null;
    MyTranTable trantable = null;
	JTable tabTran = null;
	TransactionSet  txnSet;
    TxnSet tsTrans;
    private Map<Integer, List<Account>> accounts; 
	
	private JButton closeButton;
	private JButton closeTranButton;
	private static final int MINSCROLLPANE = 300;
	private static final int LEVELSCROLLPANE = 50;
	private static final int SCROLLPANEDEPTH = 400;
	
	public static final DecimalFormat CURR_FMT = new DecimalFormat("#,##0");
	public static final DecimalFormat CENTS_FMT = new DecimalFormat("00");
	public static final SimpleDateFormat DT_FMT = new SimpleDateFormat("yyyy-MM-dd");
	/*
	 * Each node on the tree has a user object of myNodeObject
	 * This contains a structure ID which is in the format
	 * gtex
	 * g is the group, i.e. Accounts (1), Categories (2) or Extras (3)
	 * t is the type, i.e. type of account, category or extra
	 * e is tyhe particular entry, i.e. Bank Account, Expense category
	 * x is only used for budget items 
	 * ACCOUNT_INCREMENT is added to the NODE entry to give the ACCOUNT entry
	 * BUDGET_INCREMENT is added to a Budget Entry to give the item
	 */
	
	public static final int BANK_NODE = 1100;
	public static final int BANK_ACCOUNT = 1110;
	public static final int INVESTMENT_NODE = 1200;
	public static final int INVESTMENT_ACCOUNT = 1210;
	public static final int LIABILITY_NODE = 1300;
	public static final int LIABILITY_ACCOUNT = 1310;
	public static final int ASSET_NODE = 1400;
	public static final int ASSET_ACCOUNT = 1410;
	public static final int LOAN_NODE = 1500;
	public static final int LOAN_ACCOUNT = 1510;
	public static final int EXPENSE_NODE = 2100;
	public static final int EXPENSE_ACCOUNT = 2110;
	public static final int INCOME_NODE = 2200;
	public static final int INCOME_ACCOUNT = 2210;
	public static final int CREDIT_CARD_NODE = 1700;
	public static final int CREDIT_CARD_ACCOUNT = 1710;
	public static final int ACCOUNTS_NODE = 1000;
	public static final int CATEGORIES_NODE = 2000;
	public static final int EXTRAS_NODE = 3000;
	public static final int EXTRAS_ADDRESS_NODE = 3100;
	public static final int EXTRAS_ACCOUNT_NODE = 3200;
	public static final int EXTRAS_BUDGET_NODE = 3300;
	public static final int EXTRAS_CURRENCY_NODE = 3400;
	public static final int EXTRAS_MEMORIZED_NODE = 3500;
	public static final int EXTRAS_REMINDER_NODE = 3600;
	public static final int EXTRAS_SECURITY_NODE = 3700;
	public static final int EXTRAS_ADDRESS_ENTRY = 3110;
	public static final int EXTRAS_ACCOUNT_ENTRY = 3210;
	public static final int EXTRAS_BUDGET_ENTRY = 3310;
	public static final int EXTRAS_CURRENCY_ENTRY = 3410;
	public static final int EXTRAS_SECURITY_ENTRY = 3710;
	public static final int EXTRAS_MEMORIZED_GRAPH_ENTRY = 3510;
	public static final int EXTRAS_MEMORIZED_REPORT_ENTRY = 3520;
	public static final int EXTRAS_REMINDER_ENTRY = 3610;
	public static final int ACCOUNT_INCREMENT = 10;
	public static final int BUDGET_INCREMENT = 1;
	public static final int CURRENCY_INCREMENT = 1;
	public static final int SECURITY_INCREMENT = 1;
	/*
	 * Name of table columns
	 */
	public static final String[] columnNames = {"Field","Value"};
	public static final String[] transcolumns = {"ID","Account","Description","Date","Value","Type","P//S","Cheque","Status"};
	/*
	 * Table to link tree nodes back to MoneyDance account type
	 */
    private static final Map<Integer, Integer> nodetypes;
    static
    {
        nodetypes = new HashMap<Integer, Integer>();
        nodetypes.put(BANK_NODE, Account.ACCOUNT_TYPE_BANK);
        nodetypes.put(INVESTMENT_NODE, Account.ACCOUNT_TYPE_INVESTMENT);
        nodetypes.put(LIABILITY_NODE, Account.ACCOUNT_TYPE_LIABILITY);
        nodetypes.put(ASSET_NODE, Account.ACCOUNT_TYPE_ASSET);
        nodetypes.put(LOAN_NODE, Account.ACCOUNT_TYPE_LOAN);
        nodetypes.put(CREDIT_CARD_NODE, Account.ACCOUNT_TYPE_CREDIT_CARD);
        nodetypes.put(EXPENSE_NODE, Account.ACCOUNT_TYPE_EXPENSE);
        nodetypes.put(INCOME_NODE, Account.ACCOUNT_TYPE_INCOME);
        nodetypes.put(BANK_ACCOUNT, Account.ACCOUNT_TYPE_BANK);
        nodetypes.put(INVESTMENT_ACCOUNT, Account.ACCOUNT_TYPE_INVESTMENT);
        nodetypes.put(LIABILITY_ACCOUNT, Account.ACCOUNT_TYPE_LIABILITY);
        nodetypes.put(ASSET_ACCOUNT, Account.ACCOUNT_TYPE_ASSET);
        nodetypes.put(LOAN_ACCOUNT, Account.ACCOUNT_TYPE_LOAN);
        nodetypes.put(CREDIT_CARD_ACCOUNT, Account.ACCOUNT_TYPE_CREDIT_CARD);
        nodetypes.put(INCOME_ACCOUNT, Account.ACCOUNT_TYPE_INCOME);
        nodetypes.put(EXPENSE_ACCOUNT, Account.ACCOUNT_TYPE_EXPENSE);
        nodetypes.put(ACCOUNTS_NODE, 0);
        nodetypes.put(CATEGORIES_NODE, 0);
        nodetypes.put(EXTRAS_NODE, 0);
    }
    /*
     * Table to link MoneyDance account types to a string
     */
    private static final Map<Integer, String> accounttypes;
    static
    {
        accounttypes = new HashMap<Integer, String>();
        accounttypes.put(Account.ACCOUNT_TYPE_BANK,"Bank");
        accounttypes.put(Account.ACCOUNT_TYPE_INVESTMENT,"Investment");
        accounttypes.put(Account.ACCOUNT_TYPE_LIABILITY,"Liability");
        accounttypes.put(Account.ACCOUNT_TYPE_ASSET,"Asset");
        accounttypes.put(Account.ACCOUNT_TYPE_LOAN,"Loan");
        accounttypes.put(Account.ACCOUNT_TYPE_SECURITY,"Security");
        accounttypes.put(Account.ACCOUNT_TYPE_CREDIT_CARD,"Credit Card");
    }
    /*
     * Table to link budget intervals to a string
     */
    private static final Map<Integer, String> intervaltypes;
    static
    {
    		intervaltypes = new HashMap<Integer,String>();
		 	intervaltypes.put (BudgetItem.INTERVAL_ANNUALLY,"Annually"); 
			intervaltypes.put (BudgetItem.INTERVAL_BI_MONTHLY,"Bi-Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_BI_WEEKLY,"Weekly"); 
			intervaltypes.put (BudgetItem.INTERVAL_DAILY,"Daily"); 
			intervaltypes.put (BudgetItem.INTERVAL_MONTHLY,"Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_NO_REPEAT,"No Repeat");
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_ANNUALLY,"Once Annually "); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_BI_MONTHLY,"Once Bi-Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_BI_WEEKLY,"Once Bi-Weekly "); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_MONTHLY,"Once Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_SEMI_ANNUALLY,"Once Semi-Annually"); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_SEMI_MONTHLY,"Once Semi-Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_TRI_MONTHLY,"Once Tri-Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_TRI_WEEKLY,"Once Tri-Weekly"); 
			intervaltypes.put (BudgetItem.INTERVAL_ONCE_WEEKLY,"Once Weekly"); 
			intervaltypes.put (BudgetItem.INTERVAL_SEMI_ANNUALLY,"Semi-Annually"); 
			intervaltypes.put (BudgetItem.INTERVAL_SEMI_MONTHLY,"Semi-Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_TRI_MONTHLY,"Tri-Monthly"); 
			intervaltypes.put (BudgetItem.INTERVAL_TRI_WEEKLY,"Tri-Weekly"); 
			intervaltypes.put (BudgetItem.INTERVAL_WEEKLY,"Weekly");
    }
	public static final int START_DAY_OF_WEEK = Calendar.MONDAY;
	
     
	
	// -------------------------------------------
    // Private Classes
	// -------------------------------------------
	/**
	 */
	public DetailedFileDisplayWindow(Main extension,
			boolean includeBankAccountsp,boolean includeInvestmentsp,
			boolean includeAssetsp,boolean includeCreditCardsp,
			boolean includeLiabilitiesp,boolean includeLoansp,
			boolean includeSecuritiesp,boolean includeIncomecatp,
			boolean includeExpensecatp,boolean includeAddressBookp,
		    boolean includeAccountBookp,boolean includeBudgetsp,
		    boolean includeCurrenciesp,boolean includeMemorizedItemsp,
		    boolean includeRemindersp,boolean includeNoTransp,
		    boolean includeAllTransp, boolean includeTransbyAccountsp,
			int txtStartDatep,
			int txtEndDatep) {
		super(new GridBagLayout());
	    this.extension = extension;
		this.includeBankAccounts=includeBankAccountsp;
		this.includeInvestments=includeInvestmentsp;
		this.includeAssets=includeAssetsp;
		this.includeCreditCards=includeCreditCardsp;
		this.includeLiabilities=includeLiabilitiesp;
		this.includeLoans=includeLoansp;
		this.includeSecurities=includeSecuritiesp;
		this.includeIncomecat=includeIncomecatp;
		this.includeExpensecat=includeExpensecatp;
		this.includeAddressBook=includeAddressBookp;
	    this.includeAccountBook=includeAccountBookp;
	    this.includeBudgets=includeBudgetsp;
	    this.includeCurrencies=includeCurrenciesp;
	    this.includeMemorizedItems=includeMemorizedItemsp;
	    this.includeReminders=includeRemindersp;
	    this.includeAllTrans=includeAllTransp; 
	    this.includeTransbyAccounts=includeTransbyAccountsp;
	    this.startDate = txtStartDatep;
	    this.endDate = txtEndDatep;    
	    this.setBorder(new EmptyBorder(5,5,5,5));

	       //Create the nodes.
        nodeTop =
            new DefaultMutableTreeNode("MoneyDance File");
        createNodes(nodeTop);

        //Create a tree that allows one selection at a time.
        tree = new JTree(nodeTop);
        tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);

        //Listen for when the selection changes.
        tree.addTreeSelectionListener(this);
 

        //Create the scroll pane and add the tree to it. 
        treeView = new JScrollPane();
        treeView.setViewportView(tree);
        treeView.setPreferredSize(new Dimension(MINSCROLLPANE, SCROLLPANEDEPTH));
        GridBagConstraints con = new GridBagConstraints ();
        con.gridx = 0;
        con.gridy = 0;
        con.gridheight = 1;
        con.gridwidth = 4;
        con.fill = java.awt.GridBagConstraints.HORIZONTAL;
        this.add(treeView,con);
        // Create the detail table and pane, then hide
        detailtable = new MyTable(new DefaultTableModel(columnNames,0));
        detailmodel = (DefaultTableModel) detailtable.getModel();
        detailpane = new JScrollPane(detailtable);
        detailpane.setPreferredSize(new Dimension(650,SCROLLPANEDEPTH));
        con.gridx = 5;
        con.gridwidth = 1;
        this.add(detailpane,con);
 
	}
	
	public void reset(boolean includeBankAccountsp,boolean includeInvestmentsp,
			boolean includeAssetsp,boolean includeCreditCardsp,
			boolean includeLiabilitiesp,boolean includeLoansp,
			boolean includeSecuritiesp,boolean includeIncomecatp,
			boolean includeExpensecatp,boolean includeAddressBookp,
		    boolean includeAccountBookp,boolean includeBudgetsp,
		    boolean includeCurrenciesp,boolean includeMemorizedItemsp,
		    boolean includeRemindersp,boolean includeNoTransp,
		    boolean includeAllTransp, boolean includeTransbyAccountsp,
			int txtStartDatep,
			int txtEndDatep) {
		this.includeBankAccounts=includeBankAccountsp;
		this.includeInvestments=includeInvestmentsp;
		this.includeAssets=includeAssetsp;
		this.includeCreditCards=includeCreditCardsp;
		this.includeLiabilities=includeLiabilitiesp;
		this.includeLoans=includeLoansp;
		this.includeSecurities=includeSecuritiesp;
		this.includeIncomecat=includeIncomecatp;
		this.includeExpensecat=includeExpensecatp;
		this.includeAddressBook=includeAddressBookp;
	    this.includeAccountBook=includeAccountBookp;
	    this.includeBudgets=includeBudgetsp;
	    this.includeCurrencies=includeCurrenciesp;
	    this.includeMemorizedItems=includeMemorizedItemsp;
	    this.includeReminders=includeRemindersp;
	    this.includeAllTrans=includeAllTransp; 
	    this.includeTransbyAccounts=includeTransbyAccountsp;
	    this.startDate = txtStartDatep;
	    this.endDate = txtEndDatep;
        nodeTop =
                new DefaultMutableTreeNode("MoneyDance File");
       createNodes(nodeTop);

        //Create a tree that allows one selection at a time.
        tree = new JTree(nodeTop);
        tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);

        //Listen for when the selection changes.
        tree.addTreeSelectionListener(this);
        treeView.setViewportView(tree);
     	    
	}

	   /** Required by TreeSelectionListener interface. */
    public void valueChanged(TreeSelectionEvent e) {
    	DefaultMutableTreeNode dataitem;
    	Integer nodeid;
    	String nodepattern;
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                           tree.getLastSelectedPathComponent();

        if (node == null) return;
        treeView.setPreferredSize(new Dimension(MINSCROLLPANE+ node.getLevel()*LEVELSCROLLPANE, SCROLLPANEDEPTH));
        Object nodeInfo = node.getUserObject();
        lastAcct = null;
        if (node.isLeaf()){
        	// not expanded
        	myNodeObject userobj = (myNodeObject) nodeInfo;
        	if (userobj != null) {
        		nodeid = userobj.getnodetype();
        		nodepattern = nodeid.toString();
        		/*
        		 * Is the node clicked on an Account (1x0x) or a Category (2x0x)
        		 */
         		if (nodepattern.matches("[1-2]\\d0\\d")) {
         			/*
         			 * Add first level of accounts to tree
         			 */
        			List<Account> l = accounts.get(nodetypes.get(nodeid));
        			if (l == null)
        				return;
        			for (int i=0;i< l.size();i++) {
        				myNodeObject mynode = new myNodeObject (nodeid+ACCOUNT_INCREMENT,l.get(i).getAccountName());
        				mynode.setaccount(l.get(i));
        	        	dataitem = new DefaultMutableTreeNode(mynode);
        				node.add(dataitem);
        			}
                    JTree tree = (JTree)e.getSource();
                    TreePath path = tree.getSelectionPath();
                    tree.expandPath(path);
        		}
         		/*
         		 * Is the node clicked on a specific account (1x1x) or category (2x1x)
         		 */
         		if (nodepattern.matches("[1-2]\\d1\\d")) {
         			displayaccount(userobj, nodeid, node, e);
        		}
         		/*
         		 * Is the node clicked on an Extra (3x0x)
         		 */
         		if (nodepattern.matches("3\\d00")){
         			displayextrasentries (userobj, nodeid, node, e);
         		}
         		/*
         		 * Is the node clicked on a specific Extra entry (3x1x or 3x2x)
         		 */
         		if (nodepattern.matches("3\\d[1-2]0")){
         			displayextras (userobj, nodeid, node, e);
         			
         		}
         		/*
         		 * Is the node clicked on a budget item (33x1)
         		 */

         		if (nodepattern.matches("33\\d1")){
        			displaybudgetitem (userobj, nodeid, node, e);         			
         		}
         		/*
         		 * Is the node clicked on a currency item (34x1) or
         		 * security item (37x1)
         		 */

         		if (nodepattern.matches("34\\d1")){
        			displaycurrencyitem (userobj, nodeid, node, e);         			
         		}
         		if (nodepattern.matches("37\\d1")){
        			displaycurrencyitem (userobj, nodeid, node, e);         			
         		}
        	}
        }
        else {
        	myNodeObject userobj = (myNodeObject) nodeInfo;
        	if (userobj != null) {
        		nodeid = userobj.getnodetype();
        		if (nodeid == EXTRAS_BUDGET_ENTRY){
           			displayextras(userobj, nodeid, node, e);         			
        		}	
        		if (nodeid == EXTRAS_CURRENCY_ENTRY){
           			displayextras(userobj, nodeid, node, e);         			
        		}	
        		if (nodeid == EXTRAS_SECURITY_ENTRY){
           			displayextras(userobj, nodeid, node, e);         			
        		}	
        	}
        	// already expanded
        }

    }
    /*
     * adds the data from a specific account to the table
     */
    private void displayaccount(myNodeObject userobj, Integer nodeid, DefaultMutableTreeNode node,TreeSelectionEvent e) {
    	DefaultMutableTreeNode dataitem;
    	detailmodel.setRowCount(0);
		Account acct = userobj.getaccount();
		lastAcct = acct;
		Vector<String> vname = new Vector<String>();
		vname.add("Name");
		vname.add(acct.getAccountName()+" ");
		detailmodel.addRow(vname);
		Vector<String> vfname = new Vector<String>();
		vfname.add("Full Account Name");
		vfname.add(acct.getFullAccountName()+" ");
		detailmodel.addRow(vfname);
		Vector<String> vnumber = new Vector<String>();
		vnumber.add("Number");
		vnumber.add(acct.getAccountNum()+" ");
		detailmodel.addRow(vnumber);
		Vector<String> vtype = new Vector<String>();
		vtype.add("Type");
		vtype.add(accounttypes.get(acct.getAccountType())+" ");
		detailmodel.addRow(vtype);
		Vector<String> vinactive = new Vector<String>();
		vinactive.add("Inactive");
		vinactive.add(acct.getAccountIsInactive()+" ");
		detailmodel.addRow(vinactive);
		Vector<String> vcurrency = new Vector<String>();
		vcurrency.add("Currency Type");
		CurrencyType ctype = acct.getCurrencyType();
		vcurrency.add(ctype+" ");
		detailmodel.addRow(vcurrency);
		Vector<String> vbalance = new Vector<String>();
		vbalance.add("Balance");
		vbalance.add(ctype.formatFancy(acct.getBalance(),'.')+" ");
		detailmodel.addRow(vbalance);
		Vector<String> vclbalance = new Vector<String>();
		vclbalance.add("Cleared Balance");
		vclbalance.add(ctype.formatFancy(acct.getClearedBalance(),'.')+" ");
		detailmodel.addRow(vclbalance);
		Vector<String> vcnbalance = new Vector<String>();
		vcnbalance.add("Confirmed Balance");
		vcnbalance.add(ctype.formatFancy(acct.getConfirmedBalance(),'.')+" ");
		detailmodel.addRow(vcnbalance);
		Vector<String> vcubalance = new Vector<String>();
		vcubalance.add("Current Balance");
		vcubalance.add(ctype.formatFancy(acct.getCurrentBalance(),'.')+" ");
		detailmodel.addRow(vcubalance);
		Vector<String> vdate = new Vector<String>();
		vdate.add("Creation Date");
		CustomDateFormat cdate = new CustomDateFormat ("DD/MM/YYYY");
		vdate.add(cdate.format(acct.getCreationDateInt())+" ");
		detailmodel.addRow(vdate);
		Vector<String> vcattype = new Vector<String>();
		vcattype.add("Default Category");
		try {
			vcattype.add(acct.getDefaultCategory().getAccountName()+" ");
		}
		catch (Exception ecattype) {
			vcattype.add("None");
		}
		detailmodel.addRow(vcattype);
		Vector<String> vbankfi = new Vector<String>();
		vbankfi.add("OFX Connection Bank");
		vbankfi.add(acct.getBankingFI()+" ");
		detailmodel.addRow(vbankfi);
		Vector<String> vbillpayfi = new Vector<String>();
		vbillpayfi.add("OFX Bill to Bank");
		vbillpayfi.add(acct.getBillPayFI()+" ");
		detailmodel.addRow(vbillpayfi);
		Vector<String> vcomment = new Vector<String>();
		vcomment.add("Comment");
		vcomment.add(acct.getComment()+" ");
		detailmodel.addRow(vcomment);
		Vector<String> vhide = new Vector<String>();
		vhide.add("Hide on Home Page");
		vhide.add(acct.getHideOnHomePage()+" ");
		detailmodel.addRow(vhide);
		Vector<String> vofxacct = new Vector<String>();
		vofxacct.add("OFX Account Key");
		vofxacct.add(acct.getOFXAccountKey()+" ");
		detailmodel.addRow(vofxacct);
		Vector<String> vofxmsg = new Vector<String>();
		vofxmsg.add("OFX Message Type");
		vofxmsg.add(acct.getOFXAccountMsgType()+" ");
		detailmodel.addRow(vofxmsg);
		Vector<String> vofxacctnum = new Vector<String>();
		vofxacctnum.add("OFX Account Number");
		vofxacctnum.add(acct.getOFXAccountNumber()+" ");
		detailmodel.addRow(vofxacctnum);
		Vector<String> vofxaccttype = new Vector<String>();
		vofxaccttype.add("OFX Account Type");
		vofxaccttype.add(acct.getOFXAccountType()+" ");
		detailmodel.addRow(vofxaccttype);
		Vector<String> vofxbank = new Vector<String>();
		vofxbank.add("OFX Bank ID");
		vofxbank.add(acct.getOFXBankID()+" ");
		detailmodel.addRow(vofxbank);
		Vector<String> vofxbillpayacct = new Vector<String>();
		vofxbillpayacct.add("OFX Bill Pay Bank Account");
		vofxbillpayacct.add(acct.getOFXBillPayAccountNumber()+" ");
		detailmodel.addRow(vofxbillpayacct);
		Vector<String> vofxbillpayaccttype = new Vector<String>();
		vofxbillpayaccttype.add("OFX Bill Pay Bank Account");
		vofxbillpayaccttype.add(acct.getOFXBillPayAccountType()+" ");
		detailmodel.addRow(vofxbillpayaccttype);
		Vector<String> vonlavail = new Vector<String>();
		vonlavail.add("Online Available Balance");
		vonlavail.add(ctype.formatFancy(acct.getOnlineAvailBalance(),'.')+" ");
		detailmodel.addRow(vonlavail);
		Vector<String> vrecbal = new Vector<String>();
		vrecbal.add("Recursive Balance");
		vrecbal.add(ctype.formatFancy(acct.getRecursiveBalance(),'.')+" ");
		detailmodel.addRow(vrecbal);
		Vector<String> vreccbal = new Vector<String>();
		vreccbal.add("Recursive Cleared Balance");
		vreccbal.add(ctype.formatFancy(acct.getRecursiveClearedBalance(),'.')+" ");
		detailmodel.addRow(vreccbal);
		Vector<String> vreccubal = new Vector<String>();
		vreccubal.add("Recursive Current Balance");
		vreccubal.add(ctype.formatFancy(acct.getRecursiveCurrentBalance(),'.')+" ");
		detailmodel.addRow(vreccubal);
		Vector<String> vrecrbal = new Vector<String>();
		vrecrbal.add("Recursive Reconciling Balance");
		vrecrbal.add(ctype.formatFancy(acct.getRecursiveReconcilingBalance(),'.')+" ");
		detailmodel.addRow(vrecrbal);
		Vector<String> vrecsbal = new Vector<String>();
		vrecsbal.add("Recursive Start Balance");
		vrecsbal.add(ctype.formatFancy(acct.getRecursiveStartBalance(),'.')+" ");
		detailmodel.addRow(vrecsbal);
		Vector<String> vrecubal = new Vector<String>();
		vrecubal.add("Recursive User Balance");
		vrecubal.add(ctype.formatFancy(acct.getRecursiveUserBalance(),'.')+" ");
		detailmodel.addRow(vrecubal);
		Vector<String> vrecucbal = new Vector<String>();
		vrecucbal.add("Recursive User Cleared Balance");
		vrecucbal.add(ctype.formatFancy(acct.getRecursiveUserClearedBalance(),'.')+" ");
		detailmodel.addRow(vrecucbal);
		Vector<String> vrecucubal = new Vector<String>();
		vrecucubal.add("Recursive User Current Balance");
		vrecucubal.add(ctype.formatFancy(acct.getRecursiveUserCurrentBalance(),'.')+" ");
		detailmodel.addRow(vrecucubal);
		Vector<String> vrecurbal = new Vector<String>();
		vrecurbal.add("Recursive User Reconciling Balance");
		vrecurbal.add(ctype.formatFancy(acct.getRecursiveUserReconcilingBalance(),'.')+" ");
		detailmodel.addRow(vrecurbal);
		Vector<String> vrecusbal = new Vector<String>();
		vrecusbal.add("Recursive User Start Balance");
		vrecusbal.add(ctype.formatFancy(acct.getRecursiveUserStartBalance(),'.')+" ");
		detailmodel.addRow(vrecusbal);
		Vector<String> vstartbal = new Vector<String>();
		vstartbal.add("Start  Balance");
		vstartbal.add(ctype.formatFancy(acct.getStartBalance(),'.')+" ");
		detailmodel.addRow(vstartbal);
		Vector<String> vubal = new Vector<String>();
		vubal.add("User Balance");
		vubal.add(ctype.formatFancy(acct.getUserBalance(),'.')+" ");
		detailmodel.addRow(vubal);
		Vector<String> vucbal = new Vector<String>();
		vucbal.add("User Cleared Balance");
		vucbal.add(ctype.formatFancy(acct.getUserClearedBalance(),'.')+" ");
		detailmodel.addRow(vucbal);
		Vector<String> vucubal = new Vector<String>();
		vucubal.add("User Current Balance");
		vucubal.add(ctype.formatFancy(acct.getUserCurrentBalance(),'.')+" ");
		detailmodel.addRow(vucubal);
		Vector<String> vurbal = new Vector<String>();
		vurbal.add("User Reconciling Balance");
		vurbal.add(ctype.formatFancy(acct.getUserReconcilingBalance(),'.')+" ");
		detailmodel.addRow(vurbal);
		Vector<String> vusbal = new Vector<String>();
		vusbal.add("User Start Balance");
		vusbal.add(ctype.formatFancy(acct.getUserStartBalance(),'.')+" ");
		detailmodel.addRow(vusbal);
		/*
		 * determine if there are any subaccounts, add them to the tree
		 */
		int nosubaccts = acct.getSubAccountCount();
		if (nosubaccts > 0) {
			for (int i=0;i<nosubaccts;i++) {
			myNodeObject mynode = new myNodeObject (nodeid,acct.getSubAccount(i).getAccountName());
			mynode.setaccount(acct.getSubAccount(i));
        	dataitem = new DefaultMutableTreeNode(mynode);
			node.add(dataitem);
			}
		/*
		 * expand the tree at the clicked leaf just in case there are sub-accounts
		 */
        JTree tree = (JTree)e.getSource();
        TreePath path = tree.getSelectionPath();
        tree.expandPath(path);              					   				
		}
		/*
		 * tell the table that it has changed
		 */
		detailmodel.fireTableDataChanged();
    }
    /*
     * add the entries for the selected extra to the tree, this can be called whether it is 
     * a leaf or not so need to check so not adding twice
     */
    private void displayextrasentries(myNodeObject userobj, Integer nodeid, DefaultMutableTreeNode node,TreeSelectionEvent e) {
    	DefaultMutableTreeNode dataitem;
	    RootAccount root = this.extension.getUnprotectedContext().getRootAccount();
       	String nodepattern = nodeid.toString();
       	/*
       	 * determine which extra has been selected (3x00)
       	 *  
       	 */
       	switch (nodepattern.charAt(1)) {
       	case '1' :
       		// list addresses
        	AddressBook lab = root.getAddressBook();
     		int labc = lab.getSize();
     		if (labc > 0) {
     			for (int i=0;i<labc;i++) {
       				myNodeObject mynode = new myNodeObject (EXTRAS_ADDRESS_ENTRY,lab.getEntry(i).getName());
    				mynode.setobject(lab.getEntry(i));
    	        	dataitem = new DefaultMutableTreeNode(mynode);
    				node.add(dataitem);  				
     			}
     		}    		
       		break;
       	case '2' :
       		// list account books
        	List<AccountBook> lac = AccountBookModel.getAccountBooks();
     		int lacc = lac.size();
     		if (lacc > 0) {
     			for (int i=0;i<lacc;i++) {
       				myNodeObject mynode = new myNodeObject (EXTRAS_ACCOUNT_ENTRY,lac.get(i).getName());
    				mynode.setobject(lac.get(i));
    	        	dataitem = new DefaultMutableTreeNode(mynode);
    				node.add(dataitem);  				
     			}
     		}    		
       		break;
       	case '3' :
       		// list budgets
        	BudgetList lb = root.getBudgetList();
     		int lbc = lb.getBudgetCount();
     		if (lbc > 0) {
     			for (int i=0;i<lbc;i++) {
       				myNodeObject mynode = new myNodeObject (EXTRAS_BUDGET_ENTRY,lb.getBudget(i).getName());
    				mynode.setobject(lb.getBudget(i));
    	        	dataitem = new DefaultMutableTreeNode(mynode);
    				node.add(dataitem);  				
     			}
     		}    		
       		break;
       	case '4' :
       		// list currencies
         	CurrencyType[] lct = root.getCurrencyTable().getAllCurrencies();
     		int lctc = lct.length;
     		if (node.isLeaf()) {
	     		if (lctc > 0) {
	     			for (int i=0;i<lctc;i++) {
	     				if (lct[i].getCurrencyType() == CurrencyType.CURRTYPE_CURRENCY) {
	     					myNodeObject mynode = new myNodeObject (EXTRAS_CURRENCY_ENTRY,lct[i].getName());
	     					mynode.setobject(lct[i]);
	     					dataitem = new DefaultMutableTreeNode(mynode);
	     					node.add(dataitem);
	     				}
	     			}
	     		}
     		}
       		break;
       	case '5' :
       		// list memorized items
        	MemorizedItemManager lm = root.getMemorizedItemManager();
        	Map<String,StreamTable> lmgraphs = lm.getMemorizedGraphs();
        	Map<String,StreamTable> lmreports = lm.getMemorizedReports();
        	/*
        	 * two types of memorized items, Graphs and Reports
        	 */
        	Set<String> lmgset = lmgraphs.keySet();
        	Set<String> lmrset = lmreports.keySet();
        	Object [] lmgarr = lmgset.toArray();
        	Object [] lmrarr = lmrset.toArray();
        	int lmcgraphs = lmgraphs.size();
        	int lmcreports = lmreports.size();
     		if (lmcgraphs > 0) {
     			/*
     			 * add graphs to the tree
     			 */
     			for (int i=0;i<lmcgraphs;i++) {
       				myNodeObject mynode = new myNodeObject (EXTRAS_MEMORIZED_GRAPH_ENTRY,(String)lmgarr[i]);
       				mynode.setobject(lmgraphs.get((String) lmgarr[i]));
    	        	dataitem = new DefaultMutableTreeNode(mynode);
    				node.add(dataitem);  				
     			}
     		}    		
     		if (lmcreports > 0) {
     			/*
     			 * add reports to the tree
     			 */
     			for (int i=0;i<lmcreports;i++) {
       				myNodeObject mynode = new myNodeObject (EXTRAS_MEMORIZED_REPORT_ENTRY,(String) lmrarr[i]);
       				mynode.setobject(lmreports.get((String) lmrarr[i]));
    	        	dataitem = new DefaultMutableTreeNode(mynode);
    				node.add(dataitem);  				
     			}
     		}    		
        	break;
       	case '6' :
       		// list reminders
        	ReminderSet ls = root.getReminderSet();
        	long lsc = ls.getReminderCount();
     		if (lsc > 0) {
     			for (int i=0;i<lsc;i++) {
       				myNodeObject mynode = new myNodeObject (EXTRAS_REMINDER_ENTRY,ls.getReminder(i).toString());
    				mynode.setobject(ls.getReminder(i));
    	        	dataitem = new DefaultMutableTreeNode(mynode);
    				node.add(dataitem);  				
     			}
     		}    		
        	
        	break;
      	case '7' :
       		// list securities
         	CurrencyType[] lcts = root.getCurrencyTable().getAllCurrencies();
     		int lctcs = lcts.length;
     		if (node.isLeaf()) {
	     		if (lctcs > 0) {
	     			for (int i=0;i<lctcs;i++) {
	     				if (lcts[i].getCurrencyType() == CurrencyType.CURRTYPE_SECURITY) {
	     					myNodeObject mynode = new myNodeObject (EXTRAS_SECURITY_ENTRY,lcts[i].getName());
	     					mynode.setobject(lcts[i]);
	     					dataitem = new DefaultMutableTreeNode(mynode);
	     					node.add(dataitem);
	     				}
	     			}
	     		}
     		}
       		break;
       	}
		/*
		 * expand the tree at the clicked leaf just in case there are sub-accounts
		 */
        JTree tree = (JTree)e.getSource();
        TreePath path = tree.getSelectionPath();
        tree.expandPath(path);              					   				
		detailmodel.fireTableDataChanged();       	
    }
    /*
     * display the details of an extra when it is clicked
     */
    private void displayextras(myNodeObject userobj, Integer nodeid, DefaultMutableTreeNode node,TreeSelectionEvent e) {
    	DefaultMutableTreeNode dataitem;
		CustomDateFormat cdate = new CustomDateFormat ("DD/MM/YYYY");
    	detailmodel.setRowCount(0);
       	String nodepattern = nodeid.toString();
       	switch (nodepattern.charAt(1)) {
       	case '1' :
       		// display address
        	AddressBook.AddressEntry lab = (AddressBook.AddressEntry)userobj.getobject();
    		Vector<String> vadname = new Vector<String>();
    		vadname.add("Name");
    		vadname.add(lab.getName()+" ");
    		detailmodel.addRow(vadname);
    		Vector<String> vadadd = new Vector<String>();
    		vadadd.add("Address");
    		vadadd.add(lab.getAddress()+" ");
    		detailmodel.addRow(vadadd);
    		Vector<String> vademail = new Vector<String>();
    		vademail.add("Email Address");
    		vademail.add(lab.getEmailAddress()+" ");
    		detailmodel.addRow(vademail);
    		Vector<String> vadphone = new Vector<String>();
    		vadphone.add("Phone Number");
    		vadphone.add(lab.getPhoneNumber()+" ");
    		detailmodel.addRow(vadphone);
        	
       		break;
       	case '2' :
       		// display account book
        	AccountBook lac = (AccountBook)userobj.getobject();
    		Vector<String> vacname = new Vector<String>();
    		vacname.add("Name");
    		vacname.add(lac.getName()+" ");
    		detailmodel.addRow(vacname);
    		Vector<String> vacfile = new Vector<String>();
    		vacfile.add("Root File Name");
    		vacfile.add(lac.getRootFolder().getName()+" ");
    		detailmodel.addRow(vacfile);
    		Vector<String> vacfolder = new Vector<String>();
    		vacfolder.add("Root Folder Name");
    		vacfolder.add(lac.getRootFolder().getPath()+" ");
    		detailmodel.addRow(vacfolder);
    		Vector<String> vacint = new Vector<String>();
    		vacint.add("Within Internal Storage");
    		vacint.add(lac.isWithinInternalStorage()+" ");
    		detailmodel.addRow(vacint);
       		break;
       	case '3' :
       		// display budget data
       		Budget lb = (Budget)userobj.getobject();
    		Vector<String> vbname = new Vector<String>();
    		vbname.add("Name");
    		vbname.add(lb.getName()+" ");
    		detailmodel.addRow(vbname);
    		Vector<String> vbcnt = new Vector<String>();
    		vbcnt.add("Item Count");
    		vbcnt.add(lb.getItemCount()+" ");
    		detailmodel.addRow(vbcnt);
    		Vector<String> vbper = new Vector<String>();
    		vbper.add("Period Type");
    		if (lb.getPeriodType() != null) 
    			vbper.add(lb.getPeriodType().getOrder()+" ");
    		detailmodel.addRow(vbper);
    		Vector<String> vbkey = new Vector<String>();
    		vbkey.add("Key");
    		vbkey.add(lb.getKey()+" ");
    		detailmodel.addRow(vbkey);
    		Vector<String> vbns = new Vector<String>();
    		vbns.add("New Style");
    		vbns.add(lb.isNewStyle()+" ");
    		detailmodel.addRow(vbns);
    		
        		
       		// list budget items (only if not already added - isLeaf) 
    		if (node.isLeaf()) {
	     		int lbc = lb.getItemCount();
	     		if (lbc > 0) {
	     			for (int i=0;i<lbc;i++) {
	       				myNodeObject mynode = new myNodeObject (EXTRAS_BUDGET_ENTRY+BUDGET_INCREMENT,"["+i+"]");
	    				mynode.setobject(lb.getItem(i));
	    	        	dataitem = new DefaultMutableTreeNode(mynode);
	    				node.add(dataitem);  				
	     			}
	     		}    		
	    		/*
	    		 * expand the tree at the clicked leaf just in case there are sub-accounts
	    		 */
	            JTree tree = (JTree)e.getSource();
	            TreePath path = tree.getSelectionPath();
	            tree.expandPath(path);
    		}
       		break;
       	case '4' :
       	case '7' :
       		// display currency
        	CurrencyType lct = (CurrencyType)userobj.getobject();
    		Vector<String> vctname = new Vector<String>();
    		vctname.add("Name");
    		vctname.add(lct.getName()+" ");
    		detailmodel.addRow(vctname);
    		Vector<String> vctpref = new Vector<String>();
    		vctpref.add("Prefix");
    		vctpref.add(lct.getPrefix()+" ");
    		detailmodel.addRow(vctpref);
    		Vector<String> vctsuff = new Vector<String>();
    		vctsuff.add("Suffix");
    		vctsuff.add(lct.getSuffix()+" ");
    		detailmodel.addRow(vctsuff);
    		Vector<String> vctdp = new Vector<String>();
    		vctdp.add("Decimal Places");
    		vctdp.add(lct.getDecimalPlaces()+" ");
    		detailmodel.addRow(vctdp);
    		Vector<String> vctraw = new Vector<String>();
    		vctraw.add("Raw Rate");
    		vctraw.add(new DecimalFormat("#").format(lct.getRawRate())+" ");
    		detailmodel.addRow(vctraw);
    		Vector<String> vcttick = new Vector<String>();
    		vcttick.add("Ticker Symbol");
    		vcttick.add(lct.getTickerSymbol()+" ");
    		detailmodel.addRow(vcttick);
    		Vector<String> vctID = new Vector<String>();
    		vctID.add("ID");
    		vctID.add(lct.getIDString()+" ");
    		detailmodel.addRow(vctID);
    		Vector<String> vcthide = new Vector<String>();
    		vcthide.add("Hide in UI");
    		vcthide.add(lct.getHideInUI()+" ");
    		detailmodel.addRow(vcthide);
    		Vector<String> vcteffd = new Vector<String>();
    		vcteffd.add("Effective Date");
    		vcteffd.add(cdate.format(lct.getEffectiveDateInt())+" ");
    		detailmodel.addRow(vcteffd);
    		TagSet tsTags = lct.getTags();
    		if (tsTags != null)
    			if(tsTags.getTagCount() > 0) {
    				for (int iTag=0;iTag<tsTags.getTagCount();iTag++) {
    		    		Vector<String> vcttag = new Vector<String>();
    		    		vcttag.add("Tag : "+tsTags.getTagAt(iTag).getKey());
    		    		vcttag.add(tsTags.getTagAt(iTag).getValue());
    		    		detailmodel.addRow(vcttag);	
    				}
    			}
      		// list currency snap shot items (only if not already added - isLeaf) 
    		if (node.isLeaf()) {
	     		int lctc = lct.getSnapshotCount();
	     		int lctstart;
	     		if (lctc > 10)
	     			lctstart= lctc-10;
	     		else
	     			lctstart = 0;
	     		if (lctc > 0) {
	     			for (int i=lctc-1;i>=lctstart;i--) {
	     				myNodeObject mynode;
	     				if (nodepattern.charAt(1) == '4')
	     					mynode = new myNodeObject (EXTRAS_CURRENCY_ENTRY+CURRENCY_INCREMENT,"["+i+"]");
	     				else
	     					mynode = new myNodeObject (EXTRAS_SECURITY_ENTRY+SECURITY_INCREMENT,"["+i+"]");
	    				mynode.setobject(lct.getSnapshot(i));
	    	        	dataitem = new DefaultMutableTreeNode(mynode);
	    				node.add(dataitem);  				
	     			}
	     		}    		
	    		/*
	    		 * expand the tree at the clicked leaf just in case there are sub-accounts
	    		 */
	            JTree tree = (JTree)e.getSource();
	            TreePath path = tree.getSelectionPath();
	            tree.expandPath(path);
    		}
          		break;
       	case '5' :
       		// display memorized item
        	StreamTable stitem = (StreamTable) userobj.getobject();
        	String [] keys = stitem.getKeyArray();
    		Vector<String> vm = new Vector<String>();   	
        	for (String keyitem : keys) {
        		vm.add(keyitem);
        		vm.add(stitem.get(keyitem,"unknown")+" ");
        		detailmodel.addRow(vm);
        	}
        	break;
       	case '6' :
       		// display reminder
        	Reminder lrobj = (Reminder) userobj.getobject();
    		Vector<String> vrname = new Vector<String>();
    		vrname.add("Name");
    		vrname.add(lrobj.toString()+" ");
    		detailmodel.addRow(vrname);
    		Vector<String> vrdesc = new Vector<String>();
    		vrdesc.add("Description");
    		vrdesc.add(lrobj.getDescription()+" ");
    		detailmodel.addRow(vrdesc);
    		Vector<String> vridt = new Vector<String>();
    		vridt.add("Initial Date");
    		vridt.add(cdate.format(lrobj.getInitialDateInt())+" ");
    		detailmodel.addRow(vridt);
    		Vector<String> vrldt = new Vector<String>();
    		vrldt.add("Last Date");
    		vrldt.add(cdate.format(lrobj.getLastDateInt())+" ");
    		detailmodel.addRow(vrldt);
    		Vector<String> vrrt = new Vector<String>();
    		vrrt.add("Reminder Type");
    		vrrt.add(lrobj.getReminderType()+" ");
    		detailmodel.addRow(vrrt);
    		Vector<String> vrrd = new Vector<String>();
    		vrrd.add("Repeat Daily");
    		vrrd.add(lrobj.getRepeatDaily()+" ");
    		detailmodel.addRow(vrrd);
    		Vector<String> vrrm = new Vector<String>();
    		vrrm.add("Repeat Monthly");
    		int [] irm = lrobj.getRepeatMonthly();
    		if (irm.length > 0)
    			vrrm.add(lrobj.getRepeatMonthly()+" ");
    		else
    			vrrm.add("0 ");
    		detailmodel.addRow(vrrm);
    		Vector<String> vrrmm = new Vector<String>();
    		vrrmm.add("Repeat Monthly Modifier");
    		vrrmm.add(lrobj.getRepeatMonthlyModifier()+" ");
    		detailmodel.addRow(vrrmm);
    		Vector<String> vrrwd = new Vector<String>();
    		vrrwd.add("Repeat Weekly Days");
    		irm = lrobj.getRepeatWeeklyDays();
    		if (irm.length > 0)
    			vrrwd.add(lrobj.getRepeatWeeklyDays()+" ");
    		else
    			vrrwd.add("0 ");
    		detailmodel.addRow(vrrwd);
    		Vector<String> vrrwm = new Vector<String>();
    		vrrwm.add("Repeat Weekly Modifier");
    		vrrwm.add(lrobj.getRepeatWeeklyModifier()+" ");
    		detailmodel.addRow(vrrwm);
    		Vector<String> vrry = new Vector<String>();
    		vrry.add("Repeat Yearly");
    		vrry.add(lrobj.getRepeatYearly()+" ");
     		detailmodel.addRow(vrry);      	
        	break;
       	}
   		/*
		 * tell the table that it has changed
		 */
		detailmodel.fireTableDataChanged();       	
    }
    /*
     * displays a single budget item
     */
    private void displaybudgetitem(myNodeObject userobj, Integer nodeid, DefaultMutableTreeNode node,TreeSelectionEvent e) {
		CustomDateFormat cdate = new CustomDateFormat ("DD/MM/YYYY");
    	detailmodel.setRowCount(0);
       	BudgetItem bitem = (BudgetItem) userobj.getobject();
       	CurrencyType tctype = (bitem.getTransferAccount() == null) ? null : bitem.getTransferAccount().getCurrencyType();
   		Vector<String> vbiacct = new Vector<String>();
		vbiacct.add("Account Name");
		vbiacct.add((bitem.getAccount() == null) ? "unknown" : bitem.getAccount().getAccountName()+" ");
		detailmodel.addRow(vbiacct);
   		Vector<String> vbiamt = new Vector<String>();
		vbiamt.add("Amount");
		vbiamt.add((String) ((tctype == null) ? String.valueOf(bitem.getAmount()) : tctype.formatFancy(bitem.getAmount(), '.'))+" ");
		detailmodel.addRow(vbiamt);
   		Vector<String> vbicur = new Vector<String>();
		vbicur.add("Currency");
		vbicur.add(bitem.getCurrency()+" ");
		detailmodel.addRow(vbicur);
  		Vector<String> vbiint = new Vector<String>();
		vbiint.add("Interval");
		vbiint.add(intervaltypes.get(bitem.getInterval())+"("+bitem.getInterval()+")");
		detailmodel.addRow(vbiint);
  		Vector<String> vbiintsd = new Vector<String>();
		vbiintsd.add("Interval Start Date");
		vbiintsd.add(cdate.format(bitem.getIntervalStartDate()));
		detailmodel.addRow(vbiintsd);
  		Vector<String> vbiinted = new Vector<String>();
		vbiinted.add("Interval End Date");
		vbiinted.add(cdate.format(bitem.getIntervalEndDate()));
		detailmodel.addRow(vbiinted);
   		Vector<String> vbitacct = new Vector<String>();
		vbitacct.add("Transfer Accout Name");
		vbitacct.add((bitem.getTransferAccount() == null) ? "unknown" : bitem.getTransferAccount().getAccountName()+" ");
		detailmodel.addRow(vbitacct);
   		Vector<String> vbiinc = new Vector<String>();
		vbiinc.add("Is Income");
		vbiinc.add(bitem.isIncome()+" ");
		detailmodel.addRow(vbiinc);
		detailmodel.fireTableDataChanged();       			
   }
    /*
     * displays a single currency item
     */
    private void displaycurrencyitem(myNodeObject userobj, Integer nodeid, DefaultMutableTreeNode node,TreeSelectionEvent e) {
		CustomDateFormat cdate = new CustomDateFormat ("DD/MM/YYYY");
    	detailmodel.setRowCount(0);
       	CurrencyType.Snapshot ctitem = (CurrencyType.Snapshot) userobj.getobject();
   		Vector<String> vbiacct = new Vector<String>();
		vbiacct.add("Date");
		vbiacct.add(cdate.format(ctitem.getDateInt()));
		detailmodel.addRow(vbiacct);
   		Vector<String> vbrramt = new Vector<String>();
		vbrramt.add("Raw Rate");
		vbrramt.add(String.valueOf(ctitem.getRawRate()));
		detailmodel.addRow(vbrramt);
   		Vector<String> vbrramtr = new Vector<String>();
		vbrramtr.add("Real Raw Rate");
		vbrramtr.add(String.valueOf(1.0/ctitem.getRawRate()));
		detailmodel.addRow(vbrramtr);
   		Vector<String> vbudhamt = new Vector<String>();
		vbudhamt.add("User Daily High");
		vbudhamt.add(String.valueOf(ctitem.getUserDailyHigh()));
		detailmodel.addRow(vbudhamt);
   		Vector<String> vbudhamtr = new Vector<String>();
		vbudhamtr.add("Real User Daily High");
		vbudhamtr.add(String.valueOf(1.0/ctitem.getUserDailyHigh()));
		detailmodel.addRow(vbudhamtr);
   		Vector<String> vbudlamt = new Vector<String>();
		vbudlamt.add("User Daily Low");
		vbudlamt.add(String.valueOf(ctitem.getRawRate()));
		detailmodel.addRow(vbudlamt);
   		Vector<String> vbudlamtr = new Vector<String>();
		vbudlamtr.add("Real User Daily Low");
		vbudlamtr.add(String.valueOf(1.0/ctitem.getRawRate()));
		detailmodel.addRow(vbudlamtr);
  		Vector<String> vburamt = new Vector<String>();
		vburamt.add("User Rate");
		vburamt.add(String.valueOf(ctitem.getRawRate()));
		detailmodel.addRow(vburamt);
  		Vector<String> vburamtr = new Vector<String>();
		vburamtr.add("Real User Rate");
		vburamtr.add(String.valueOf(1.0/ctitem.getRawRate()));
		detailmodel.addRow(vburamtr);
		detailmodel.fireTableDataChanged();

   }
    public void viewTrans() {
		CustomDateFormat cdate = new CustomDateFormat ("DD/MM/YYYY");
		final JFrame frame = new JFrame("Moneydance Transactions");
    	final JPanel pTrans = new JPanel(new BorderLayout());
    	final JPanel panTop = new JPanel(new GridBagLayout());
    	final JPanel panMid = new JPanel();
    	final JPanel panBot = new JPanel(new GridBagLayout());
        GridBagConstraints con = new GridBagConstraints ();
    	CurrencyType ctype = null;
    	CurrencyTable ctab = this.extension.getUnprotectedContext().getRootAccount().getCurrencyTable();
    	CurrencyType ctgbp = ctab.getCurrencyByIDString("GBP");
    	pTrans.setBorder(new EmptyBorder(5,5,5,5));
        trantable = new MyTranTable(new DefaultTableModel(transcolumns,0),this);
        trantable.setAutoCreateRowSorter(true);
        DefaultTableModel tranmodel = (DefaultTableModel) trantable.getModel();
    	JScrollPane pscroll = new JScrollPane(trantable);
    	pscroll.setPreferredSize(new Dimension(1000,600));
    	con.gridx = 0;
    	con.gridy = 0;
    	JLabel txtstart = new JLabel("Start Date : ");
    	panTop.add (txtstart,con);
    	con.gridx = 1;
    	JLabel stdate = new JLabel(cdate.format(startDate));
    	panTop.add(stdate,con);
    	con.gridx = 2;
    	JLabel txtend = new JLabel(" End Date : ");
    	panTop.add (txtend,con);
    	con.gridx = 3;
    	JLabel endate = new JLabel(cdate.format(endDate));
    	panTop.add(endate,con);
    	pTrans.add(panTop,BorderLayout.PAGE_START);
    	panMid.add(pscroll);
    	pTrans.add(panMid,BorderLayout.CENTER);
    	txnSet = this.extension.getUnprotectedContext().getRootAccount().getTransactionSet();
    	if (this.includeAllTrans)
    		tsTrans = txnSet.getAllTxns();
    	else {
    		if (this.includeTransbyAccounts) {
	    		if (lastAcct == null)
	    			return;
	    		if (!(lastAcct instanceof Account))
	    			return;
	    		tsTrans = txnSet.getTransactionsForAccount(lastAcct);
    		}
    		else
    			return;
    	}
    	for (int i=0;i<tsTrans.getSize();i++) {
    		AbstractTxn txn = tsTrans.getTxn(i);
    		if (txn.getDateInt()< this.startDate || txn.getDateInt() > this.endDate)
    			continue;
    		Vector<String> vec = new Vector<String>();
    		vec.add(new DecimalFormat("#").format(txn.getTxnId())+" ");
    		if (txn.getAccount() == null) {
    			vec.add("None");
    			ctype = ctgbp;
    		}
    		else {
    			vec.add(txn.getAccount().getAccountName());
    			ctype = txn.getAccount().getCurrencyType();
    		}
    		vec.add(txn.getDescription());
    		vec.add(cdate.format(txn.getDateInt()));
    		vec.add(ctype.formatFancy(txn.getValue(),'.'));
    		vec.add(txn.getTransferType());
    		if (txn instanceof ParentTxn)
    			vec.add("P");
    		else
    			vec.add("S");
    		vec.add(txn.getCheckNumber());
       		vec.add(String.valueOf(txn.getStatus()));
      		tranmodel.addRow(vec);
    	}
    	tranmodel.fireTableDataChanged();
        con.gridy = 2;
        con.gridwidth = 1;
	    con.gridx = 0;
	    closeButton = new JButton("Close");
	    closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		    	closetran(pTrans, frame);
			}
		});
	    panBot.add(closeButton, con);
	    pTrans.add(panBot,BorderLayout.PAGE_END);
    	//Display the window.
    	frame.add(pTrans);
        frame.pack();
        frame.setVisible(true);

    }
    /*
     * display an individual transaction
     */
    public void displayTransaction(int iRow) {
		CustomDateFormat cdate = new CustomDateFormat ("DD/MM/YYYY");
		final JFrame frame = new JFrame("Transaction Detail");
    	final JPanel pTrans = new JPanel(new BorderLayout());
       	CurrencyType ctype = null;
    	CurrencyTable ctab = this.extension.getUnprotectedContext().getRootAccount().getCurrencyTable();
    	CurrencyType ctgbp = ctab.getCurrencyByIDString("GBP");
    	pTrans.setBorder(new EmptyBorder(5,5,5,5));
        tabTran = new JTable(new DefaultTableModel(columnNames,0));
        tabTran.setAutoCreateRowSorter(true);
        DefaultTableModel modTran = (DefaultTableModel) tabTran.getModel();
    	JScrollPane pscroll = new JScrollPane(tabTran);
    	Vector<String> vecid = new Vector<String>();
    	/*
    	 * find transaction
    	 */
    	String strTxnID = (String)(trantable.getValueAt(iRow, 0));
    	strTxnID = strTxnID.replaceAll("\\s", "");
    	Long lTxnID = Long.valueOf((String)(strTxnID));
		AbstractTxn txn = tsTrans.getTxnByID(lTxnID);
		vecid.add("ID");
		vecid.add(new DecimalFormat("#").format(txn.getTxnId())+" ");
		modTran.addRow(vecid);
		Vector<String> vecacct = new Vector<String>();
		vecacct.add("Account");
		
		if (txn.getAccount() == null) {
			vecacct.add("None");
			ctype = ctgbp;
		}
		else {
			vecacct.add(txn.getAccount().getAccountName());
			ctype = txn.getAccount().getCurrencyType();
		}
		modTran.addRow(vecacct);
		Vector<String> vecdesc = new Vector<String>();
		vecdesc.add("Description");
		vecdesc.add(txn.getDescription());
		modTran.addRow(vecdesc);
		Vector<String> vecdate = new Vector<String>();
		vecdate.add("Date");
		vecdate.add(cdate.format(txn.getDateInt()));
		modTran.addRow(vecdate);
		Vector<String> vecvalue = new Vector<String>();
		vecvalue.add("Value");
		vecvalue.add(ctype.formatFancy(txn.getValue(),'.'));
		modTran.addRow(vecvalue);
		Vector<String> vecttype = new Vector<String>();
		vecttype.add("Transfer Type");
		vecttype.add(txn.getTransferType());
		modTran.addRow(vecttype);
		Vector<String> vecps = new Vector<String>();
		vecps.add("Parent/Split");
		if (txn instanceof ParentTxn)
			vecps.add("Parent");
		else
			vecps.add("Split");
		modTran.addRow(vecps);
		Vector<String> veccheque = new Vector<String>();
		veccheque.add("Cheque");

		veccheque.add(txn.getCheckNumber());
		modTran.addRow(veccheque);
		Vector<String> vecstatus = new Vector<String>();
		vecstatus.add("Status");
   		vecstatus.add(String.valueOf(txn.getStatus()));
		modTran.addRow(vecstatus);
		Vector<String> vecotc = new Vector<String>();
		vecotc.add("Num other trans");
   		vecotc.add(String.valueOf(txn.getOtherTxnCount()));
		modTran.addRow(vecotc);
		TagSet tsTran = txn.getTags();
		if (tsTran != null) {
			for (int i=0;i<tsTran.getTagCount();i++) {
				Vector<String> vectag = new Vector<String>();
				Tag tTran = tsTran.getTagAt(i);
				vectag.add("Tag: "+tTran.getKey());
				vectag.add(tTran.getValue());
				modTran.addRow(vectag);
			}
		}
    	modTran.fireTableDataChanged();
     	pTrans.add(pscroll,BorderLayout.CENTER);
	    closeTranButton = new JButton("Close");
	    closeTranButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		    	closesingletran(pTrans, frame);
			}
		});
	    closeTranButton.setMaximumSize(new Dimension(40,20));
	    pTrans.add(closeTranButton,BorderLayout.PAGE_END);
    	//Display the window.
    	frame.add(pTrans);
        frame.pack();
        frame.setVisible(true);

   	
    }
    protected void closetran(JPanel pTrans, JFrame frame) {
    	pTrans.setVisible (false);
    	frame.dispose();
    }

    protected void closesingletran(JPanel pTrans, JFrame frame) {
    	pTrans.setVisible (false);
    	frame.dispose();
    }
	/** Close Window */
	protected void close() {
		this.setVisible(false);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.dispose();
	}
	
	/*
	 * Create the original tree nodes based on choices made
	 */
   private void createNodes(DefaultMutableTreeNode top) {
	        DefaultMutableTreeNode datatype = null;
	        DefaultMutableTreeNode dataitem = null;
	        int noaccounts;
	        accounts = new HashMap<Integer, List<Account>>();
	        loadbaseaccounts ();
	        datatype = new DefaultMutableTreeNode(new myNodeObject (ACCOUNTS_NODE,"Accounts"));
	        top.add(datatype);
	        if (this.includeBankAccounts) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_BANK);
	        	if (l== null)
	        		noaccounts = 0;	
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (BANK_NODE,"Banks("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeInvestments) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_INVESTMENT);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (INVESTMENT_NODE,"Investments("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeAssets) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_ASSET);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (ASSET_NODE,"Assets("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeCreditCards) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_CREDIT_CARD);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (CREDIT_CARD_NODE,"Credit Cards("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeLiabilities) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_LIABILITY);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (LIABILITY_NODE,"Liabilities("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeLoans) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_LOAN);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (LOAN_NODE,"Loans("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        datatype = new DefaultMutableTreeNode(new myNodeObject (CATEGORIES_NODE,"Categories"));
	        top.add(datatype);
	        if (this.includeIncomecat) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_INCOME);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode( new myNodeObject (INCOME_NODE,"Income("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeExpensecat) {
	        	List<Account> l = accounts.get(Account.ACCOUNT_TYPE_EXPENSE);
	        	if (l== null)
	        		noaccounts = 0;
	        	else
	        		noaccounts = l.size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXPENSE_NODE,"Expense("+noaccounts+")"));
	        	datatype.add(dataitem);
	        }
		    RootAccount root = this.extension.getUnprotectedContext().getRootAccount();
	        datatype = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_NODE,"Extras"));
	        top.add(datatype);
	        if (this.includeAddressBook) {
	        	AddressBook lab = root.getAddressBook();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_ADDRESS_NODE,"Addresses Book("+lab.getSize()+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeAccountBook) {
	        	int lac = AccountBookModel.getAccountBooksCount();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_ACCOUNT_NODE,"Account Book("+lac+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeBudgets) {
	        	BudgetList lab = root.getBudgetList();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_BUDGET_NODE,"Budgets("+lab.getBudgetCount()+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeCurrencies) {
	         	CurrencyTable lc = root.getCurrencyTable();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_CURRENCY_NODE,"Currencies"+getCurrencyCount(lc,CurrencyType.CURRTYPE_CURRENCY)));
	        	datatype.add(dataitem);
	        }
	        if (this.includeSecurities) {
	         	CurrencyTable lc = root.getCurrencyTable();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_SECURITY_NODE,"Securities"+getCurrencyCount(lc,CurrencyType.CURRTYPE_SECURITY)));
	        	datatype.add(dataitem);
	        }
	        if (this.includeMemorizedItems) {
	        	MemorizedItemManager lm = root.getMemorizedItemManager();
	        	int lmi = lm.getMemorizedGraphs().size()+lm.getMemorizedReports().size();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_MEMORIZED_NODE,"Memorized Items("+lmi+")"));
	        	datatype.add(dataitem);
	        }
	        if (this.includeReminders) {
	        	ReminderSet ls = root.getReminderSet();
	        	dataitem = new DefaultMutableTreeNode(new myNodeObject (EXTRAS_REMINDER_NODE,"Reminders("+ls.getReminderCount()+")"));
	        	datatype.add(dataitem);
	        }
   }
   /*
    * loads the top level accounts for adding to the tree
    */
   private void loadbaseaccounts (){
	    int accounttype;
	    RootAccount root = this.extension.getUnprotectedContext().getRootAccount();
	    int sz = root.getSubAccountCount();
	    for(int i=0; i<sz; i++) {
	      Account acct = root.getSubAccount(i);
	      accounttype = acct.getAccountType();
          List<Account> l = accounts.get(accounttype);
          if (l == null)
              accounts.put(accounttype, l=new ArrayList<Account>());
          l.add(acct);
	  }


   }
   /*
    * count the type of currencies
    */
   private String getCurrencyCount (CurrencyTable ctEntries, int iType) {
	   CurrencyType [] arrTypes = ctEntries.getAllCurrencies();
	   int iCount = 0;
	   for (int i=0;i<ctEntries.getCurrencyCount();i++) {
		   if (arrTypes[i].getCurrencyType() == iType)
			   iCount++;
	   }
	   return "("+iCount+")";
   }
	        
}
