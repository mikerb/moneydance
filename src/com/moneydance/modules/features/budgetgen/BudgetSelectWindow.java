/*
 * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 


package com.moneydance.modules.features.budgetgen;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.moneydance.awt.AwtUtil;

import java.awt.Insets;

/*
 * Window to select the budget to be processes, note only budgets in new format are displayed
 * 
 * Old format has a PeriodType of 'Mixed'
 */
public class BudgetSelectWindow extends javax.swing.JFrame 
{
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  private Main extension;
  private BudgetListExtend objBudgetList;
  @SuppressWarnings("rawtypes")
  private JComboBox boxBudget;
  private JPanel panScreen;
  private JButton butValues;
  private JButton butClose;
  private BudgetValuesWindow panValues = null;
  

  @SuppressWarnings({ "unchecked", "rawtypes" })


    /**
     * Creates new form BudgetMainWindow
     */
  public BudgetSelectWindow(Main ext) {
	super("Budget Generator");
    this.extension = ext;
 
    GridBagLayout gbl_panScreen = new GridBagLayout();
    gbl_panScreen.columnWeights = new double[]{0.0, 1.0};
    panScreen = new JPanel(gbl_panScreen);
    panScreen.setBorder(new EmptyBorder(10,10,10,10));
    
   
    GridBagConstraints conBudgl = new GridBagConstraints();
    conBudgl.insets = new Insets(0, 0, 5, 5);
    conBudgl.gridx=0;
    conBudgl.gridy = 0;
    conBudgl.gridwidth=1;
    conBudgl.gridheight=1;
    conBudgl.fill = GridBagConstraints.HORIZONTAL;
    conBudgl.anchor = GridBagConstraints.LINE_START;
    // Budget
    JLabel lblAccountsName = new JLabel("Budget:");
    panScreen.add(lblAccountsName,conBudgl);

    // Select Budget
    objBudgetList = new BudgetListExtend (extension.getUnprotectedContext());
    GridBagConstraints conBudg = new GridBagConstraints();
    conBudg.insets = new Insets(0, 0, 5, 0);
    conBudg.gridx = 1;
    conBudg.gridy = 0;
    conBudg.gridwidth=1;
    conBudg.gridheight=1;
    conBudg.fill = GridBagConstraints.HORIZONTAL;
    conBudg.anchor = GridBagConstraints.LINE_START;
    String strNames[] = objBudgetList.getBudgetNames();
    boxBudget = new JComboBox (strNames);
    panScreen.add(boxBudget,conBudg);

        // Buttons
    butValues = new JButton("Enter Values");
    butValues.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			enterValues();
		}
    });

    GridBagConstraints conBV  = new GridBagConstraints();
    conBV.insets = new Insets(0, 0, 0, 5);
    conBV.gridx = 0;
    conBV.gridy = 6;
    conBV.gridwidth=1;
    conBV.gridheight=1;
    conBV.fill = GridBagConstraints.HORIZONTAL;
    conBV.anchor = GridBagConstraints.LINE_START;
    panScreen.add(butValues, conBV);
    butClose = new JButton("Close");
    butClose.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
	                   closeConsole();
		}
	    });
    GridBagConstraints conBC  = new GridBagConstraints();
    conBC.gridx = 1;
    conBC.gridy = 6;
    conBC.gridwidth=1;
    conBC.gridheight=1;
    conBC.fill = GridBagConstraints.HORIZONTAL;
    conBC.anchor = GridBagConstraints.LINE_START;
    panScreen.add(butClose, conBC);
    setSize(300,100);
    getContentPane().add(panScreen);
    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    enableEvents(WindowEvent.WINDOW_CLOSING);
        
    AwtUtil.centerWindow(this);
        
    
  }
	public void closeConsole(){
		if (panValues != null) {
			panValues.setVisible(false);
			JFrame ValueFrame = (JFrame) SwingUtilities.getWindowAncestor(panValues);
			ValueFrame.dispose();
		}
		this.extension.closeConsole();
	}

  /* 
   * Display the detail based on selected options
   */
  protected void enterValues() {
      //Create and set up the window.
      JFrame frame = new JFrame("MoneyDance Budget Generator - Enter Values");
      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      panValues = new BudgetValuesWindow(this.extension,(String) boxBudget.getSelectedItem());
      frame.getContentPane().add(panValues);

      //Display the window.
      frame.setPreferredSize(new Dimension(Constants.FRAMEWIDTH,
    		  Constants.FRAMEDEPTH));
      frame.pack();
      frame.setVisible(true);
  }


  public final void processEvent(AWTEvent evt) {
    if(evt.getID()==WindowEvent.WINDOW_CLOSING) {
      extension.closeConsole();
      return;
    }
    if(evt.getID()==WindowEvent.WINDOW_OPENED) {
    }
    super.processEvent(evt);
  }
  


  void goAway() {
    setVisible(false);
    dispose();
  }
}
