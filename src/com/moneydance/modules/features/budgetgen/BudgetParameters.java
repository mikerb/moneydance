/*
 * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 

package com.moneydance.modules.features.budgetgen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.moneydance.apps.md.controller.DateRange;
import com.moneydance.apps.md.controller.FeatureModuleContext;
import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.AccountBook;
import com.moneydance.apps.md.model.BudgetItem;
import com.moneydance.apps.md.model.ExpenseAccount;
import com.moneydance.apps.md.model.RootAccount;
import com.moneydance.awt.JDateField;

/*
 * Container for all parameters for the budget.  Data is serialised into file
 * {filepath}/{budgetname}.bpam
 * 
 * {filepath} is file system path from AccountBook
 * {budgetname} is the internal key for the selected budget
 */
public class BudgetParameters implements java.io.Serializable {

	/*
	 * Static and transient fields are not stored 
	 */
	private static final long serialVersionUID = 1L;
	private transient FeatureModuleContext conCurrentCon;
	private transient AccountBook abCurAcctBook;
	private transient RootAccount rootAcct;
	private transient BudgetExtend objCurBudget;
	private transient File fiCurFolder;
	private transient FileInputStream fiCurInFile;
	private transient FileOutputStream fiCurOutFile;
	private transient String strFileName;
	private transient boolean bDirty;
    private transient SortedMap<String,Account> mapAccounts;
    private transient SortedMap<String,Account> mapMissing;
    private transient int iBudgetPer;
    /*
     * Table to link budget intervals item intervals
     */
    private static final Map<Integer, Integer> intervaltypes;
    static
    {
    		intervaltypes = new HashMap<Integer,Integer>();
		 	intervaltypes.put (Constants.PERIODANNUAL,BudgetItem.INTERVAL_ANNUALLY); 
			intervaltypes.put (Constants.PERIODWEEKLY,BudgetItem.INTERVAL_WEEKLY); 
			intervaltypes.put (Constants.PERIODBIWEEKLY,BudgetItem.INTERVAL_BI_WEEKLY); 
			intervaltypes.put (Constants.PERIODMONTHLY,BudgetItem.INTERVAL_MONTHLY); 
    }
   /*
     * The following fields are stored
     */

	private int iStartDate;
	private int iEndDate;
	private double dRPI;
	private int iDatePer;
	private List<BudgetLine> listBudgetLines;
	/*
	 * Constructor
	 */
	public BudgetParameters(FeatureModuleContext context, BudgetExtend objBudget,JDateField jdtFiscalStart) {
        mapAccounts = new TreeMap<String,Account>();
		conCurrentCon = context;
		objCurBudget = objBudget;
		/*
		 * set parameters to default
		 * 	Start Date - Fiscal Year
		 *  End Date - Fiscal End
		 *  RPI - 0.0
		 *  Date Period - 0 fiscal year
		 */
		Calendar calTemp = Calendar.getInstance();
		JDateField jdtTemp = new JDateField(Main.cdate);
		iStartDate = jdtFiscalStart.getDateInt();
		calTemp.set(iStartDate/10000, iStartDate/100-iStartDate/10000*100-1, iStartDate - iStartDate/100*100);
		calTemp.add(Calendar.YEAR, 1);
		jdtTemp.setDate(calTemp.getTime());
		jdtTemp.decrementDate();
		iEndDate = jdtTemp.getDateInt();
		dRPI = 0.0;
		iBudgetPer = objBudget.getPeriodOrder();
		iDatePer = 0;
		/*
		 * load Expense Accounts
		 */
		rootAcct = conCurrentCon.getRootAccount();
		loadAccounts(rootAcct, mapAccounts,"");
		/*
		 * mapMissing used to hold Expense Accounts declared in the main file but not
		 * being used by this budget.  Will only have entries if new entries added to the 
		 * main file or if user has deleted an Account from the budget.
		 */
		mapMissing = new TreeMap<String,Account>(mapAccounts);
		/*
		 * determine if file already exists
		 */
		abCurAcctBook = context.getRootAccount().getAccountBook();
		fiCurFolder = abCurAcctBook.getRootFolder();
		strFileName = fiCurFolder.getAbsolutePath()+"\\"+objCurBudget.getKey()+".bpam";
		try {
			fiCurInFile = new FileInputStream(strFileName);
			ObjectInputStream ois = new ObjectInputStream(fiCurInFile);
			/*
			 * file exists, copy temporary object to this object
			 */
			BudgetParameters objTemp = (BudgetParameters) ois.readObject();
			this.iStartDate = objTemp.iStartDate;
			this.iEndDate = objTemp.iEndDate;
			this.dRPI = objTemp.dRPI;
			this.iDatePer = objTemp.iDatePer;
			this.listBudgetLines = objTemp.listBudgetLines;
			/*
			 * can not guarantee Account object ids are correct,
			 * go through the budget lines and set the account object ids using
			 * a map of names to object ids
			 * 
			 * Set date to force set up of internal fields
			 */
			for (BudgetLine objLine : listBudgetLines) {
				objLine.setCategory(mapAccounts.get(objLine.getCategoryIndent()));
				objLine.setStartDate(objLine.getStartDate());
				/*
				 * remove from the mapMissing map to leave a list of those not in budgetlines
				 */
				mapMissing.remove(objLine.getCategoryIndent());
			}
			fiCurInFile.close();
		}
		catch (IOException | ClassNotFoundException ioException) {
			/*
			 * file does not exist
			 */
			listBudgetLines = new ArrayList<BudgetLine>();
			/*
			 * load Expense Categories into the Budget Line by traversing the accounts
			 */
			rootAcct = conCurrentCon.getRootAccount();

			/*
			 * traverse sorted map to get categories in ascending order and create budget lines
			 */
			for (Map.Entry<String,Account> mapEntry : mapAccounts.entrySet()){
		    	  listBudgetLines.add(new BudgetLine(mapEntry.getKey(),
		    			  mapEntry.getValue(), iStartDate,Constants.mapDatePeriod.get(iBudgetPer)));			
			}
			/*
			 * create the file
			 */
			try {
				fiCurOutFile = new FileOutputStream(strFileName);
				ObjectOutputStream oos = new ObjectOutputStream(fiCurOutFile);
				oos.writeObject(this);
				fiCurOutFile.close();
			}
			catch(IOException i)
			{
				i.printStackTrace();
			}
		}
		/*
		 * put budget lines in ascending indented name order
		 */
		Collections.sort(listBudgetLines);
	}

	/*
	 * Create a table of category names and account objects
	 * Account name is made up of all accounts in path
	 */
	 private static void loadAccounts(Account parentAcct, Map<String, Account> mapAccounts,String strIndentp) {
	    int sz = parentAcct.getSubAccountCount();
	    String strIndent;
	    strIndent = strIndentp;
	    for(int i=0; i<sz; i++) {
	      Account acct = parentAcct.getSubAccount(i);
	      if (acct instanceof ExpenseAccount){
	    	  mapAccounts.put(strIndent + Constants.chSeperator+acct.getAccountName(), acct);
	      }
	      loadAccounts(acct, mapAccounts,strIndent + Constants.chSeperator+acct.getAccountName());
	    }
	 }	
	 public int getLineCount() {
		return (listBudgetLines == null ? 0 :listBudgetLines.size());
	 }
	/*
	 * add a category from missing list
	 * 
	 * add to lines, remove from missing list
	 */
	public void addCategory(String strCategory) {
		if (mapMissing.containsKey(strCategory)) {
			Account acct = mapMissing.get(strCategory);
			mapMissing.remove(strCategory);
			listBudgetLines.add(new BudgetLine(strCategory,acct,iStartDate,Constants.mapDatePeriod.get(iBudgetPer)));
			mapAccounts.put(strCategory,acct);
		}
	}
	/*
	 * Return the whole data table
	 */
	public List<BudgetLine> getLines(){
		return listBudgetLines;
	}
	/*
	 * get individual line
	 */
	public BudgetLine getItem(int i) {
		return listBudgetLines.get(i);
	}
	/*
	 * determine if dirty
	 */
	public boolean isDirty() {
		if (bDirty)
			return true;
		for (BudgetLine objLine : listBudgetLines){
			if (objLine.isDirty())
				return true;
		}
		return false;
	}
	/*
	 * reset all dirty flags
	 */
	public void resetDirty() {
		bDirty = false;
		for (BudgetLine objLine : listBudgetLines){
			objLine.setDirty(false);
		}
	
	}
	/*
	 * gets
	 * 
	 * Budget Period
	 */
	public int getPeriod() {
		return iBudgetPer;
	}
	/*
	 * Date Period
	 */
	public int getDatePeriod() {
		return iDatePer;
	}
	/*
	 * Start Date
	 */
	public int getStartDate() {
		return iStartDate;
	}
	/*
	 * End Date
	 */
	public int getEndDate() {
		return iEndDate;
	}
	/*
	 * RPI
	 */
	public double getRPI() {
		return dRPI;
	}
	/*
	 * array of missing categories
	 */
	public SortedMap<String, Account> getMissing() {
		return mapMissing;
	}
	/*
	 * sets
	 * 
	 * Budget Period
	 */
	public void setPeriod(int iPer) {
		iBudgetPer = iPer;
		return;
	}
	/*
	 * Date Period
	 */
	public void setDatePeriod (int iDatePerp){
		iDatePer = iDatePerp;
	}
	/*
	 * set dirty flag
	 */
	public void setDirty(boolean bDir) {
		bDirty = bDir;
		return;
	}
	/*
	 * Start Date
	 */
	public void setStartDate(int iStart) {
		bDirty = true;
		iStartDate = iStart;
		return;
	}
	/*
	 * End Date
	 */
	public void setEndDate(int iEnd) {
		bDirty = true;
		iEndDate = iEnd;
		return;
	}
	/*
	 * RPI
	 */
	public void setRPI(double dRPIp) {
		bDirty = true;
		dRPI = dRPIp;
		return;
	}
	/*
	 * calculation 
	 * 
	 * all lines
	 */
	public boolean calculateAll () {
		boolean bActive = false;
		for (BudgetLine objLine : listBudgetLines) {
			if (objLine.getAmount()!= 0) {
				objLine.calculateLine(iStartDate, iEndDate, dRPI);
				bActive = true;
			}
		}
		return bActive;
	}
	/*
	 * selected lines
	 */
	public boolean calculateSelected () {
		boolean bActive = false;
		for (BudgetLine objLine : listBudgetLines) {
			if (objLine.getSelect()&& objLine.getAmount()!= 0) {
				objLine.calculateLine(iStartDate, iEndDate, dRPI);
				bActive = true;
			}
		}
		return bActive;
	}
	/*
	 * delete a specific line, add it to mapMissing
	 */
	public void deleteLine(int iRow) {
		String strAccount = listBudgetLines.get(iRow).getCategoryIndent();
		Account acct = listBudgetLines.get(iRow).getCategory();
		mapMissing.put(strAccount, acct);
		mapAccounts.remove(strAccount);
		listBudgetLines.remove(iRow);
		bDirty = true;
	}

	/*
	 * Save the parameters into the specified file
	 */
	public void saveParams() {
		try {
			fiCurOutFile = new FileOutputStream(strFileName);
			ObjectOutputStream oos = new ObjectOutputStream(fiCurOutFile);
			oos.writeObject(this);
			oos.close();
			fiCurOutFile.close();
		}
		catch(IOException i)
		{
			i.printStackTrace();
		}
		/*
		 * clear dirty flags
		 */
		resetDirty();
		
	}
	/*
	 * Generate
	 */
	public void generate(DateRange[] arrPeriods) {
		for (BudgetLine objLine : listBudgetLines) {
			if (objLine.getAmount() != 0.0)
				objLine.generateLine(arrPeriods);
		}
		
	}
}
