package com.moneydance.modules.features.budgetgen;

import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;

import javax.swing.table.DefaultTableModel;

import com.moneydance.apps.md.controller.DateRange;
import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.CurrencyType;
import com.moneydance.awt.JDateField;

public class GenerateTableModel extends DefaultTableModel {
	
	private List<String> listCategoryName;
	private List<Account> listCategoryObj;
	private List<long[]> listGenData1;
	private List<long[]> listGenData2;
	private List<long[]> listGenData3;
	private List<long[]> listCurrentData1;
	private List<long[]> listCurrentData2;
	private List<long[]> listCurrentData3;
	private List<CurrencyType> listCurrency;
	private int iPeriods = 0;
	private int iIncrement;
	private int iStartDate;
	private int iYear;
	private BudgetParameters objParams = BudgetValuesWindow.objParams;
	private CurrencyType objLocalCur;
	private DateRange[] arrDates;
	private String strColumn = "Date";

	public GenerateTableModel() {
		super();
		Calendar dtTemp;
        objLocalCur = Main.context.getRootAccount().getCurrencyTable().getBaseType();
		iStartDate = objParams.getStartDate();
		switch (BudgetValuesWindow.objBudget.getPeriodOrder()) {
		case Constants.PERIODWEEKLY :
			iPeriods = 52;
			iIncrement = 7;
			break;
		case Constants.PERIODBIWEEKLY :
			iPeriods = 26;
			iIncrement = 14;
			break;
		case Constants.PERIODMONTHLY :
			iPeriods = 12;
			iIncrement = -1;
			break;
		default :
			iPeriods = 1;
			iIncrement = -12;
		}
		dtTemp = Calendar.getInstance();
		int iYeart = iStartDate/10000;
		int iMontht = (iStartDate - iYeart*10000)/100;
		int iDayt = iStartDate - iYeart*10000 - iMontht *100;
		dtTemp.set(iYeart,iMontht-1,iDayt);
		arrDates = new DateRange[iPeriods];
		Calendar dtTemp2 = Calendar.getInstance();
		for (int i=0;i<iPeriods;i++) {
			dtTemp2.setTime(dtTemp.getTime());
			if (iIncrement >0)
				dtTemp2.add(Calendar.DAY_OF_YEAR, iIncrement);
			else
				dtTemp2.add(Calendar.MONTH, -iIncrement);
			dtTemp2.add(Calendar.DAY_OF_YEAR, -1);			
			arrDates[i] = new DateRange(dtTemp.getTime(), dtTemp2.getTime());
			dtTemp.setTime(dtTemp2.getTime());
			dtTemp.add(Calendar.DAY_OF_YEAR, 1);	
		}
		listCategoryName = new ArrayList<String>();
		listCategoryObj = new ArrayList<Account>();
		listCurrency = new ArrayList<CurrencyType>();
		listGenData1 = new ArrayList<long[]>();
		listGenData2 = new ArrayList<long[]>();
		listGenData3 = new ArrayList<long[]>();
		listCurrentData1 = new ArrayList<long[]>();
		listCurrentData2 = new ArrayList<long[]>();
		listCurrentData3 = new ArrayList<long[]>();		
	}

	public void AddLine (BudgetLine objLine) {
		listCategoryName.add(objLine.getCategoryName());
		listCategoryObj.add(objLine.getCategory());
		listGenData1.add(objLine.getYear1Array());
		listGenData2.add(objLine.getYear2Array());
		listGenData3.add(objLine.getYear3Array());
		Account objAcct =  objLine.getCategory();
		CurrencyType objCur;
		/*
		 * default to file base currency if currency not set
		 */
		objCur = (objAcct == null?null : objAcct.getCurrencyType());
		if (objCur == null)
			objCur = objLocalCur;
		listCurrency.add(objCur);
		/*
		 * get current budget items
		 */
		listCurrentData1.add(BudgetValuesWindow.objBudget.getCurrentValues(1,objLine.getCategory(),arrDates));
		listCurrentData2.add(BudgetValuesWindow.objBudget.getCurrentValues(2,objLine.getCategory(),arrDates));
		listCurrentData3.add(BudgetValuesWindow.objBudget.getCurrentValues(3,objLine.getCategory(),arrDates));
		return;
	}
	public void reloadCurrent() {
		listCurrentData1 = new ArrayList<long[]>();
		listCurrentData2 = new ArrayList<long[]>();
		listCurrentData3 = new ArrayList<long[]>();		
		for (Account objAcct:listCategoryObj) {
			listCurrentData1.add(BudgetValuesWindow.objBudget.getCurrentValues(1,objAcct,arrDates));
			listCurrentData2.add(BudgetValuesWindow.objBudget.getCurrentValues(2,objAcct,arrDates));
			listCurrentData3.add(BudgetValuesWindow.objBudget.getCurrentValues(3,objAcct,arrDates));
		}
	}
	public void setYear(int iYearp) {
		iYear=iYearp;
		return;
	}
	@Override
	public int getRowCount() {
		return (listGenData1 == null?0:listGenData1 .size()*2);
	}

	@Override
	public int getColumnCount() {
		return iPeriods;
	}
	
	@Override
	public String getColumnName(int c) {
		int iStart = arrDates[c].getStartDateInt() + (iYear-1)*10000;
		int iEnd = arrDates[c].getEndDateInt() + (iYear-1)*10000;
		JDateField jdtTemp = new JDateField(Main.cdate);
		strColumn= "<html>";
		strColumn += jdtTemp.getStringFromDateInt(iStart);
		strColumn +="-<br>";
		strColumn += jdtTemp.getStringFromDateInt(iEnd);
		strColumn +="</html>";
		return strColumn;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		CurrencyType objCur = listCurrency.get(rowIndex/2);	
		if ((rowIndex & 1) == 0) {
			switch (iYear) {
			case 1:
				return objCur.formatFancy(listGenData1.get(rowIndex/2)[columnIndex],'.');
			case 2:
				return objCur.formatFancy(listGenData2.get(rowIndex/2)[columnIndex],'.');
			default:
				return objCur.formatFancy(listGenData3.get(rowIndex/2)[columnIndex],'.');
			}
		}
		else {
			switch (iYear) {
			case 1:
				String strTemp =objCur.formatFancy(listCurrentData1.get(rowIndex/2)[columnIndex],'.');
				return strTemp;
			case 2:
				return objCur.formatFancy(listCurrentData2.get(rowIndex/2)[columnIndex],'.');
			default:
				return objCur.formatFancy(listCurrentData3.get(rowIndex/2)[columnIndex],'.');
			}
		}	
	}
	@Override
    public boolean isCellEditable(int row, int col) {
			return false;
	}
 	@Override
	public void setValueAt(Object value, int row, int col){
 	}
 	/*
 	 * return period dates
 	 */
 	public DateRange[] getPeriods() {
 		return arrDates;
 	}
 	/*
 	 * return generated values
 	 */
 	public long[] getGeneratedValues(int iRow) {
 		switch (iYear) {
 		case 1:
 			return listGenData1.get(iRow);
 		case 2:
 			return listGenData2.get(iRow);
 		default:
 			return listGenData3.get(iRow);
 		}
 	}
 	/*
 	 * return current values
 	 */
 	public long[] getCurrentValues(int iRow) {
 		switch (iYear) {
 		case 1:
 			return listCurrentData1.get(iRow);
 		case 2:
 			return listCurrentData2.get(iRow);
 		default:
 			return listCurrentData3.get(iRow);
 		}
 	}
 	/*
 	 * return the account object for row i
 	 */
 	public Account getCategoryObj(int iRow) {
 		return listCategoryObj.get(iRow);
 	}
}
