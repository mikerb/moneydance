/*
 * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.budgetgen;

import java.util.List;

import javax.swing.JComboBox;
import javax.swing.table.DefaultTableModel;

import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.CurrencyType;
import com.moneydance.awt.JDateField;

public class MyTableModel extends DefaultTableModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int iYears = 1;
	private static final String[] strColumns = 
		{"Select", "Category", "Amount", "Period", "Start Date","Extra RPI", "Year 1"};
	@SuppressWarnings("rawtypes")
	private static final Class[] columnTypes = new Class[] {
			Boolean.class, Object.class, String.class, JComboBox.class, String.class,String.class, String.class
		};

	@Override
	public int getRowCount() {
		return ( BudgetValuesWindow.objParams == null? 0 :  BudgetValuesWindow.objParams.getLineCount());
	}

	@Override
	public int getColumnCount() {
			return strColumns.length+iYears-1;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getColumnClass(int c){
		if (c == strColumns.length || c == strColumns.length+1)
			return String.class;
		return columnTypes[c];
	}
	@Override
	public String getColumnName(int c) {
		if (c == strColumns.length)
			return "Year 2";
		if (c == strColumns.length+1)
			return "Year 3";
		return strColumns[c];
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Account objAcct =  BudgetValuesWindow.objParams.getLines().get(rowIndex).getCategory();
        CurrencyType objLocalCur = Main.context.getRootAccount().getCurrencyTable().getBaseType();
		CurrencyType objCur;
		BudgetLine objLine = BudgetValuesWindow.objParams.getLines().get(rowIndex);
		/*
		 * default to file base currency if currency not set
		 */
		objCur = (objAcct == null?null : objAcct.getCurrencyType());
		if (objCur == null)
			objCur = objLocalCur;
		switch (columnIndex) {
		case 0:
			return  objLine.getSelect();
		case 1:
			return  objLine.getCategoryName();
		case 2:
			return objCur.formatFancy( objLine.getAmount(),'.');
		case 3:
			return Constants.arrPeriod[ objLine.getPeriod()];
		case 4:
			JDateField jdtTemp = new JDateField(Main.cdate);
			return 	jdtTemp.getStringFromDateInt(objLine.getStartDate());
		case 5:
			return  objLine.getRPI()/100.0;
		case 6:
			return objCur.formatFancy( objLine.getYear1Amt(),'.');
		case 7:
			return objCur.formatFancy( objLine.getYear2Amt(),'.');
		default:
			return objCur.formatFancy( objLine.getYear3Amt(),'.');
		}
	}
	@Override
    public boolean isCellEditable(int row, int col) {
        /*
         * Only Select, amount, period, start date and RPI are editable
         * Category, Year 1, Year 2, Year 3 are not
         */
        switch (col) {
        case 0:
        case 2:
        case 3:
        case 4:
        case 5:
            return true;
        case 1:
        case 6:
        case 7:
       default:
            return false;
        }
    }
	@Override
	public void setValueAt(Object value, int row, int col){
		BudgetLine objBud =  BudgetValuesWindow.objParams.getItem(row);
		/*
		 * copes with call when data is invalid
		 */
		if (value == null)
			return;
		switch (col) {
		case 0:
			objBud.setSelect((Boolean)value);
			break;
		case 2:
			/*
			 * Get the currency type for the row.  If it is null use the default type for the file
			 */
	        List<BudgetLine> listLines =  BudgetValuesWindow.objParams.getLines();
	        CurrencyType objCur =(listLines.get(row).getCategory() == null ? 
	        		Main.context.getRootAccount().getCurrencyTable().getBaseType():listLines.get(row).getCategory().getCurrencyType());
	        long lAmt =objCur.parse((String)value,'.');
			objBud.setAmount((int)lAmt);
			break;
		case 3:
			for (int i=0;i<Constants.arrPeriod.length;i++){
				if (((String)value).equals(Constants.arrPeriod[i]))
					objBud.setPeriod(i);				
			}
			break;
		case 4:
			JDateField jdtField = new JDateField(Main.cdate);
			objBud.setStartDate(jdtField.getDateIntFromString((String)value));
			break;
		case 5:
			objBud.setRPI(Double.parseDouble((String)value));
		}
	}
	public void alterYears(int iYearsp) {
		if (this.iYears > iYearsp) {
			iYears = iYearsp;
		}
		else {
			while (this.iYears < iYearsp) {
				this.addColumn("Year "+(iYears+1));
				iYears++;
			}
		}
		this.fireTableStructureChanged();		
	}
}


