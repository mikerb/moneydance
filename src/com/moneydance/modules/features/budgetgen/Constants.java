package com.moneydance.modules.features.budgetgen;

import java.util.HashMap;
import java.util.Map;

import com.moneydance.apps.md.model.BudgetItem;

public abstract class Constants {
	/*
	 * Budget Lines
	 */
	public static final String PERIOD_BIWEEK = "Bi-weekly";
	public static final String PERIOD_MONTH = "Monthly";
	public static final String PERIOD_WEEK = "Weekly";
	public static final String PERIOD_QUARTER = "Quarterly";
	public static final String PERIOD_TENMONTH = "Ten Monthly";
	public static final String PERIOD_YEAR = "Annual";
	public static final String [] arrPeriod = {PERIOD_WEEK,PERIOD_BIWEEK,PERIOD_MONTH,PERIOD_QUARTER,PERIOD_TENMONTH,PERIOD_YEAR};
	public static final String chSeperator = "/";
	/*
	 * Internal order for Period Type - note must match internal order on MD Budget Period Type
	 */
	public static final int PERIODWEEKLY = 0;
	public static final int PERIODBIWEEKLY = 1;
	public static final int PERIODMONTHLY = 2;
	public static final int PERIODANNUAL = 3;
	/*
	 * table to match MD Period TYpe to Line Date Period - second number must match arrPeriod
	 */
	public static final Map<Integer,Integer> mapDatePeriod;
	static
	{
		mapDatePeriod = new HashMap<Integer,Integer>();
		mapDatePeriod.put(PERIODWEEKLY, 0);
		mapDatePeriod.put(PERIODBIWEEKLY, 1);
		mapDatePeriod.put(PERIODMONTHLY, 2);
		mapDatePeriod.put(PERIODANNUAL, 5);
	}
    /*
     * Table to link budget intervals item intervals
     */
    public static final Map<Integer, Integer> intervaltypes;
    static
    {
    	intervaltypes = new HashMap<Integer,Integer>();
		intervaltypes.put (PERIODANNUAL,BudgetItem.INTERVAL_ANNUALLY); 
		intervaltypes.put (PERIODWEEKLY,BudgetItem.INTERVAL_WEEKLY); 
		intervaltypes.put (PERIODBIWEEKLY,BudgetItem.INTERVAL_BI_WEEKLY); 
		intervaltypes.put (PERIODMONTHLY,BudgetItem.INTERVAL_MONTHLY); 
    }
	/*
	 * Types of date periods to determine start and end date
	 */
	public static final String PERIOD_FISCAL = "Fiscal Year";
	public static final String PERIOD_CALENDAR = "Calendar Year";
	public static final String PERIOD_CUSTOM = "Custom";
	public static final String [] PERIODS = new String [] 
			  {PERIOD_FISCAL,PERIOD_CALENDAR ,PERIOD_CUSTOM};
    /*
	 * Screen Size Parameters
	 */
	public static final int FRAMEWIDTH =800;
	public static final int FRAMEDEPTH = 800;
	public static final int TOPWIDTH = FRAMEWIDTH-100;
	public static final int TOPDEPTH = 200;
	public static final int BOTWIDTH = FRAMEWIDTH-100;
	public static final int BOTDEPTH = 100;
	public static final int MIDWIDTH = FRAMEWIDTH;
	public static final int MIDDEPTH = FRAMEDEPTH - TOPDEPTH - BOTDEPTH;
	public static final int ADDSCREENWIDTH = 350;
	public static final int ADDSCREENHEIGHT =300;
	public static final int GENSCREENWIDTH = 1000;
	public static final int GENSCREENHEIGHT = 800;
	/*
	 * Generate screen panel sizes
	 */
	public static final int GENBOTWIDTH = GENSCREENWIDTH-50;
	public static final int GENBOTDEPTH = (GENSCREENHEIGHT)/8;
	public static final int GENTOPWIDTH = GENSCREENWIDTH-50;
	public static final int GENTOPDEPTH = (GENSCREENHEIGHT)/8;
	public static final int GENMIDWIDTH = GENSCREENWIDTH-50;
	public static final int GENMIDDEPTH = GENSCREENHEIGHT-GENTOPDEPTH-GENBOTDEPTH-50;
	public static final int GENSELECTPREFWIDTH = 40;
	public static final int GENSELECTMINWIDTH = 20;
	public static final int GENCATPREFWIDTH = 200;
	public static final int GENCATMINWIDTH = 100;
	public static final int GENAMOUNTWIDTH = 80;
}
