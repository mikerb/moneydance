/*
 * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.budgetgen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JPanel;

import com.moneydance.apps.md.controller.UserPreferences;
import com.moneydance.awt.JDateField;

import javax.swing.JScrollPane;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import java.awt.Insets;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
/*
 * Main window of budget parameters. Contains a row for each BudgetLine.  BudgetLines are
 * obtained from the Budget Parameters
 */

public class BudgetValuesWindow extends JPanel
{
		/**
		 * Identifies the budget being worked on
		 */
		public static BudgetListExtend objBudgetList;
		public static BudgetExtend objBudget;
		public static BudgetParameters objParams;
		/**
		 * Screen fields
		 */
		public static MyTable tabBudget;
		public static MyTableModel modBudget;
		private JButton btnAddAccount;
		private JButton btnSelectAll;
		private JButton btnDeselectAll;
		private JButton btnDeleteSelected;
		private JButton btnCalculateSelected;
		private JButton btnCalculateAll;
		private JButton btnGenerate;
		private JButton btnClose;
		private JButton btnSave;
		private JFormattedTextField txtRPI;
		private JComboBox<String> boxPeriod;
		private JComboBox<String> boxYears;
		/*
		 * date fields
		 */
		public static int iFiscalYear;
		public static int iYear;
		public static JDateField jdtStartDate;
		public static JDateField jdtEndDate;
		public static JDateField jdtFiscalStart;
		public static JDateField jdtFiscalEnd;
		public static JDateField jdtStartYear;
		public static JDateField jdtEndYear;
		public static Calendar dtWeekStart;


		private JPanel panTop;
		private JPanel panMid;
		private JPanel panBot;
		private JScrollPane spScrollPane;
		private GenerateWindow panGen = null;
		private AddAccountsWindow panAcct = null;

		
	public BudgetValuesWindow(Main extension,
			String strSelectedBud) {
	    /*
	     * set up budget objects
	     */
		objBudgetList = new BudgetListExtend (extension.getUnprotectedContext());
	    iFiscalYear = Integer.parseInt(extension.up.getSetting(UserPreferences.FISCAL_YEAR_START_MMDD));
		objBudget = objBudgetList.getBudget(strSelectedBud,iFiscalYear);
		objParams = objBudget.getParameters();
		/*
		 * set up dates for periods (Fiscal Start date is set when BudgetExtend is created)
		 */
	    Calendar gc = Calendar.getInstance();
	    iYear = gc.get(Calendar.YEAR);
	    Calendar dtYear = Calendar.getInstance();
	    dtYear.set(iYear,0, 1);
	    jdtStartYear = new JDateField (Main.cdate);
	    jdtStartYear.setDate(dtYear.getTime());
	    jdtEndYear = new JDateField (Main.cdate);
	    jdtEndYear.setDateInt(jdtStartYear.getDateInt());
	    jdtEndYear.gotoLastDayInYear();
	    Calendar dtFiscalEnd = Calendar.getInstance();
	    dtWeekStart= Calendar.getInstance();
	    dtWeekStart.setWeekDate(iYear,1,Calendar.SUNDAY);
	    jdtFiscalStart = objBudget.getFiscalStart();
	    jdtFiscalEnd = new JDateField (Main.cdate);    	    
	    dtFiscalEnd.setTime(jdtFiscalStart.parseDate());
	    dtFiscalEnd.add(Calendar.YEAR,1);
	    jdtFiscalEnd.setDate(dtFiscalEnd.getTime());
	    jdtFiscalEnd.decrementDate();
	    jdtStartDate = new JDateField(Main.cdate);
	    jdtEndDate = new JDateField(Main.cdate);
		/*
		 * set up table
		 */
		tabBudget = new MyTable(new MyTableModel());
		modBudget = (MyTableModel) tabBudget.getModel();
		/*
		 * start of screen
		 * 
		 * Top panel
		 */
		this.setLayout(new BorderLayout());
		panTop = new JPanel();
		panTop.setSize(Constants.TOPWIDTH,Constants.TOPDEPTH);
		GridBagLayout gbl_panTop = new GridBagLayout();
		gbl_panTop.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0};
		gbl_panTop.columnWidths = new int[]{100, 100, 100, 100, 0};
		gbl_panTop.rowHeights = new int[] {20, 20, 20, 20};
		panTop.setLayout(gbl_panTop);
		/*
		 * Budget Selected
		 */
		JLabel lblBudget = new JLabel("Budget :");
		GridBagConstraints gbc_lblBudget = new GridBagConstraints();
		gbc_lblBudget.insets = new Insets(10,10,10,10);
		gbc_lblBudget.gridx = 0;
		gbc_lblBudget.gridy = 0;
		gbc_lblBudget.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblBudget, gbc_lblBudget);
		JLabel lblBudgetName = new JLabel(objBudget.getName()+"("+objBudget.getPeriodTypeName()+")");
		GridBagConstraints gbc_lblBudgetName = new GridBagConstraints();
		gbc_lblBudgetName.insets = new Insets(10,10,10,10);
		gbc_lblBudgetName.gridx = 1;
		gbc_lblBudgetName.gridy = 0;
		gbc_lblBudgetName.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblBudgetName, gbc_lblBudgetName);
		/*
		 * Budget Period
		 */
		JLabel lblPeriod = new JLabel("Period :");
		GridBagConstraints gbc_lblPeriod = new GridBagConstraints();
		gbc_lblPeriod.insets = new Insets(10,10,10,10);
		gbc_lblPeriod.gridx = 2;
		gbc_lblPeriod.gridy = 0;
		gbc_lblPeriod.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblPeriod, gbc_lblPeriod);
		
		boxPeriod = new JComboBox<>(Constants.PERIODS);
		GridBagConstraints gbc_boxPeriod = new GridBagConstraints();
		gbc_boxPeriod.insets = new Insets(10,10,10,10);
		gbc_boxPeriod.gridx = 3;
		gbc_boxPeriod.gridy = 0;
		gbc_boxPeriod.anchor = GridBagConstraints.FIRST_LINE_START;
		boxPeriod.setSelectedIndex(objParams.getDatePeriod());
		panTop.add(boxPeriod, gbc_boxPeriod);
		boxPeriod.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				objParams.setDatePeriod(boxPeriod.getSelectedIndex());
				changeDates();
			}
	    });
		/*
		 * Start Date
		 */
		JLabel lblDatesStart = new JLabel("Dates - Start :");
		GridBagConstraints gbc_lblDateStart = new GridBagConstraints();
		gbc_lblDateStart.insets = new Insets(10,10,10,10);
		gbc_lblDateStart.gridx = 0;
		gbc_lblDateStart.gridy = 1;
		gbc_lblDateStart.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblDatesStart, gbc_lblDateStart);
		jdtStartDate.setDateInt(objParams.getStartDate());
	    jdtStartDate.setDisabledTextColor(Color.BLACK);	
		GridBagConstraints gbc_txtStartDate = new GridBagConstraints();
		gbc_txtStartDate.anchor = GridBagConstraints.FIRST_LINE_START;
		gbc_txtStartDate.insets = new Insets(10,10,10,10);
		gbc_txtStartDate.gridx = 1;
		gbc_txtStartDate.gridy = 1;
		panTop.add(jdtStartDate, gbc_txtStartDate);
		jdtStartDate.addPropertyChangeListener(new PropertyChangeListener () {
			@Override
			public void propertyChange(PropertyChangeEvent e) {
				changeStartDate(e);
			}
	    });
		/*
		 * End Date
		 */
		JLabel lblEnd = new JLabel("End :");
		GridBagConstraints gbc_lblEnd = new GridBagConstraints();
		gbc_lblEnd.insets = new Insets(10,10,10,10);
		gbc_lblEnd.gridx = 2;
		gbc_lblEnd.gridy = 1;
		gbc_lblEnd.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblEnd, gbc_lblEnd);
		
		jdtEndDate.setDateInt(objParams.getEndDate());
	    jdtEndDate.setDisabledTextColor(Color.BLACK);
		GridBagConstraints gbc_txtEndDate = new GridBagConstraints();
		gbc_txtEndDate.insets = new Insets(10,10,10,10);
		gbc_txtEndDate.gridx = 3;
		gbc_txtEndDate.gridy = 1;
		gbc_txtEndDate.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(jdtEndDate, gbc_txtEndDate);
		jdtEndDate.setInputVerifier(new InputVerifier() {
			@Override
			public boolean verify(JComponent jcField){			
				JDateField jdtEnd = (JDateField) jcField;
				Calendar dtStart = Calendar.getInstance();
				Calendar dtEnd = Calendar.getInstance();
				dtStart.setTime(jdtStartDate.getDate());
				dtEnd.setTime(jdtEnd.getDate());
				dtStart.add(Calendar.YEAR, 1);
				dtStart.add(Calendar.DAY_OF_YEAR,-1);
				if (dtEnd.after(dtStart)) {
					JFrame fTemp = new JFrame();
					JOptionPane.showMessageDialog(fTemp,"End Date can not be beyond 1 year after start date");
					return false;
				}
				objParams.setEndDate(jdtEnd.getDateInt());
				return true;
			} 
		});
		/*
		 * years to generate
		 */
		JLabel lblYearsl = new JLabel("Years:");
		lblYearsl.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblYearsl = new GridBagConstraints();
		gbc_lblYearsl.insets = new Insets(10,10,10,10);
		gbc_lblYearsl.gridx = 0;
		gbc_lblYearsl.gridy = 2;
		gbc_lblYearsl.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblYearsl, gbc_lblYearsl);
		
		boxYears = new JComboBox<String>();
		boxYears.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2", "3"}));
		boxYears.setSelectedIndex(0);
		GridBagConstraints gbc_boxYears = new GridBagConstraints();
		gbc_boxYears.insets = new Insets(10,10,10,10);
		gbc_boxYears.gridx = 1;
		gbc_boxYears.gridy = 2;
		gbc_boxYears.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(boxYears, gbc_boxYears);
		boxYears.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				changeYears();
			}
	    });
		/*
		 * RPI
		 */
		JLabel lblRPIl = new JLabel("RPI:");
		lblRPIl.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblRPIl = new GridBagConstraints();
		gbc_lblRPIl.insets = new Insets(10,10,10,10);
		gbc_lblRPIl.gridx = 2;
		gbc_lblRPIl.gridy = 2;
		gbc_lblRPIl.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblRPIl, gbc_lblRPIl);

		
        txtRPI = new JFormattedTextField();
        GridBagConstraints gbc_txtRPI = new GridBagConstraints();
		gbc_txtRPI.insets = new Insets(10,10,10,10);
		gbc_txtRPI.gridx = 3;
		gbc_txtRPI.gridy = 2;
		gbc_txtRPI.anchor = GridBagConstraints.FIRST_LINE_START;
		txtRPI.setInputVerifier(new RPIVerifier());
		txtRPI.setValue(String.format("%1$,.2f%%",objParams.getRPI()));
		txtRPI.setColumns(10);
		txtRPI.addPropertyChangeListener("value",new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent e) {
			    JFormattedTextField source = (JFormattedTextField)e.getSource();
			    String strTemp = (String) source.getValue();
			    if (strTemp.endsWith("%")) {
			    	strTemp = strTemp.substring(0,strTemp.length()-1);
			    }
			    try {
			    	objParams.setRPI(Double.parseDouble((String) strTemp));
					source.setValue(String.format("%1$,.2f%%",objParams.getRPI()));
			    } catch (NumberFormatException pe) { }
			}
		});

		panTop.add(txtRPI, gbc_txtRPI);
		this.add(panTop, BorderLayout.PAGE_START);

		/*
		 * Middle panel - table
		 */
		spScrollPane = new JScrollPane(tabBudget);
		panMid = new JPanel(new GridLayout(1,1));
		panMid.setSize(Constants.MIDWIDTH,Constants.MIDDEPTH);
		panMid.add(spScrollPane);
		this.add(panMid,BorderLayout.CENTER);
		/*
		 * Bottom Panel - buttons
		 */
		panBot = new JPanel(new GridBagLayout());
		panBot.setSize(Constants.BOTWIDTH,Constants.BOTDEPTH);
		/*
		 * Button 0 - Add Account
		 */
		GridBagConstraints gbcbt0 = new GridBagConstraints();
		gbcbt0.gridx = 0;
		gbcbt0.gridy = 0;
		gbcbt0.anchor = GridBagConstraints.LINE_START;
		gbcbt0.insets = new Insets(15,15,15,15);
		gbcbt0.fill = GridBagConstraints.HORIZONTAL; 
		btnAddAccount = new JButton("Add Category");
		btnAddAccount.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addAccount();
			}
		});
		panBot.add(btnAddAccount,gbcbt0);
		/*
		 * Button 1 - Select All
		 */
		GridBagConstraints gbcbt1 = new GridBagConstraints();
		gbcbt1.gridx = gbcbt0.gridx+1;
		gbcbt1.gridy = gbcbt0.gridy;
		gbcbt1.anchor = GridBagConstraints.LINE_START;
		gbcbt1.insets = new Insets(15,15,15,15);
		gbcbt1.fill = GridBagConstraints.HORIZONTAL; 
		btnSelectAll = new JButton("Select All");
		btnSelectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectAll();
			}
		});
		panBot.add(btnSelectAll,gbcbt1);
		/*
		 * Button 2 - Calculate Selected
		 */
		GridBagConstraints gbcbt2 = new GridBagConstraints();
		gbcbt2.gridx = gbcbt1.gridx+1;
		gbcbt2.gridy = gbcbt1.gridy;
		gbcbt2.anchor = GridBagConstraints.LINE_START;
		gbcbt2.insets = new Insets(15,15,15,15);
		gbcbt2.fill = GridBagConstraints.HORIZONTAL; 
		btnCalculateSelected = new JButton("Calculate \r\nSelected");
		btnCalculateSelected.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculateSelected();
			}
		});
		panBot.add(btnCalculateSelected,gbcbt2);
		/*
		 * Button 8 - Save
		 */
		GridBagConstraints gbcbt8 = new GridBagConstraints();
		gbcbt8.gridx = gbcbt2.gridx+1;
		gbcbt8.gridy = gbcbt1.gridy;
		gbcbt8.anchor = GridBagConstraints.LINE_START;
		gbcbt8.insets = new Insets(15,15,15,15);
		gbcbt8.fill = GridBagConstraints.HORIZONTAL; 
		btnSave = new JButton("Save Parameters");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});

		/*
		 * Button 4 - Close
		 */
		GridBagConstraints gbcbt4 = new GridBagConstraints();
		gbcbt4.gridx = gbcbt8.gridx+1;
		gbcbt4.gridy = gbcbt1.gridy;
		gbcbt4.anchor = GridBagConstraints.LINE_START;
		gbcbt4.insets = new Insets(15,15,15,15);
		gbcbt4.fill = GridBagConstraints.HORIZONTAL; 
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		panBot.add(btnClose,gbcbt4);
		/*
		 * Button 3 - Delete Selected
		 */
		GridBagConstraints gbcbt3 = new GridBagConstraints();
		gbcbt3.gridx = gbcbt0.gridx;
		gbcbt3.gridy = gbcbt0.gridy+1;
		gbcbt3.anchor = GridBagConstraints.LINE_START;
		gbcbt3.insets = new Insets(15,15,15,15);
		gbcbt3.fill = GridBagConstraints.HORIZONTAL; 
		btnDeleteSelected = new JButton("Delete Selected");
		btnDeleteSelected.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deleteSelected();
			}
		});
		panBot.add(btnDeleteSelected,gbcbt3);
		/*
		 * Button 5 - De-Select All
		 */
		GridBagConstraints gbcbt5 = new GridBagConstraints();
		gbcbt5.gridx = gbcbt3.gridx+1;
		gbcbt5.gridy = gbcbt3.gridy;
		gbcbt5.anchor = GridBagConstraints.LINE_START;
		gbcbt5.insets = new Insets(15,15,15,15);
		gbcbt5.fill = GridBagConstraints.HORIZONTAL; 
		btnDeselectAll = new JButton("Deselect All");
		btnDeselectAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				deselectAll();
			}
		});
		panBot.add(btnDeselectAll,gbcbt5);
		/*
		 * Button 6 - Calculate All
		 */
		GridBagConstraints gbcbt6 = new GridBagConstraints();
		gbcbt6.gridx = gbcbt5.gridx+1;
		gbcbt6.gridy = gbcbt5.gridy;
		gbcbt6.anchor = GridBagConstraints.LINE_START;
		gbcbt6.insets = new Insets(15,15,15,15);
		gbcbt6.fill = GridBagConstraints.HORIZONTAL; 
		btnCalculateAll = new JButton("Calculate \r\nAll");
		btnCalculateAll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculateAll();
			}
		});

		panBot.add(btnCalculateAll,gbcbt6);

		/*
		 * Button 7 - Generate values
		 */
		GridBagConstraints gbcbt7 = new GridBagConstraints();
		gbcbt7.gridx = gbcbt6.gridx+1;
		gbcbt7.gridy = gbcbt5.gridy;
		gbcbt7.anchor = GridBagConstraints.LINE_START;
		gbcbt7.insets = new Insets(15,15,15,15);
		gbcbt7.fill = GridBagConstraints.HORIZONTAL; 
		btnGenerate = new JButton("Generate");
		btnGenerate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generate();
			}
		});
		panBot.add(btnGenerate,gbcbt7);
		panBot.add(btnSave,gbcbt8);
		this.add(panBot,BorderLayout.PAGE_END);
		modBudget.fireTableDataChanged();
		/*
		 * Set dirty back to false so real changes are caught
		 */
		objParams.resetDirty();
	}
	/*
	 * When period is changed update dates to reflect new start and end dates
	 */
    public void changeDates() {
       GridBagConstraints con = new GridBagConstraints();
       if (boxPeriod.getSelectedItem().equals(Constants.PERIOD_FISCAL)) {
    	  jdtStartDate.setDate(jdtFiscalStart.getDate());
    	  jdtEndDate.setDate(jdtFiscalEnd.getDate());
       }
       else if (boxPeriod.getSelectedItem().equals(Constants.PERIOD_CALENDAR)) {
    	  jdtStartDate.setDate(jdtStartYear.getDate());
    	  jdtEndDate.setDate(jdtEndYear.getDate());	  	
      }
	  /*
	   * update budget lines with new start date
	   */
	  List<BudgetLine> listLines =objParams.getLines(); 
	  for (BudgetLine objBline: listLines){
		  objBline.setStartDate(jdtStartDate.getDateInt());
	  }
	  /*	
	   * set parameters
	   */
	  objParams.setStartDate(jdtStartDate.getDateInt());
	  objParams.setEndDate(jdtEndDate.getDateInt());
	  /*
	   * Remove and Add back dates to update panel
	   */
      panTop.remove(jdtStartDate);
      panTop.validate();
      con.gridwidth=1;
      con.gridheight=1;
      con.anchor = GridBagConstraints.FIRST_LINE_START;
      con.anchor = GridBagConstraints.FIRST_LINE_START;
	  con.insets = new Insets(10,10,10,10);
	  con.gridx = 1;
	  con.gridy = 1;
      jdtStartDate.setDisabledTextColor(Color.BLACK);
      panTop.add(jdtStartDate,con);
      jdtStartDate.setEnabled(false);
      panTop.remove(jdtEndDate);
      GridBagConstraints conend = new GridBagConstraints();
      conend.insets = new Insets(10,10,10,10);
      conend.gridx = 3;
      conend.gridy = 1;
      conend.anchor = GridBagConstraints.FIRST_LINE_START;
      conend.gridwidth=1;
      conend.gridheight=1;
      jdtEndDate.setDisabledTextColor(Color.BLACK);
      panTop.add(jdtEndDate,conend);
      jdtEndDate.setEnabled(false);
      panTop.validate();
      panTop.repaint();
      modBudget.fireTableDataChanged();
  }
	/*
	 * When period is changed update dates to reflect new start and end dates
	 */
    public void changeStartDate(PropertyChangeEvent e) {
       Calendar dtTemp = Calendar.getInstance();
       JDateField jdtStart = (JDateField)e.getSource();
       /*
        * do not do anything if date has not changed
        */
       if (jdtStart.parseDateInt() == objParams.getStartDate())
    	   return;
       dtTemp.setTime(jdtStart.getDate());
       dtTemp.add(Calendar.YEAR, 1);
       dtTemp.add(Calendar.DAY_OF_YEAR, -1);
   	  jdtEndDate.setDate(dtTemp.getTime());	  	
	  /*
	   * update budget lines with new start date
	   */
	  List<BudgetLine> listLines =objParams.getLines(); 
	  for (BudgetLine objBline: listLines){
		  objBline.setStartDate(jdtStartDate.getDateInt());
	  }
	  /*	
	   * set parameters
	   */
	  objParams.setStartDate(jdtStartDate.getDateInt());
	  objParams.setEndDate(jdtEndDate.getDateInt());
	  jdtEndDate.revalidate();
	  modBudget.fireTableDataChanged();
  }
  /*
   * when the number of years has been changed add/remove table columns (done in table model)
   * adjust screen sizes
   */
  private void changeYears() {
	 int iYears = boxYears.getSelectedIndex()+1;
	 modBudget.alterYears(iYears);
	 panTop.setSize(Constants.TOPWIDTH+(iYears-1)*100,Constants.TOPDEPTH);
	 panMid.setSize(Constants.MIDWIDTH+(iYears-1)*100,Constants.MIDDEPTH);
	 panBot.setSize(Constants.BOTWIDTH+(iYears-1)*100,Constants.BOTDEPTH);
	 JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
	 topFrame.setSize(Constants.FRAMEWIDTH+(iYears-1)*100,Constants.FRAMEDEPTH);
	 topFrame.invalidate();
	 topFrame.validate();
	 tabBudget.resetCombo();
	 
 }
  /*
   * Button actions
   * 
   * Add Accounts - creates new window of Expense Categories not in the Budget Lines
   */
  	private void addAccount() {
  		if (objParams.getMissing().isEmpty()) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No more categories to available");
			return;
  		}
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                showAddAccountsWindow();
                modBudget.fireTableDataChanged();
            }
        });
   	}
    /**
     * Display the add accounts window
     */
    private void showAddAccountsWindow() {

        //Create and set up the window.
        JFrame frame = new JFrame("MoneyDance Budget Generator - Add missing Categories");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(Constants.ADDSCREENWIDTH,Constants.ADDSCREENHEIGHT));
        panAcct = new AddAccountsWindow();
        frame.getContentPane().add(panAcct);

        //Display the window.
        frame.pack();
        frame.setVisible(true);

    }
    /*
     * Select all lines - sets Selected to true for all lines
     */
	private void selectAll () {
		for (BudgetLine objLine : objParams.getLines()) {
			objLine.setSelect(true);
		}
		modBudget.fireTableDataChanged();
	}
	/*
	 * Deselect all lines - sets Selected to false for all lines
	 */
	private void deselectAll () {
		for (BudgetLine objLine : objParams.getLines()) {
			objLine.setSelect(false);
		}
		modBudget.fireTableDataChanged();		
	}
	/*
	 * calculate selected lines - use Budget Parameters to do this work
	 */
	private void calculateSelected () {
		
		if (!objParams.calculateSelected())
		{
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No lines to calculate");			
		}
		modBudget.fireTableDataChanged();
	}
	/*
	 * calculate all lines - use Budget Parameters to do this work
	 */
	private void calculateAll () {
		if (!objParams.calculateAll()) 
		{
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No lines to calculate");			
		}		
		modBudget.fireTableDataChanged();
		
	}
	/*
	 * delete selected lines, asks for confirmation 
	 */
	private void deleteSelected () {
		JFrame fTemp = new JFrame();
		List<BudgetLine> listLines = objParams.getLines();
		/*
		 * determine if there are any lines to delete
		 */
		boolean bFound = false;
		for (BudgetLine objLine : listLines) {
			if (objLine.getSelect())
				bFound = true;
		}
		if (!bFound) {
			JOptionPane.showMessageDialog(fTemp,"No lines to delete");
			return;
		}
		int iResult = JOptionPane.showConfirmDialog(fTemp,"Are you sure you wish to delete the selected items?");
		if (iResult==JOptionPane.YES_OPTION) {	
			boolean bSelected = true;
			while (bSelected) {
				bSelected = false;
				for (int i=0; i<listLines.size();i++) {
					if (objParams.getItem(i).getSelect()) {
						objParams.deleteLine(i);
						bSelected = true;
						/*
						 * listLines has changed, need to reload and reiterate the list
						 */
						listLines = objParams.getLines();
						break;
					}
				}
			}
			objParams.setDirty(true);
			modBudget.fireTableDataChanged();		
		}
	}
	/*
	 * Generate Budget Items using lines  
	 */
	private void generate () {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                showGenerateWindow();
            }
        });	
		
		
	}
    /**
     * Display the generated transactions
     */
    private void showGenerateWindow() {

        //Create and set up the window.
        JFrame frame = new JFrame("MoneyDance Budget Generator - Generated Figures");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(Constants.GENSCREENWIDTH,Constants.GENSCREENHEIGHT));
        panGen = new GenerateWindow(boxYears.getSelectedIndex());
        frame.getContentPane().add(panGen);

        //Display the window.
        frame.pack();
        frame.setVisible(true);

    }
	/*
	 * Close the window checking for data changes first.
	 * Give user chance to save the data
	 */
	public void close () {
		if (objParams.isDirty()){
			JFrame fTemp = new JFrame();
			int iResult = JOptionPane.showConfirmDialog(fTemp,"The parameters have been changed.  Do you wish to save them?");
			if (iResult==JOptionPane.YES_OPTION) {
				objParams.saveParams();
			}
		}
		/*
		 * check if other windows are open
		 */
		if (panAcct != null) {
			panAcct.setVisible(false);
			JFrame AcctFrame = (JFrame) SwingUtilities.getWindowAncestor(panAcct);
			AcctFrame.dispose();
		}
		if (panGen != null) {
			panGen.setVisible(false);
			JFrame GenFrame = (JFrame) SwingUtilities.getWindowAncestor(panGen);
			GenFrame.dispose();
		}
		this.setVisible(false);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.dispose();
	}
	/*
	 * save parameters at user request
	 */
	private void save() {
		objParams.saveParams();
	}
}
