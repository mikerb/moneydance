package com.moneydance.modules.features.budgetgen;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.DefaultCellEditor;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import com.moneydance.awt.JDateField;

public class MyDateEditor extends DefaultCellEditor { 
	private static final long serialVersionUID = 1L;
	private JFormattedTextField ftf;
	
	@SuppressWarnings({"serial" })
	public MyDateEditor() {
	      super(new JFormattedTextField());
	      ftf = (JFormattedTextField)getComponent();
	
	
	      ftf.setHorizontalAlignment(JTextField.RIGHT);
	      ftf.setFocusLostBehavior(JFormattedTextField.PERSIST);
	      //React when the user presses Enter while the editor is
	      //active.  (Tab is handled as specified by
	      //JFormattedTextField's focusLostBehavior property.)
	      ftf.getInputMap().put(KeyStroke.getKeyStroke(
	                                      KeyEvent.VK_ENTER, 0),
	                                      "check");
	      ftf.getActionMap().put("check", new AbstractAction() {
	          public void actionPerformed(ActionEvent e) {
	          	if (validateDate((String)e.getSource())){ //The text is invalid.
	              	ftf.postActionEvent(); //inform the editor
	              } else try {              //The text is valid,
	                  ftf.commitEdit();     //so use it.
	                  ftf.postActionEvent(); //stop editing
	              } catch (java.text.ParseException exc) { }
	          }
	      });
	  }
	
	  //Override to invoke setValue on the formatted text field.
	  public Component getTableCellEditorComponent(JTable table,
	          Object value, boolean isSelected,
	          int row, int column) {
          JFormattedTextField ftf =
  	            (JFormattedTextField)super.getTableCellEditorComponent(
  	                table, value, isSelected, row, column);
	      ftf.setValue((String)value);
	      return ftf;
	  }
	
	  /*
	   * Override to ensure that the value remains a valid currency value based on
	   * Row currency type
	   */
	  public Object getCellEditorValue() {
	      JFormattedTextField ftf = (JFormattedTextField)getComponent();
	      return ftf.getValue();
	  }
	
	  //Override to check whether the edit is valid,
	  //setting the value if it is and complaining if
	  //it isn't.  If it's OK for the editor to go
	  //away, we need to invoke the superclass's version 
	  //of this method so that everything gets cleaned up.
	  public boolean stopCellEditing() {
	      JFormattedTextField ftf = (JFormattedTextField)getComponent();
	      if (validateDate((String)ftf.getText())) {
	          try {
	              ftf.commitEdit();
	          } catch (java.text.ParseException exc) {
	          	return false;
	          }
		    
	      } else { //text is invalid
		        return false; //don't let the editor go away
		    } 
	      return super.stopCellEditing();
	  }
	  /*
	   * validate text of field to determine if valid.
	   * 
	   * Field is in format {prefix}n.d{suffix}
	   * 
	   * {prefix} = prefix from CurrencyType
	   * {suffix} = suffix from CurrencyType
	   * n = integer of any number of digits (at least one)
	   * d = decimals to the number of decimal places from CurrencyType
	   */
	  private boolean validateDate(String ftf) {
		  @SuppressWarnings("unused")
		Date dtTemp;
		  try {
		      DateFormat formatter = new SimpleDateFormat(Main.cdate.getPattern());
		      formatter.setLenient(false);
		      dtTemp = formatter.parse(ftf);
		  } catch (ParseException e) { 
		      return false;
		  }
		  JDateField jdtTemp = new JDateField(Main.cdate);
		  jdtTemp.setDateInt(jdtTemp.getDateIntFromString(ftf));
		  if (jdtTemp.parseDateInt() < BudgetValuesWindow.objParams.getStartDate() ||
				  jdtTemp.parseDateInt() > BudgetValuesWindow.objParams.getEndDate()) {
				JFrame fTemp = new JFrame();
				JOptionPane.showMessageDialog(fTemp,"Date outside budget period");
				return false;
		  }
		  return true;
  }	
}
