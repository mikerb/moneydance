package com.moneydance.modules.features.budgetgen;


//import java.awt.Component;

//import javax.swing.DefaultCellEditor;
//import javax.swing.JCheckBox;
//import javax.swing.JComponent;
import javax.swing.JTable;
//import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
//import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;


public class GenerateTable extends JTable {
/*	private class RenderSelect extends JComponent implements TableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {
			if ((row & 1)==0) {
				JCheckBox boxTemp = new JCheckBox();
				boxTemp.setSelected((boolean)value);
				return boxTemp;
			}
			else
				return new JTextField(" ");
		}
		
	}
	RenderSelect objRender = new RenderSelect();*/
	public GenerateTable(GenerateTableModel model) {
		super(model);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		/*
		 * Select
		 */
		TableColumn colSelect = this.getColumnModel().getColumn(0);
//		colSelect.setPreferredWidth(70);
//		colSelect.setMinWidth(40);
//		colSelect.setCellRenderer(objRender);
//		colSelect.setCellEditor(new DefaultCellEditor(new JCheckBox()));
		/*
		 * category
		 */
//		this.getColumnModel().getColumn(1).setResizable(false);
//		this.getColumnModel().getColumn(1).setPreferredWidth(200);
//		this.getColumnModel().getColumn(1).setMinWidth(100);
		/*
		 * Amount values
		 */
		for (int i=0;i<model.getColumnCount();i++) {
			colSelect = this.getColumnModel().getColumn(i);
			colSelect.setPreferredWidth(Constants.GENAMOUNTWIDTH);
			colSelect.setMinWidth(Constants.GENAMOUNTWIDTH);
			colSelect.setResizable(false);
		}
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	}

}
