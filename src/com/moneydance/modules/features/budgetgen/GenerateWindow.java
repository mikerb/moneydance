package com.moneydance.modules.features.budgetgen;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.moneydance.apps.md.controller.DateRange;
/*
 * Create Generate Window, contains BudgetLines that have an Amount
 */
public class GenerateWindow extends JPanel {
	private GenerateTable tabGen;
	private GenerateTableModel modGen;
	private GenerateTableHeader tabGenHead;
	private GenerateTableHeaderModel modGenHead;
	private List<BudgetLine> listBudgetLines;
	/*
	 * screen fields
	 */
	private JButton btnSave;
	private JButton btnClose;
	private JCheckBox cbDelete;
	private JComboBox<String> boxYears;
	private JPanel panTop;
	private JPanel panMid;
	private JPanel panBot;
	private JScrollPane spMid;
	
	public GenerateWindow(int iYears) {
		super();
		/*
		 * first set up table model so it contains data
		 */
		modGen = new GenerateTableModel();
		modGenHead = new GenerateTableHeaderModel();
		modGen.setYear(1);
		BudgetValuesWindow.objParams.generate(modGen.getPeriods());
		listBudgetLines = BudgetValuesWindow.objParams.getLines();
		for ( BudgetLine objLine :listBudgetLines){
			if (objLine.getAmount() != 0) {
				modGen.AddLine(objLine);
				modGenHead.AddLine(objLine);
			}
		}
		/*
		 * now create table
		 */
		tabGen = new GenerateTable(modGen);
		tabGenHead = new GenerateTableHeader(modGenHead);
		/*
		 * start of screen
		 */
		/*
		 * Top panel
		 */
		panTop = new JPanel(new GridBagLayout());
		panTop.setPreferredSize(new Dimension(Constants.GENTOPWIDTH,Constants.GENTOPDEPTH));
		/*
		 * year to display
		 */
		JLabel lblYearsl = new JLabel("Year to Display:");
		lblYearsl.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblYearsl = new GridBagConstraints();
		gbc_lblYearsl.insets = new Insets(10,10,10,10);
		gbc_lblYearsl.gridx = 0;
		gbc_lblYearsl.gridy = 0;
		gbc_lblYearsl.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(lblYearsl, gbc_lblYearsl);
		
		boxYears = new JComboBox<String>();
		boxYears.setModel(new DefaultComboBoxModel<String>(new String[] {"1", "2", "3"}));
		boxYears.setSelectedIndex(0);
		GridBagConstraints gbc_boxYears = new GridBagConstraints();
		gbc_boxYears.insets = new Insets(10,10,10,10);
		gbc_boxYears.gridx = 1;
		gbc_boxYears.gridy = 0;
		gbc_boxYears.anchor = GridBagConstraints.FIRST_LINE_START;
		panTop.add(boxYears, gbc_boxYears);
		boxYears.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				changeYears();
			}
	    });

		this.add(panTop);

		/*
		 * Middle panel - table
		 */

		panMid = new JPanel();
		spMid = new JScrollPane();
		spMid.getViewport().add(tabGen);
		int iHeaderWidth = Constants.GENCATPREFWIDTH+Constants.GENSELECTPREFWIDTH;
		int iDataWidth = Constants.GENMIDWIDTH-iHeaderWidth-50;
		spMid.getViewport().setPreferredSize(new Dimension(iDataWidth,Constants.GENMIDDEPTH-100));
		JViewport vpHeader = new JViewport();
		vpHeader.add(tabGenHead);
		vpHeader.setPreferredSize(new Dimension(iHeaderWidth,Constants.GENMIDDEPTH-100));
		spMid.setRowHeader(vpHeader);
		spMid.setCorner(JScrollPane.UPPER_LEFT_CORNER, tabGenHead.getTableHeader());
		panMid.add(spMid);
		spMid.setPreferredSize(new Dimension(Constants.GENMIDWIDTH,Constants.GENMIDDEPTH));
		this.add(panMid);
		/*
		 * Bottom Panel - buttons
		 */
		panBot = new JPanel(new GridBagLayout());
		panBot.setPreferredSize(new Dimension(Constants.GENBOTWIDTH,Constants.GENBOTDEPTH));

		/*
		 * Button 1
		 */
		GridBagConstraints gbcbt1 = new GridBagConstraints();
		gbcbt1.gridx = 0;
		gbcbt1.gridy = 0;
		gbcbt1.anchor = GridBagConstraints.LINE_START;
		gbcbt1.insets = new Insets(15,15,15,15);
		gbcbt1.fill = GridBagConstraints.HORIZONTAL; 
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		panBot.add(btnClose,gbcbt1);

		/*
		 * Button 2
		 */
		GridBagConstraints gbcbt2 = new GridBagConstraints();
		gbcbt2.gridx = gbcbt1.gridx+1;
		gbcbt2.gridy = gbcbt1.gridy;
		gbcbt2.anchor = GridBagConstraints.LINE_START;
		gbcbt2.insets = new Insets(15,15,15,15);
		gbcbt2.fill = GridBagConstraints.HORIZONTAL; 
		btnSave = new JButton("Save Selected Values");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		panBot.add(btnSave,gbcbt2);
		/*
		 * Check Box
		 */
		GridBagConstraints gbccb1 = new GridBagConstraints();
		gbccb1.gridx = gbcbt2.gridx+1;
		gbccb1.gridy = gbcbt2.gridy;
		gbcbt2.anchor = GridBagConstraints.LINE_START;
		gbcbt2.insets = new Insets(15,15,15,15);
		gbcbt2.fill = GridBagConstraints.HORIZONTAL; 
		cbDelete = new JCheckBox("Delete Current Values");
		cbDelete.setSelected(false);
		panBot.add(cbDelete,gbccb1);
		
		this.add(panBot);
		panBot.setPreferredSize(new Dimension(Constants.GENBOTWIDTH,Constants.GENBOTDEPTH));
		this.setPreferredSize(new Dimension(Constants.GENSCREENWIDTH,Constants.GENSCREENHEIGHT));
	}
	public void close () {
		this.setVisible(false);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.dispose();
	}
	/*
	 * Save selected lines
	 */
	private void save() {
		DateRange[] drPeriods = modGen.getPeriods();
		DateRange[] drYear = new DateRange[drPeriods.length];
		/*
		 * set date ranges based on year
		 */
		for (int i = 0;i<drYear.length;i++) {
			drYear[i] = new DateRange(drPeriods[i].getStartDateInt()+(boxYears.getSelectedIndex())*10000,
					drPeriods[i].getEndDateInt()+(boxYears.getSelectedIndex())*10000);
		}			
		/*
		 * Cycle through displayed lines - if Selected is true process Generated amount
		 * 
		 * Gen amt  Cur amt  Add  Update  Delete
		 *  != 0      = 0	  X
		 *  != 0 	 != 0           X
		 *  = 0      != 0                   X(a)
		 *  
		 *  
		 *  (a) only delete if flag set to delete
		 */
		for (int i = 0;i < modGen.getRowCount()/2;i++) {
			if ((boolean) modGenHead.getValueAt(i*2, 0)) {
				long[] arrGen = modGen.getGeneratedValues(i);
				long[] arrCur = modGen.getCurrentValues(i);
				for (int j=0;j<drYear.length;j++) {
					if (arrCur[j] == 0) {
						if (arrGen[j] != 0) {
							BudgetValuesWindow.objBudget.createItem(modGenHead.getCategoryObj(i),drYear[j],arrGen[j]);
						}
					}
					else
						if (arrGen[j] == 0) {
							if (cbDelete.isSelected())
								BudgetValuesWindow.objBudget.deleteItem(modGenHead.getCategoryObj(i),drYear[j]);
						}
						else {
							BudgetValuesWindow.objBudget.updateItem(modGenHead.getCategoryObj(i),drYear[j],arrGen[j]);
						}
				}
			}
		}
		modGen.reloadCurrent();
		modGen.fireTableDataChanged();
		BudgetValuesWindow.objBudget.reloadItems();
		
	}
	private void changeYears() {
		modGen.setYear(boxYears.getSelectedIndex()+1);
		for (int i = 0; i < modGen.getColumnCount(); i++) {
			  String name = modGen.getColumnName(i);
			  int viewIdx = tabGen.convertColumnIndexToView(i);
			  tabGen.getColumnModel().getColumn(viewIdx).setHeaderValue(name);
			}
		modGen.fireTableDataChanged();
		panMid.revalidate();
	}

}
