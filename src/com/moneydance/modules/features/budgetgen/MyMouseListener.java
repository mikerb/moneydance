package com.moneydance.modules.features.budgetgen;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.moneydance.awt.DatePicker;
import com.moneydance.awt.JDateField;


public class MyMouseListener
	implements MouseListener{
	DatePicker picker;
	JDateField jdtCurrent = new JDateField(Main.cdate);
	public MyMouseListener() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void mouseClicked(MouseEvent e) {
//		 picker = new DatePicker(jdtCurrent.parseDateInt());
		  picker.setListener(new ChangeListener() {
		    public void stateChanged(ChangeEvent evt) {
		      int newDate = picker.getSelectedDate();
		      if(newDate>0) {
		        jdtCurrent.setDateInt(newDate);
		      }
		    }
		  });
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
