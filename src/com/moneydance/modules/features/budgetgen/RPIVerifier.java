/*
 * Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.budgetgen;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class RPIVerifier extends InputVerifier {

	@Override
	public boolean verify(JComponent input) {
		Double dTemp;
		if (input instanceof JFormattedTextField) {
	       JFormattedTextField ftf = (JFormattedTextField)input;
           String text = ftf.getText();
          try {
              dTemp = Double.parseDouble(text);
              } catch (NumberFormatException pe) {
            	  if (text.endsWith("%"))
            		  text = text.substring(0,text.length()-1);
            	  	  try {
            	  		  dTemp = Double.parseDouble(text);
                      } catch (NumberFormatException pe2) {  
                    	  JFrame fTemp = new JFrame();
                    	  JOptionPane.showMessageDialog(fTemp,
      					    "Invalid RPI amount",
      					    "Input Error",
      					    JOptionPane.ERROR_MESSAGE);
                    	  return false;
                      }
              }
          	  if (dTemp < -100.00 || dTemp >100.00 ) {
     				JFrame fTemp = new JFrame();
    				JOptionPane.showMessageDialog(fTemp,
    					    "RPI amount must be within -100 to +100",
    					    "Input Error",
    					    JOptionPane.ERROR_MESSAGE);
    				return false;
               }
               return true;
		}
	       return true;
	}
	public boolean shouldYieldFocus(JComponent input) {
	    return verify(input);
	}

}
