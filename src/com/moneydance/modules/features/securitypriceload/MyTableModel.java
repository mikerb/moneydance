package com.moneydance.modules.features.securitypriceload;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;

import javax.swing.table.DefaultTableModel;

import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.CurrencyType;
import com.moneydance.util.CustomDateFormat;


public class MyTableModel extends DefaultTableModel {
	private CustomDateFormat cdate = new CustomDateFormat("dd/mm/yyyy");
    private SortedMap<String,Double> mapPrices;
    private List<Entry<String,Integer>> listDates;
    private List<Entry<String,Account>> listAccounts;
    private List<Entry<String,Double>> listCurrent;
	private boolean[] arrSelect;
	private static String[] arrColumns = {"Select","Ticker","Name","Last Update","Last Price","New Price"};

	public MyTableModel(SortedMap<String,Double> mapPricesp,SortedMap<String, Double> mapCurrentp,
			SortedMap<String,Integer> mapDatesp, SortedMap<String,Account> mapAccountsp){
		super();
		mapPrices = mapPricesp;
		listCurrent = new ArrayList<Entry<String,Double>>(mapCurrentp.entrySet());
		listDates = new ArrayList<Entry<String,Integer>>(mapDatesp.entrySet());
		listAccounts = new ArrayList<Entry<String,Account>>(mapAccountsp.entrySet());
		arrSelect = new boolean[mapCurrentp.size()];
		for (int i=0;i<arrSelect.length;i++)
			arrSelect[i] = false;
	}
	public void ResetData(SortedMap<String, Double> mapCurrentp,
			SortedMap<String,Integer> mapDatesp, SortedMap<String,Account> mapAccountsp){
		listCurrent = new ArrayList<Entry<String,Double>>(mapCurrentp.entrySet());
		listDates = new ArrayList<Entry<String,Integer>>(mapDatesp.entrySet());
		listAccounts = new ArrayList<Entry<String,Account>>(mapAccountsp.entrySet());
		for (int i=0;i<arrSelect.length;i++)
			arrSelect[i] = false;
	}
	@Override
	public int getRowCount() {
		if (listCurrent == null)
			return 0;
		return (listCurrent.size());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getColumnClass(int c){
		if (c == 0)
			return Boolean.class;
		return String.class;
	}

		@Override
	public int getColumnCount() {
			return 6;
	}	
	@Override
	public String getColumnName(int c) {
		return arrColumns[c];
	}
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		DecimalFormat dfNumbers = new DecimalFormat("#0.0000");
		switch (columnIndex) {
		case 0:
			return  arrSelect[rowIndex];
		case 1:
			return  listCurrent.get(rowIndex).getKey();
		case 2:
			return  listAccounts.get(rowIndex).getValue().getAccountName();
		case 3:
			return  cdate.format(listDates.get(rowIndex).getValue());
		case 4:
			return  dfNumbers.format(listCurrent.get(rowIndex).getValue());
		default:
			if (mapPrices.get(listCurrent.get(rowIndex).getKey())== null)
				return "0.0";
			return dfNumbers.format(mapPrices.get(listCurrent.get(rowIndex).getKey()));
		}
	}
	@Override
    public boolean isCellEditable(int row, int col) {
        /*
         * Only Select, amount, period, start date and RPI are editable
         * Category, Year 1, Year 2, Year 3 are not
         */
		if (col ==0)
			return true;
		else
			return false;
    }
	@Override
	public void setValueAt(Object value, int row, int col){
		DecimalFormat dfNumbers = new DecimalFormat("#0.0000");
		if (value == null)
			return;
		if (col ==0)
			if (mapPrices.get(listCurrent.get(row).getKey())== null)
				return;
			if (dfNumbers.format(mapPrices.get(listCurrent.get(row).getKey())).equals("0.0"))
					return;
			arrSelect [row] = (boolean) value;
	}
	/*
	 * update line
	 */
	public boolean updateLine(int iRow,int iAsOfDate) {
		Account acct;
		CurrencyType ctTicker;
		if (!arrSelect[iRow])
			return false; // line not selected - do not process
		if (mapPrices.get(listCurrent.get(iRow).getKey())== null)
			return false; // no new price for line - do not process
		acct = listAccounts.get(iRow).getValue();
		ctTicker = acct.getCurrencyType();
		if (ctTicker == null)
			return false;  // no currency - do not process
		Double dRate = 1.0/mapPrices.get(listCurrent.get(iRow).getKey());
		ctTicker.setSnapshotInt(iAsOfDate,dRate);
		ctTicker.setUserRate(dRate);
		arrSelect[iRow] = false;
		return true;
	}
	/*
	 * Reload current prices
	 */
	public void reloadPrices () {
		Account acct;
		CurrencyType ctTicker;
		for (int i=0;i<listCurrent.size();i++) {
			acct = listAccounts.get(i).getValue();
	    	ctTicker = acct.getCurrencyType();
	    	/*
	    	 * Get last price entry
	    	 */
	    	if (ctTicker != null) {
	    	  if (!ctTicker.getTickerSymbol().equals("")) {
		    	  int iSnapIndex = ctTicker.getSnapshotCount()-1;
		    	  CurrencyType.Snapshot ctssLast = ctTicker.getSnapshot(iSnapIndex);
		    	  if (ctssLast != null) {
		    		  listCurrent.get(i).setValue(1.0/ctssLast.getUserRate());
		    		  }
	    		  }
	    	  }

		}
	}
}
