package com.moneydance.modules.features.securitypriceload;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.CurrencyType;
import com.moneydance.apps.md.model.SecurityAccount;
import com.moneydance.awt.JDateField;
import com.moneydance.util.CustomDateFormat;

public class loadPricesWindow extends JPanel {
    private SortedMap<String,Double> mapPrices;
    private SortedMap<String, Double> mapCurrent;
    private SortedMap<String,Integer> mapDates; 
    private SortedMap<String,Account> mapAccounts;
    private MyTableModel modPrices;
    private MyTable tabPrices;
    JScrollPane spPrices;
    JPanel panBot;
    JPanel panTop;
    JPanel panMid;
    JButton btnClose;
    JButton btnSave;
    Parameters objParms;
    JDateField jdtAsOfDate;
	JCheckBox jcSelect;
	public loadPricesWindow(JTextField txtFileName,Parameters objParmsp)
 {
	 	objParms = objParmsp;
		mapPrices = new TreeMap<String, Double> ();
		mapCurrent = new TreeMap<String, Double> ();
		mapDates = new TreeMap<String, Integer> ();
		mapAccounts = new TreeMap<String, Account> ();
		loadFile (txtFileName);
		loadAccounts(Main.context.getRootAccount());
		modPrices = new MyTableModel (mapPrices, mapCurrent, mapDates, mapAccounts);
		tabPrices = new MyTable (modPrices);
		/*
		 * Start of screen
		 * 
		 * Top Panel date
		 */
		this.setLayout(new BorderLayout());
		panTop = new JPanel (new GridBagLayout());
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		JLabel lbAsOfDate = new JLabel("As of Date for new prices:");
		panTop.add(lbAsOfDate,gbc_label);
		GridBagConstraints gbc_date = new GridBagConstraints();
		gbc_date.gridx = 0;
		gbc_date.gridy = 1;
		jdtAsOfDate = new JDateField(new CustomDateFormat("dd/mm/yyyy"));
		panTop.add(jdtAsOfDate,gbc_date);
		this.add(panTop,BorderLayout.PAGE_START);
		/*
		 * Middle Panel table
		 */
		panMid = new JPanel (new BorderLayout());
		spPrices = new JScrollPane (tabPrices);
		panMid.add(spPrices,BorderLayout.CENTER);
		jcSelect = new JCheckBox();
		jcSelect.setAlignmentX(LEFT_ALIGNMENT);
		jcSelect.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				boolean bNewValue;
				if (e.getStateChange() == ItemEvent.DESELECTED)
					bNewValue = false;
				else
					bNewValue = true;
				for (int i=0;i<modPrices.getRowCount();i++) {
					if ((String)modPrices.getValueAt(i,5) != "0.0")
						modPrices.setValueAt(bNewValue, i, 0);
				}
				modPrices.fireTableDataChanged();
			}
		});
		panMid.add(jcSelect,BorderLayout.PAGE_END);
		this.add(panMid,BorderLayout.CENTER);
		/*
		 * Add Buttons
		 */
		panBot = new JPanel(new GridBagLayout());
		/*
		 * Button 1
		 */
		GridBagConstraints gbcbt1 = new GridBagConstraints();
		gbcbt1.gridx = 0;
		gbcbt1.gridy = 0;
		gbcbt1.anchor = GridBagConstraints.LINE_START;
		gbcbt1.insets = new Insets(15,15,15,15);
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
		panBot.add(btnClose,gbcbt1);

		/*
		 * Button 2
		 */
		GridBagConstraints gbcbt2 = new GridBagConstraints();
		gbcbt2.gridx = gbcbt1.gridx+1;
		gbcbt2.gridy = gbcbt1.gridy;
		gbcbt2.insets = new Insets(15,15,15,15);
		btnSave = new JButton("Save Selected Values");
		btnSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		panBot.add(btnSave,gbcbt2);
		
		this.add(panBot,BorderLayout.PAGE_END);
		

	}
	/*
	 * Create 3 tables of current rate, date rate set and account object
	 * all keyed by ticker symbol
	 */
	 private void loadAccounts(Account parentAcct) {
	    int sz = parentAcct.getSubAccountCount();
	    for(int i=0; i<sz; i++) {
	      Account acct = parentAcct.getSubAccount(i);
	      if (acct instanceof SecurityAccount) {
		      if((acct.getCurrentBalance() != 0L)||(objParms.getZero())){
		    	  CurrencyType ctTicker = acct.getCurrencyType();
		    	  /*
		    	   * Get last price entry
		    	   */
		    	  if (ctTicker != null) {
		    		  if (!ctTicker.getTickerSymbol().equals("")) {
			    		  int iSnapIndex = ctTicker.getSnapshotCount()-1;
			    		  if (iSnapIndex < 0)
					    	  mapCurrent.put(ctTicker.getTickerSymbol(),1.0);
			    		  else {
					    	  CurrencyType.Snapshot ctssLast = ctTicker.getSnapshot(iSnapIndex);
					    	  if (ctssLast != null) {
						    	  mapCurrent.put(ctTicker.getTickerSymbol(),1.0/ctssLast.getUserRate());
					    	  }
				    	  mapDates.put(ctTicker.getTickerSymbol(), ctssLast.getDateInt());
				    	  mapAccounts.put(ctTicker.getTickerSymbol(), acct);
			    		  }
		    		  }
		    	  }
		      }
	      }
	      loadAccounts(acct);
	    }
	 }
	 /*
	  * try to load selected file
	  */
	 private void loadFile(JTextField txtFileName) {
		 	int iTicker=0;
		 	int iPrice=0;
		 	String strExchange;
			String strTicker; 
			try {
				FileReader frPrices = new FileReader(txtFileName.getText());
				BufferedReader brPrices = new BufferedReader(frPrices);
				/*
				 * Get the headers
				 */
				String strLine = brPrices.readLine(); 
				String [] arColumns = strLine.split(",");
				for (int i = 0;i<arColumns.length;i++) {
					if (arColumns[i].equals(objParms.getTicker()))
							iTicker = i;
					if (arColumns[i].equals(objParms.getPrice()))
							iPrice = i;
				}
				while ((strLine = brPrices.readLine())!= null) {
					arColumns = splitString(strLine);
					strExchange = "";
					if (objParms.getExch()) {
						strTicker = arColumns[iTicker];
						int iPeriod = strTicker.indexOf('.');
						if (iPeriod > -1) {
							arColumns[iTicker] = strTicker.substring(0,iPeriod);
							strExchange = strTicker.substring(iPeriod+1);
						}
						else {
							iPeriod = strTicker.indexOf(':');
							if (iPeriod > -1) {
								arColumns[iTicker] = strTicker.substring(0,iPeriod);
								strExchange = strTicker.substring(iPeriod+1);								
							}
						}
							
					}
					else {
						strTicker = arColumns[iTicker];
						int iPeriod = strTicker.indexOf('.');
						if (iPeriod > -1) {
							strExchange = strTicker.substring(iPeriod+1);
						}
						else {
							iPeriod = strTicker.indexOf(':');
							if (iPeriod > -1) {
								strExchange = strTicker.substring(iPeriod+1);								
							}
						}						
					}
						
					/*
					 * Amount maybe different to Moneydance, use multiplier
					 */
					double dAmount = 0;
					dAmount = Double.parseDouble(arColumns[iPrice])*Math.pow(10D,objParms.getMultiplier(strExchange));
					mapPrices.put(arColumns[iTicker], dAmount);
				}
				brPrices.close();
			}
			catch (FileNotFoundException e) {
				JFrame fTemp = new JFrame();
				JOptionPane.showMessageDialog(fTemp,"File "+txtFileName+" not Found");
				close();
			}
			catch (IOException e) {
				JFrame fTemp = new JFrame();
				JOptionPane.showMessageDialog(fTemp,"I//O Error whilst reading "+txtFileName);
				close();
				
			}
			
	 }
	 
	 public void close() {
		this.setVisible(false);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.dispose();		  
	 }
	 private void save() {
		 int iRows = modPrices.getRowCount();
		 boolean bUpdated = false;
		 int iRowCount = 0;
		 for (int i=0;i<iRows;i++){
			 if(modPrices.updateLine(i,jdtAsOfDate.getDateInt())) {
				 bUpdated = true;
				 iRowCount++;
			 }
		 }
		 if (bUpdated){
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,iRowCount+" prices updated");
			/*
			 * Clear current account data and reload before redisplaying
			 * the table
			 */
			mapCurrent.clear();
			mapDates.clear();
			mapAccounts.clear();
			loadAccounts(Main.context.getRootAccount());
			modPrices.ResetData(mapCurrent, mapDates, mapAccounts);
			modPrices.fireTableDataChanged();
			this.revalidate();
		 }
	 }	  /*
	   * Utility method to split a string containing both " and ,
	   */
	  
	  private String[] splitString(String strInput) {
		  List<String> listParts = new ArrayList<String>();
		  int i=0;
		  String strPart = "";
		  boolean bString = false;
		  while(i<strInput.length()) {
			switch (strInput.substring(i, i+1)) {
			case "\"" : 
				if (bString) {
					bString = false;
				}
				else
					bString = true;
				break;
			case "," :
				if (!bString) {
					listParts.add(strPart);
					strPart = "";
				}
				break;
			default :
				strPart += strInput.substring(i, i+1);
			}
			i++;
		  }
		  listParts.add(strPart);
		  String[] arrString = new String[listParts.size()];
		  return listParts.toArray(arrString);
	  }
}
