package com.moneydance.modules.features.securitypriceload;

import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;


import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.GridBagConstraints;

import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class FileSelectWindow extends JPanel{
	  private static final long serialVersionUID = 1L;
	  private JTextField txtFileName;
	  private JComboBox<Integer> cbMultiplier;
	  private JFileChooser objFileChooser;
	  private File fSecurities;
	  private loadPricesWindow objLoadWindow;
	  private JComboBox<String> cbTicker;
	  private JCheckBox chbZero;
	  private JCheckBox chbExch;
	  private JComboBox<String> cbPrice;
	  private JPanel panMult;
	  private JButton btnAdd;
	  private Parameters objParms;
	  private List<ExchangeLine> listLines;

	  private String [] arColumns;
	  private Map<Object,String> mapCombos;
	  private Map<Object,String> mapDelete;
	public FileSelectWindow() throws HeadlessException {
	    try {
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException | ClassNotFoundException e) {}
	    objFileChooser = new JFileChooser();		
		GridBagLayout gbl_panel = new GridBagLayout();
		this.setLayout(gbl_panel);
		
		JLabel lblFileName = new JLabel("File Name : ");
		GridBagConstraints gbc_lblFileName = new GridBagConstraints();
		gbc_lblFileName.insets = new Insets(10, 10, 10, 10);
		gbc_lblFileName.gridx = 0;
		gbc_lblFileName.gridy = 0;
		this.add(lblFileName, gbc_lblFileName);
		
		txtFileName = new JTextField();
		GridBagConstraints gbc_txtFileName = new GridBagConstraints();
		gbc_txtFileName.insets = new Insets(10, 10, 10, 10);
		gbc_txtFileName.gridx = 1;
		gbc_txtFileName.gridy = 0;
		gbc_txtFileName.gridwidth=3;
		txtFileName.setColumns(50);
		this.add(txtFileName, gbc_txtFileName);
				
		JButton btnChoose = new JButton("Choose File");
		GridBagConstraints gbc_btnChoose = new GridBagConstraints();
		gbc_btnChoose.insets = new Insets(10, 10, 10, 10);
		gbc_btnChoose.gridx = 4;
		gbc_btnChoose.gridy = 0;
		this.add(btnChoose, gbc_btnChoose);
		btnChoose.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFile();
			}
		});

		JLabel lblLTicker = new JLabel("Select Ticker Field");		
		GridBagConstraints gbc_lblLTicker = new GridBagConstraints();
		gbc_lblLTicker.insets = new Insets(10, 10, 10, 10);
		gbc_lblLTicker.gridx = 0;
		gbc_lblLTicker.gridy = 2;
		this.add(lblLTicker, gbc_lblLTicker);	
		cbTicker = new JComboBox<String>();
		GridBagConstraints gbc_cbLTicker = new GridBagConstraints();
		gbc_cbLTicker.insets = new Insets(10, 10, 10, 10);
		gbc_cbLTicker.gridx = 1;
		gbc_cbLTicker.gridy = 2;
		gbc_cbLTicker.anchor = GridBagConstraints.LINE_START;
		cbTicker.addItem("Please Select a Field");
		cbTicker.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbTick = (JComboBox<Integer>) e.getSource();
				objParms.setTicker((String)cbTick.getSelectedItem());
			}
		});
		this.add(cbTicker, gbc_cbLTicker);	
		JLabel lblLExch = new JLabel("Remove Exchange from Ticker?");
		GridBagConstraints gbc_cbLExch = new GridBagConstraints();
		gbc_cbLExch.insets = new Insets(10, 10, 10,0);
		gbc_cbLExch.gridx =2;
		gbc_cbLExch.gridy = 2;
		gbc_cbLExch.anchor = GridBagConstraints.LINE_END;
		this.add(lblLExch, gbc_cbLExch );	
		chbExch = new JCheckBox();
		GridBagConstraints gbc_chbExch = new GridBagConstraints();
		gbc_chbExch.insets = new Insets(10, 0, 10, 10);
		gbc_chbExch.gridx = 3;
		gbc_chbExch.gridy = 2;
		gbc_chbExch.anchor = GridBagConstraints.LINE_START;
		chbExch.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox chbExchT = (JCheckBox) e.getSource();
				objParms.setExch(chbExchT.isSelected());
			}
		});
		this.add(chbExch, gbc_chbExch);	
		JLabel lblLPrice = new JLabel("Select Price Field");		
		GridBagConstraints gbc_lblLPrice = new GridBagConstraints();
		gbc_lblLPrice.insets = new Insets(10, 10, 10, 10);
		gbc_lblLPrice.gridx =0;
		gbc_lblLPrice.gridy = 3;
		this.add(lblLPrice, gbc_lblLPrice);	
		cbPrice = new JComboBox<String>();
		GridBagConstraints gbc_cbLPrice = new GridBagConstraints();
		gbc_cbLPrice.insets = new Insets(10, 10, 10, 10);
		gbc_cbLPrice.gridx = 1;
		gbc_cbLPrice.gridy = 3;
		gbc_cbLPrice.anchor = GridBagConstraints.LINE_START;
		cbPrice.addItem("Please Select a Field");
		cbPrice.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbPric = (JComboBox<Integer>) e.getSource();
				objParms.setPrice((String)cbPric.getSelectedItem());
			}
		});
		this.add(cbPrice, gbc_cbLPrice);	
		JLabel lblLZero = new JLabel("Include zero accounts?");
		GridBagConstraints gbc_cbLZero = new GridBagConstraints();
		gbc_cbLZero.insets = new Insets(10, 10, 10,0);
		gbc_cbLZero.gridx =2;
		gbc_cbLZero.gridy = 3;
		gbc_cbLZero.anchor = GridBagConstraints.LINE_END;
		this.add(lblLZero, gbc_cbLZero );	
		chbZero = new JCheckBox();
		GridBagConstraints gbc_chbZero = new GridBagConstraints();
		gbc_chbZero.insets = new Insets(10, 0, 10, 10);
		gbc_chbZero.gridx = 3;
		gbc_chbZero.gridy = 3;
		gbc_chbZero.anchor = GridBagConstraints.LINE_START;
		chbZero.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox chbZeroT = (JCheckBox) e.getSource();
				objParms.setZero(chbZeroT.isSelected());
			}
		});
		this.add(chbZero, gbc_chbZero);
		JLabel lblMultiplier = new JLabel("Select multiplier(s) for Prices");
		GridBagConstraints gbc_lblLMultiplier = new GridBagConstraints();
		gbc_lblLMultiplier.insets = new Insets(10, 10, 10, 10);
		gbc_lblLMultiplier.gridx =0;
		gbc_lblLMultiplier.gridy = 4;
		this.add(lblMultiplier, gbc_lblLMultiplier);	
		JLabel lblMulttxt = new JLabel("e.g: -2 = * by 0.01, +2 = * by 100");
		GridBagConstraints gbc_lblLMulttxt = new GridBagConstraints();
		gbc_lblLMulttxt.insets = new Insets(10, 10, 10, 10);
		gbc_lblLMulttxt.gridx =1;
		gbc_lblLMulttxt.gridy = 4;
		this.add(lblMulttxt, gbc_lblLMulttxt);	
		JLabel lblDefault = new JLabel("Default");
		GridBagConstraints gbc_lblLDefault = new GridBagConstraints();
		gbc_lblLDefault.insets = new Insets(10, 10, 10, 10);
		gbc_lblLDefault.gridx =0;
		gbc_lblLDefault.gridy = 5;
		gbc_lblLDefault.anchor = GridBagConstraints.LINE_END;
		this.add(lblDefault, gbc_lblLDefault );	
		cbMultiplier = new JComboBox<Integer>(Parameters.arMultipliers);
		GridBagConstraints gbc_cbMultiplier = new GridBagConstraints();
		gbc_cbMultiplier.insets = new Insets(10, 10, 10, 10);
		gbc_cbMultiplier.gridx = 1;
		gbc_cbMultiplier.gridy = 5;
		gbc_cbMultiplier.anchor = GridBagConstraints.LINE_START;
		cbMultiplier.setSelectedIndex(4);
		cbMultiplier.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbMult = (JComboBox<Integer>) e.getSource();
				objParms.setDefaultMult(cbMult.getSelectedIndex());
			}
		});	
		this.add(cbMultiplier, gbc_cbMultiplier );
		panMult = new JPanel(new GridBagLayout());
		GridBagConstraints gbc_panMult = new GridBagConstraints();
		gbc_panMult.insets = new Insets(10, 10, 10, 10);
		gbc_panMult.gridx = 0;
		gbc_panMult.gridy = 6;
		gbc_panMult.gridwidth=2;
		this.add(panMult,gbc_panMult);
		JButton btnSave = new JButton("Save Parameters");
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.insets = new Insets(10, 10, 10, 10);
		gbc_btnSave.gridx = 1;
		gbc_btnSave.gridy = 7;
		this.add(btnSave, gbc_btnSave);
		btnSave.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				objParms.save();
			}
		});
		JButton btnLoad = new JButton("Load Data");
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.insets = new Insets(10, 10, 10, 10);
		gbc_btnLoad.gridx = 2;
		gbc_btnLoad.gridy = 7;
		this.add(btnLoad, gbc_btnLoad);
		btnLoad.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadData();
			}
		});
		
		
		JButton btnClose = new JButton("Close");
		GridBagConstraints gbc_btnClose = new GridBagConstraints();
		gbc_btnClose.insets = new Insets(10, 10, 10, 10);
		gbc_btnClose.gridx = 3;
		gbc_btnClose.gridy = 7;
		this.add(btnClose, gbc_btnClose);
		btnClose.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
	}
	private void chooseFile() {
		objFileChooser.setFileFilter(new FileNameExtensionFilter("csv","CSV"));
		int iReturn = objFileChooser.showDialog(this, "Open File");
		if (iReturn == JFileChooser.APPROVE_OPTION) {
			fSecurities = objFileChooser.getSelectedFile();
			txtFileName.setText(fSecurities.getAbsolutePath());
		}
		objParms = new Parameters();
		loadFields();
	}
	private void loadFields() {
		FileReader frPrices;
		BufferedReader brPrices;
		if (txtFileName.getText().equals("")) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"Please Select a file first");
			return;
		}
		try {
			frPrices = new FileReader(txtFileName.getText());
			brPrices = new BufferedReader(frPrices);
			/*
			 * Get the headers
			 */
			String strLine = brPrices.readLine(); 
			arColumns = strLine.split(",");
			brPrices.close();
		}
		catch (FileNotFoundException e) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"File "+txtFileName+" not Found");
			return;
		}
		catch (IOException e) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"I//O Error whilst reading "+txtFileName);
			return;
		}
		int iTickerItem = 0;
		int iPriceItem = 0;
		for (int i=0;i<arColumns.length;i++) {
			if (arColumns[i].equals(objParms.getTicker()))
				iTickerItem = i;
			cbTicker.addItem(arColumns[i]);
			if (arColumns[i].equals(objParms.getPrice()))
				iPriceItem = i;
			cbPrice.addItem(arColumns[i]);
		}
		cbTicker.setSelectedIndex(iTickerItem+1);
		cbPrice.setSelectedIndex(iPriceItem+1);
		chbExch.setSelected(objParms.getExch());
		chbZero.setSelected(objParms.getZero());
		cbMultiplier.setSelectedIndex(objParms.getDefaultMult());
		listLines = objParms.getLines();
		mapCombos = new HashMap<Object,String>();
		mapDelete = new HashMap<Object,String>();
		buildLines();
		chbExch.revalidate();
		chbZero.revalidate();
		cbTicker.revalidate();
		cbPrice.revalidate();
		cbMultiplier.revalidate();
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.pack();
	}
	private void loadData() {
		if (cbTicker.getSelectedIndex() == cbPrice.getSelectedIndex()) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"Ticker and Price can not be the same field");
			return;
		}
		if (cbTicker.getSelectedIndex() == 0 ||
			cbPrice.getSelectedIndex() == 0	) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"Both Ticker and Price must be selected");
			return;
		}
		
	      //Create and set up the window.
	      JFrame frame = new JFrame("MoneyDance Load Security Prices");
	      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	      objLoadWindow = new loadPricesWindow(txtFileName,objParms);
	      frame.getContentPane().add(objLoadWindow);

	      //Display the window.
	      frame.pack();
	      frame.setLocationRelativeTo(null);
	      frame.setVisible(true);
	      return;
	  }
	  private void addMult() {
			JPanel panInput = new JPanel(new GridBagLayout());
			JLabel lblExch = new JLabel("Enter Exchange (no .)");
			GridBagConstraints gbc_lblExch = new GridBagConstraints();
			gbc_lblExch.insets = new Insets(10, 10, 10, 10);
			gbc_lblExch.gridx = 0;
			gbc_lblExch.gridy = 0;
			panInput.add(lblExch,gbc_lblExch);
			JTextField txtExch = new JTextField();
			txtExch.setColumns(5);
			GridBagConstraints gbc_txtExch = new GridBagConstraints();
			gbc_txtExch.insets = new Insets(10, 10, 10, 10);
			gbc_txtExch.gridx = 1;
			gbc_txtExch.gridy = 0;
			panInput.add(txtExch,gbc_txtExch);
			JLabel lblMult = new JLabel("Multiplier");
			GridBagConstraints gbc_lblMult = new GridBagConstraints();
			gbc_lblMult.insets = new Insets(10, 10, 10, 10);
			gbc_lblMult.gridx = 2;
			gbc_lblMult.gridy = 0;
			panInput.add(lblMult,gbc_lblMult);
			JComboBox<Integer> cbMult = new JComboBox<Integer>(Parameters.arMultipliers);
			GridBagConstraints gbc_cbMult = new GridBagConstraints();
			gbc_cbMult.insets = new Insets(10, 10, 10, 10);
			gbc_cbMult.gridx = 3;
			gbc_cbMult.gridy = 0;
			panInput.add(cbMult,gbc_cbMult);
			while (true) {
				int iResult = JOptionPane.showConfirmDialog(null,  panInput, 
						"Enter Exchange and Multiplier", JOptionPane.OK_CANCEL_OPTION);
				if (iResult == JOptionPane.OK_OPTION){
					String strExch = txtExch.getText();
					if (strExch.equals("")) {
						JOptionPane.showMessageDialog(null, "Exchange can not be blank");
						continue;
					}
					objParms.addExchange(strExch, cbMult.getSelectedIndex());
					break;
				}
				if (iResult == JOptionPane.CANCEL_OPTION)
					break;
			}
			panMult.removeAll();
			buildLines();
			panMult.revalidate();
			JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
			topFrame.pack();
			return;
		
	  }
	  private void buildLines() {
			int iRow = 2;
			mapCombos.clear();
			mapDelete.clear();
			for (ExchangeLine objLine : listLines) {
				JLabel lblExch = new JLabel(objLine.getExchange());
				GridBagConstraints gbc_lblExch = new GridBagConstraints();
				gbc_lblExch.insets = new Insets(10, 10, 10, 10);
				gbc_lblExch.gridx = 1;
				gbc_lblExch.gridy = iRow;
				panMult.add(lblExch, gbc_lblExch);
				JComboBox<Integer> cbMult = new JComboBox<Integer>(Parameters.arMultipliers);
				GridBagConstraints gbc_cbMultiplier = new GridBagConstraints();
				gbc_cbMultiplier.insets = new Insets(10, 10, 10, 10);
				gbc_cbMultiplier.gridx = 2;
				gbc_cbMultiplier.gridy = iRow;
				cbMult.setSelectedIndex(objLine.getMultiplier());
				mapCombos.put(cbMult,objLine.getExchange());
				cbMult.addActionListener(new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						@SuppressWarnings("unchecked")
						JComboBox<Integer> cbMultT = (JComboBox<Integer>) e.getSource();
						String strExch = mapCombos.get(cbMultT);
						objParms.updateLine(strExch, cbMultT.getSelectedIndex());
					}
				});
				panMult.add(cbMult, gbc_cbMultiplier );
				JButton btnDelete = new JButton("Delete");
				GridBagConstraints gbc_btnDelete = new GridBagConstraints();
				gbc_btnDelete.insets = new Insets(10, 10, 10, 10);
				gbc_btnDelete.gridx = 3;
				gbc_btnDelete.gridy = iRow;
				btnDelete.addActionListener(new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						deleteExchange(e);
					}
				});
				panMult.add(btnDelete,gbc_btnDelete);
				mapDelete.put(btnDelete,objLine.getExchange());
				
				iRow++;
			}
			btnAdd = new JButton("Add Exchange Multiplier");
			GridBagConstraints gbc_butAdd= new GridBagConstraints();
			gbc_butAdd.insets = new Insets(10, 10, 10, 10);
			gbc_butAdd.gridx = 0;
			gbc_butAdd.gridy = iRow;
			btnAdd.addActionListener(new ActionListener () {
				@Override
				public void actionPerformed(ActionEvent e) {
					addMult();
				}
			});
			panMult.add(btnAdd, gbc_butAdd);

	  }
	  private void deleteExchange(ActionEvent e){
			String strExch = mapDelete.get(e.getSource());
			objParms.deleteExchange(strExch);
			panMult.removeAll();
			buildLines();
			panMult.revalidate();
			JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
			topFrame.pack();
		  
	  }
	  private void close() {
		  this.setVisible(false);
		  if (objLoadWindow != null)
			  objLoadWindow.close();
		  JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		  topFrame.dispose();		  
	  }
}
