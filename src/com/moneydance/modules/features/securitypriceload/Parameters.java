package com.moneydance.modules.features.securitypriceload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.moneydance.apps.md.model.AccountBook;

public class Parameters implements Serializable{
	/*
	 * Static and transient fields are not stored 
	 */
	private static final long serialVersionUID = 1L;
	private transient AccountBook abCurAcctBook;
	private transient File fiCurFolder;
	private transient FileInputStream fiCurInFile;
	private transient FileOutputStream fiCurOutFile;
	private transient String strFileName;
	public transient static Integer [] arMultipliers = {-4,-3,-2,-1,0,1,2,3,4};
	/*
     * The following fields are stored
     */

	private String strTicker;
	private String strPrice;
	private int iMultiplier;
	private boolean bExch;
	private boolean bZero;
	private List<ExchangeLine> listExchangeLines;
	public Parameters() {
		/*
		 * determine if file already exists
		 */
		abCurAcctBook = Main.context.getRootAccount().getAccountBook();
		fiCurFolder = abCurAcctBook.getRootFolder();
		strFileName = fiCurFolder.getAbsolutePath()+"\\SecurePriceLoad.bpam";
		try {
			fiCurInFile = new FileInputStream(strFileName);
			ObjectInputStream ois = new ObjectInputStream(fiCurInFile);
			/*
			 * file exists, copy temporary object to this object
			 */
			Parameters objTemp = (Parameters) ois.readObject();
			this.strTicker = objTemp.strTicker;
			this.strPrice = objTemp.strPrice;
			this.bExch = objTemp.bExch;
			this.bZero = objTemp.bZero;
			this.iMultiplier = objTemp.iMultiplier;
			this.listExchangeLines = objTemp.listExchangeLines;
			fiCurInFile.close();
		}
		catch (IOException | ClassNotFoundException ioException) {
			/*
			 * file does not exist, initialize fields
			 */
			listExchangeLines = new ArrayList<ExchangeLine>();
			strTicker = "";
			strPrice = "";
			bExch = false;
			bZero = false;
			iMultiplier = 4;
			/*
			 * create the file
			 */
			try {
				fiCurOutFile = new FileOutputStream(strFileName);
				ObjectOutputStream oos = new ObjectOutputStream(fiCurOutFile);
				oos.writeObject(this);
				fiCurOutFile.close();
			}
			catch(IOException i)
			{
				i.printStackTrace();
			}
		}		
	}
	public String getTicker() {
		return strTicker;
	}
	public String getPrice() {
		return strPrice;
	}
	public boolean getExch() {
		return bExch;
	}
	public boolean getZero () {
		return bZero;
	}
	public int getDefaultMult () {
		return iMultiplier;
	}
	public List<ExchangeLine> getMultipliers() {
		return listExchangeLines;
	}
	public void setTicker(String strTickerp) {
		strTicker = strTickerp;
	}
	public void setPrice(String strPricep) {
		strPrice = strPricep;
	}
	public void setExch(boolean bExchp) {
		bExch = bExchp;
	}
	public void setZero (boolean bZerop) {
		bZero = bZerop;
	}
	public void setDefaultMult(int iMultiplierp){
		iMultiplier = iMultiplierp;
	}
	public void addExchange(String strExch, int iMultiplier) {
		ExchangeLine objLine = new ExchangeLine(strExch, iMultiplier);
		if (listExchangeLines == null)
			listExchangeLines = new ArrayList<ExchangeLine>();
		listExchangeLines.add(objLine);
	}
	public int getMultiplier(String strExchange) {
		int iReturn = iMultiplier;
		for (ExchangeLine objLine :listExchangeLines) {
			if (objLine.getExchange().equals(strExchange)){
				iReturn = objLine.getMultiplier();
				break;
			}
		}
		return arMultipliers[iReturn];
	}
	public void updateLine(String strExchange, int iMultiplierp){
		for (ExchangeLine objLine :listExchangeLines) {
			if (objLine.getExchange().equals(strExchange))
				objLine.setMultiplier(iMultiplierp);
		}

	}
	public void deleteExchange(String strExchange){
		for (ExchangeLine objLine :listExchangeLines) {
			if (objLine.getExchange().equals(strExchange)) {
				listExchangeLines.remove(objLine);
				break;
			}
		}

	}
	public List<ExchangeLine> getLines() {
		return listExchangeLines;
	}
	
	public void save() {
		/*
		 * Save the parameters into the specified file
		 */
		try {
			fiCurOutFile = new FileOutputStream(strFileName);
			ObjectOutputStream oos = new ObjectOutputStream(fiCurOutFile);
			oos.writeObject(this);
			oos.close();
			fiCurOutFile.close();
		}
		catch(IOException i)
		{
			i.printStackTrace();
		}
	}

}
