/*
 *  Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.loadsectrans;

import com.moneydance.apps.md.model.Account;

public class GenerateTransaction {
	private char chType;
	private long lAmount;
	private Account acct;
	private String strDesc;
	private String strCheque;
	private String strTType;
	private int iDate;
	private String strRef;
	private int iIndex;
	

	public GenerateTransaction(char chTypep,Account acctp,
			int iDatep, long lAmountp,String strDescp,String strChequep,
			String strTTypep, String strRefp) {
		chType = chTypep;
		acct = acctp;
		iDate = iDatep;
		lAmount = lAmountp;
		strDesc = strDescp;
		strCheque = strChequep;
		strTType = strTTypep;
		strRef = strRefp;
		iIndex = -1;
	}
	
	public int getIndex() {
		return iIndex;
	}
	public char getType() {
		return chType;
	}
	
	public long getAmount() {
		return lAmount;
	}
	
	public Account getAccount() {
		return acct;
	}
	
	public int getDate() {
		return iDate;
	}
	
	public String getDesc() {
		return strDesc;
	}
	
	public String getCheque () {
		return strCheque;
	}

	public String getTType () {
		return strTType;
	}
	public String getRef () {
		return strRef;
	}
	
	public void setIndex(int iIndexp) {
		iIndex = iIndexp;
	}
}
