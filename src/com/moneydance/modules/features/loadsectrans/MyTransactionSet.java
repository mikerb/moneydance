/*
 *  Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.loadsectrans;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.moneydance.apps.md.model.AbstractTxn;
import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.RootAccount;
import com.moneydance.apps.md.model.SearchTxnList;
import com.moneydance.apps.md.model.Txn;
import com.moneydance.apps.md.model.TxnSearch;

public class MyTransactionSet implements TxnSearch{
	private SearchTxnList stlTrans;
	private Account acct;
	private Enumeration<AbstractTxn> enumTrans;
	private Map<Integer,List<AbstractTxn>> mapLines;
	private Parameters objParms;
	public MyTransactionSet(RootAccount root, Account acctp, Parameters objParmsp) {
		acct = acctp;
		objParms = objParmsp;
		stlTrans = new SearchTxnList(root, this);
		/*
		 * Create map of transactions by date
		 */
		enumTrans = stlTrans.getAllTransactions();
		mapLines = new HashMap<Integer, List<AbstractTxn>>();
		while (enumTrans.hasMoreElements()) {
			AbstractTxn txnLine = enumTrans.nextElement();
			if (mapLines.get(txnLine.getDateInt()) == null) {
				List<AbstractTxn> listTxn = new ArrayList<AbstractTxn>();
				listTxn.add(txnLine);
				mapLines.put(txnLine.getDateInt(), listTxn);
			}
			else {
				List<AbstractTxn> listTxn2 = mapLines.get(txnLine.getDateInt());
				listTxn2.add(txnLine);
			}
		}	
	}
	@Override
	public boolean matches(Txn txnParm) {
		if (txnParm.getTag(Constants.TAGGEN) != null) {
			if (txnParm.getParentTxn().getAccount()== acct)
				return true;
		}
		return false;
	}

	@Override
	public boolean matchesAll() {
		return false;
	}
	/*
	 * determine if current line has been generated
	 * 
	 *
	 */
	public void findTransaction (SecLine slTran) {
		/*
		 * check same date
		 */
		List<AbstractTxn> listTxn = mapLines.get(slTran.getDate());
		if (listTxn == null) {
			return;
		}		
		for (AbstractTxn txnLine : listTxn) {
			long lValue = slTran.getValue();
			long lTxnValue = txnLine.getValue();
			lValue = (lValue < 0 ? lValue*-1 : lValue);
			lTxnValue = (lTxnValue < 0 ? lTxnValue*-1 : lTxnValue);
			/*
			 * check the same unsigned value (Moneydance manipulates the amounts)
			 */
			if ( lTxnValue != lValue)
				continue;
			/*
			 * determine if same Transaction Transfer Type
			 */
			if (txnLine.getTag(Constants.TAGGEN) != null)
				if (!(objParms.isDefined(txnLine.getTag(Constants.TAGGEN))))
					continue;
			
			slTran.setProcessed(true);
		}
		return;
	}
	

}
