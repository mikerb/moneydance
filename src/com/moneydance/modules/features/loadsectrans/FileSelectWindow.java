/* 
 *  Copyright (c) 2014, Michael Bray. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   - The name of the author may not used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */ 
package com.moneydance.modules.features.loadsectrans;


import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;


import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.GridBagConstraints;

import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.moneydance.apps.md.model.Account;
import com.moneydance.apps.md.model.CurrencyType;
import com.moneydance.apps.md.model.ExpenseAccount;
import com.moneydance.apps.md.model.IncomeAccount;
import com.moneydance.apps.md.model.InvestmentAccount;
import com.moneydance.apps.md.model.SecurityAccount;
import com.moneydance.modules.features.loadsectrans.Parameters;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class FileSelectWindow extends JPanel{
	  private static final long serialVersionUID = 1L;
	  private JTextField txtFileName;
	  private JButton btnLoad;
	  private JButton btnChoose;
	  private JButton btnAdd;
	  private JComboBox<String> cbAccounts;
	  private JComboBox<String> cbTicker;
	  private JComboBox<String> cbDate;
	  private JComboBox<String> cbValue;
	  private JComboBox<String> cbReference;
	  private JComboBox<String> cbDesc;
	  private JCheckBox chbExch;
	  JScrollPane spFields;
	  private JPanel panFields;
	  private Parameters objParms;
	  private JFileChooser objFileChooser = null;
	  private File fSecurities;
	  private loadPricesWindow objLoadWindow;
	  private Map<String,Account> mapNames = new HashMap<String,Account>(); 
	  private String[] arrNames;
	  private LookAndFeel previousLF;

	  private String [] arColumns;
	  private List<FieldLine> listLines;
	  private Map<Object,String> mapCombos;
	  private List<String> listCategories;
	  private String [] arrCategories;
	  private Map<String,Account> mapAccts;
	  private String strName = "";
	public FileSelectWindow() throws HeadlessException {
	    previousLF = UIManager.getLookAndFeel();
	    try {
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException | ClassNotFoundException e) {}
	    listCategories = new ArrayList<String>();
	    mapAccts = new HashMap<String,Account>();
	    mapNames.put("Please Select an Account", null);
		loadAccounts(Main.context.getRootAccount(),strName);
		arrCategories = listCategories.toArray(new String[0]);
		objFileChooser = new JFileChooser();
		arrNames = mapNames.keySet().toArray(new String[0]);
		GridBagLayout gbl_panel = new GridBagLayout();
		this.setLayout(gbl_panel);
		
		JLabel lblFileName = new JLabel("File : ");
		GridBagConstraints gbc_lblFileName = new GridBagConstraints();
		gbc_lblFileName.insets = new Insets(20, 20, 0, 0);
		gbc_lblFileName.gridx = 0;
		gbc_lblFileName.gridy = 0;
		this.add(lblFileName, gbc_lblFileName);
		
		txtFileName = new JTextField();
		txtFileName.setText("");
		GridBagConstraints gbc_txtFileName = new GridBagConstraints();
		gbc_txtFileName.insets = new Insets(20,5, 0, 0);
		gbc_txtFileName.gridx = gbc_lblFileName.gridx+1;
		gbc_txtFileName.gridy = gbc_lblFileName.gridy;
		gbc_txtFileName.gridwidth=3;
		txtFileName.setColumns(50);
		this.add(txtFileName, gbc_txtFileName);
	
		btnChoose = new JButton();
	    Image img = getIcon("Search-Folder-icon.jpg");
	    if (img == null)
	    	btnChoose.setText("Find");
	    else
	    	btnChoose.setIcon(new ImageIcon(img));
		GridBagConstraints gbc_btnChoose = new GridBagConstraints();
		gbc_btnChoose.insets = new Insets(10, 5, 0, 0);
		gbc_btnChoose.gridx = gbc_txtFileName.gridx+gbc_txtFileName.gridwidth;
		gbc_btnChoose.gridy = gbc_txtFileName.gridy;
		this.add(btnChoose, gbc_btnChoose);
		btnChoose.setBorder(javax.swing.BorderFactory.createLineBorder(this.getBackground()));     
		btnChoose.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				chooseFile();
			}
		});
				
				
			
		JLabel lblAccounts = new JLabel("Which Investment Account : ");
		GridBagConstraints gbc_lblAccounts = new GridBagConstraints();
		gbc_lblAccounts.insets = new Insets(10, 0, 0, 0);
		gbc_lblAccounts.gridx = gbc_lblFileName.gridx+1;
		gbc_lblAccounts.gridy = gbc_lblFileName.gridy+1;
		this.add(lblAccounts, gbc_lblAccounts);

		cbAccounts = new JComboBox<String>(arrNames);
		GridBagConstraints gbc_boxAccounts = new GridBagConstraints();
		gbc_boxAccounts.insets = new Insets(5, 0, 0, 0);
		gbc_boxAccounts.gridx = gbc_lblAccounts.gridx+1;
		gbc_boxAccounts.gridy = gbc_lblAccounts.gridy;
		this.add(cbAccounts, gbc_boxAccounts);
		
		JLabel lblLReference = new JLabel("Select Transaction Type Field");		
		GridBagConstraints gbc_lblLReference = new GridBagConstraints();
		gbc_lblLReference.insets = new Insets(5,5,5,5);
		gbc_lblLReference.gridx = gbc_lblAccounts.gridx;
		gbc_lblLReference.gridy = gbc_lblAccounts.gridy+1;
		this.add(lblLReference, gbc_lblLReference);	
		cbReference = new JComboBox<String>();
		GridBagConstraints gbc_cbLReference = new GridBagConstraints();
		gbc_cbLReference.insets = new Insets(5,5,5,5);
		gbc_cbLReference.gridx = gbc_lblLReference.gridx+1;
		gbc_cbLReference.gridy = gbc_lblLReference.gridy;
		gbc_cbLReference.anchor = GridBagConstraints.LINE_START;
		cbReference.addItem("Please Select a Field");
		cbReference.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbReferenceT = (JComboBox<Integer>) e.getSource();
				objParms.setReference((String)cbReferenceT.getSelectedItem());
			}
		});
		this.add(cbReference, gbc_cbLReference);
		JLabel lblLTicker = new JLabel("Select Ticker Field");		
		GridBagConstraints gbc_lblLTicker = new GridBagConstraints();
		gbc_lblLTicker.insets = new Insets(5,5,5,5);
		gbc_lblLTicker.gridx = gbc_lblLReference.gridx;
		gbc_lblLTicker.gridy = gbc_lblLReference.gridy+1;
		this.add(lblLTicker, gbc_lblLTicker);	
		cbTicker = new JComboBox<String>();
		GridBagConstraints gbc_cbTicker = new GridBagConstraints();
		gbc_cbTicker.insets = new Insets(5,5,5,5);
		gbc_cbTicker.gridx = gbc_lblLTicker.gridx+1;
		gbc_cbTicker.gridy = gbc_lblLTicker.gridy;
		gbc_cbTicker.anchor = GridBagConstraints.LINE_START;
		cbTicker.addItem("Please Select a Field");
		cbTicker.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbTick = (JComboBox<Integer>) e.getSource();
				objParms.setTicker((String)cbTick.getSelectedItem());
			}
		});
		this.add(cbTicker, gbc_cbTicker);	
		JLabel lblLExch = new JLabel("Remove Exchange from Ticker?");
		GridBagConstraints gbc_cbLExch = new GridBagConstraints();
		gbc_cbLExch.insets = new Insets(5, 5, 5,0);
		gbc_cbLExch.gridx =gbc_cbTicker.gridx+1;
		gbc_cbLExch.gridy = gbc_cbTicker.gridy;
		gbc_cbLExch.anchor = GridBagConstraints.LINE_END;
		this.add(lblLExch, gbc_cbLExch );	
		chbExch = new JCheckBox();
		GridBagConstraints gbc_chbExch = new GridBagConstraints();
		gbc_chbExch.insets = new Insets(5, 0, 5, 5);
		gbc_chbExch.gridx = gbc_cbLExch.gridx+1;
		gbc_chbExch.gridy = gbc_cbLExch.gridy;
		gbc_chbExch.anchor = GridBagConstraints.LINE_START;
		chbExch.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox chbExchT = (JCheckBox) e.getSource();
				objParms.setExch(chbExchT.isSelected());
			}
		});
		this.add(chbExch, gbc_chbExch);	
		JLabel lblLDate = new JLabel("Select Date Field");		
		GridBagConstraints gbc_lblLDate = new GridBagConstraints();
		gbc_lblLDate.insets = new Insets(5,5,5,5);
		gbc_lblLDate.gridx = gbc_lblLTicker.gridx;
		gbc_lblLDate.gridy = gbc_lblLTicker.gridy+1;
		this.add(lblLDate, gbc_lblLDate);	
		cbDate = new JComboBox<String>();
		GridBagConstraints gbc_cbLDate = new GridBagConstraints();
		gbc_cbLDate.insets = new Insets(5,5,5,5);
		gbc_cbLDate.gridx = gbc_lblLDate.gridx+1;
		gbc_cbLDate.gridy = gbc_lblLDate.gridy;
		gbc_cbLDate.anchor = GridBagConstraints.LINE_START;
		cbDate.addItem("Please Select a Field");
		cbDate.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbDateT = (JComboBox<Integer>) e.getSource();
				objParms.setDate((String)cbDateT.getSelectedItem());
			}
		});
		this.add(cbDate, gbc_cbLDate);	
		JLabel lblLValue = new JLabel("Select Value Field");		
		GridBagConstraints gbc_lblLValue = new GridBagConstraints();
		gbc_lblLValue.insets = new Insets(5,5,5,5);
		gbc_lblLValue.gridx = gbc_lblLDate.gridx;
		gbc_lblLValue.gridy = gbc_lblLDate.gridy+1;
		this.add(lblLValue, gbc_lblLValue);	
		cbValue = new JComboBox<String>();
		GridBagConstraints gbc_cbLValue = new GridBagConstraints();
		gbc_cbLValue.insets = new Insets(5,5,5,5);
		gbc_cbLValue.gridx = gbc_lblLValue.gridx+1;
		gbc_cbLValue.gridy = gbc_lblLValue.gridy;
		gbc_cbLValue.anchor = GridBagConstraints.LINE_START;
		cbValue.addItem("Please Select a Field");
		cbValue.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbValueT = (JComboBox<Integer>) e.getSource();
				objParms.setValue((String)cbValueT.getSelectedItem());
			}
		});
		this.add(cbValue, gbc_cbLValue);	

		JLabel lblLDesc = new JLabel("Select Description Field");		
		GridBagConstraints gbc_lblLDesc = new GridBagConstraints();
		gbc_lblLDesc.insets = new Insets(5,5,5,5);
		gbc_lblLDesc.gridx = gbc_lblLValue.gridx;
		gbc_lblLDesc.gridy = gbc_lblLValue.gridy+1;
		this.add(lblLDesc, gbc_lblLDesc);	
		cbDesc = new JComboBox<String>();
		GridBagConstraints gbc_cbLDesc = new GridBagConstraints();
		gbc_cbLDesc.insets = new Insets(5,5,5,5);
		gbc_cbLDesc.gridx = gbc_lblLDesc.gridx+1;
		gbc_cbLDesc.gridy = gbc_lblLDesc.gridy;
		gbc_cbLDesc.anchor = GridBagConstraints.LINE_START;
		cbDesc.addItem("Please Select a Field");
		cbDesc.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				@SuppressWarnings("unchecked")
				JComboBox<Integer> cbDescT = (JComboBox<Integer>) e.getSource();
				objParms.setDesc((String)cbDescT.getSelectedItem());
			}
		});
		this.add(cbDesc, gbc_cbLDesc);	
		JLabel lblTransType = new JLabel("Transaction Types");
		GridBagConstraints gbc_lblTransType = new GridBagConstraints();
		gbc_lblTransType.insets = new Insets(5, 5, 5, 5);
		gbc_lblTransType.gridx =gbc_lblLDesc.gridx;
		gbc_lblTransType.gridy = gbc_lblLDesc.gridy +1;
		this.add(lblTransType, gbc_lblTransType);	
		JLabel lblTrantxt = new JLabel("Add one entry for each type of transaction to generate");
		GridBagConstraints gbc_lblLTrantxt = new GridBagConstraints();
		gbc_lblLTrantxt.insets = new Insets(5, 5, 5, 5);
		gbc_lblLTrantxt.gridx =gbc_lblTransType.gridx+1;
		gbc_lblLTrantxt.gridy = gbc_lblTransType.gridy;
		this.add(lblTrantxt, gbc_lblLTrantxt);	
		panFields = new JPanel(new GridBagLayout());
		panFields.setPreferredSize(new Dimension(800,100));
		panFields.setAutoscrolls(true);
		spFields = new JScrollPane(panFields);
		spFields.setPreferredSize(new Dimension(800,100));
		GridBagConstraints gbc_panFields = new GridBagConstraints();
		gbc_panFields.insets = new Insets(5, 5, 5, 5);
		gbc_panFields.gridx = gbc_lblTransType.gridx;
		gbc_panFields.gridy = gbc_lblTransType.gridy+1;
		gbc_panFields.gridwidth=5;
		this.add(spFields,gbc_panFields);

		btnLoad = new JButton("Load");
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.insets = new Insets(5, 5, 5, 5);
		gbc_btnLoad.gridx = gbc_panFields.gridx;
		gbc_btnLoad.gridy = gbc_panFields.gridy+1;
		this.add(btnLoad, gbc_btnLoad);
		btnLoad.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadSecurities();
			}
		});
		
		JButton btnSave = new JButton("Save Parameters");
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.insets = new Insets(5, 5, 5, 5);
		gbc_btnSave.gridx = gbc_btnLoad.gridx+1;
		gbc_btnSave.gridy = gbc_btnLoad.gridy;
		this.add(btnSave, gbc_btnSave);
		btnSave.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				objParms.save();
			}
		});

		
		
		JButton btnClose = new JButton("Close");
		GridBagConstraints gbc_btnClose = new GridBagConstraints();
		gbc_btnClose.insets = new Insets(5,5,5,5);
		gbc_btnClose.gridx = gbc_btnSave.gridx +1;
		gbc_btnClose.gridy = gbc_btnSave.gridy;
		this.add(btnClose, gbc_btnClose);
		btnClose.addActionListener(new ActionListener () {
			@Override
			public void actionPerformed(ActionEvent e) {
				close();
			}
		});
	}
	private void chooseFile() {
		objFileChooser.setFileFilter(new FileNameExtensionFilter("csv","CSV"));
		int iReturn = objFileChooser.showDialog(this, "Select File");
		if (iReturn == JFileChooser.APPROVE_OPTION) {
			fSecurities = objFileChooser.getSelectedFile();
			txtFileName.setText(fSecurities.getAbsolutePath());
		}
		objParms = new Parameters();
		for (FieldLine objLine:objParms.getLines()){
			objLine.setAccountObject();
		}
		loadFields();
	}
	private void loadFields() {
		FileReader frPrices;
		BufferedReader brPrices;
		if (txtFileName.getText().equals("")) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"Please Select a file first");
			return;
		}
		try {
			frPrices = new FileReader(txtFileName.getText());
			brPrices = new BufferedReader(frPrices);
			/*
			 * Get the headers
			 */
			String strLine = brPrices.readLine(); 
			arColumns = strLine.split(",");
			brPrices.close();
		}
		catch (FileNotFoundException e) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"File "+txtFileName+" not Found");
			return;
		}
		catch (IOException e) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"I//O Error whilst reading "+txtFileName);
			return;
		}
		int iTickerItem = -1;
		int iDateItem = -1;
		int iReferenceItem = -1;
		int iDescItem = -1;
		int iValueItem = -1;
		for (int i=0;i<arColumns.length;i++) {
			if (arColumns[i].equals(objParms.getTicker()))
				iTickerItem = i;
			cbTicker.addItem(arColumns[i]);
			if (arColumns[i].equals(objParms.getValue()))
				iValueItem = i;
			cbValue.addItem(arColumns[i]);
			if (arColumns[i].equals(objParms.getDate()))
				iDateItem = i;
			cbDate.addItem(arColumns[i]);
			if (arColumns[i].equals(objParms.getReference()))
				iReferenceItem = i;
			cbReference.addItem(arColumns[i]);
			if (arColumns[i].equals(objParms.getDesc()))
				iDescItem = i;
			cbDesc.addItem(arColumns[i]);
		}
		cbTicker.setSelectedIndex(iTickerItem == -1?0:iTickerItem+1);
		cbValue.setSelectedIndex(iValueItem == -1?0:iValueItem+1);
		cbDate.setSelectedIndex(iDateItem == -1?0:iDateItem+1);
		cbReference.setSelectedIndex(iReferenceItem == -1?0:iReferenceItem+1);
		cbDesc.setSelectedIndex(iDescItem == -1?0:iDescItem+1);
		chbExch.setSelected(objParms.getExch());
		listLines = objParms.getLines();
		mapCombos = new HashMap<Object,String>();
		buildLines();
		chbExch.revalidate();
		cbTicker.revalidate();
		cbValue.revalidate();
		cbDate.revalidate();
		cbReference.revalidate();
		cbDesc.revalidate();
		panFields.revalidate();
		spFields.revalidate();
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.pack();
	}
	  private void buildLines() {
			int iRow = 1;
			mapCombos.clear();
			JLabel lblRTypeH = new JLabel("Transaction Type");
			GridBagConstraints gbc_lblRTypeH = new GridBagConstraints();
			gbc_lblRTypeH.insets = new Insets(5,5,5,5);
			gbc_lblRTypeH.gridx = 0;
			gbc_lblRTypeH.gridy = iRow;
			gbc_lblRTypeH.gridwidth = 2;
			panFields.add(lblRTypeH, gbc_lblRTypeH);
			JLabel lblTTypeH = new JLabel("Investment Type");
			GridBagConstraints gbc_lblTTypeH = new GridBagConstraints();
			gbc_lblTTypeH.insets = new Insets(5,5,5,5);
			gbc_lblTTypeH.gridx = 2;
			gbc_lblTTypeH.gridy = iRow;
			panFields.add(lblTTypeH, gbc_lblTTypeH);
			JLabel lblAcctH = new JLabel("Category");
			GridBagConstraints gbc_lblAcctH = new GridBagConstraints();
			gbc_lblAcctH.insets = new Insets(5,5,5,5);
			gbc_lblAcctH.gridx = 3;
			gbc_lblAcctH.gridy = iRow;
			panFields.add(lblAcctH, gbc_lblAcctH);
			iRow++;
			int iHeight = 100;
			for (FieldLine objLine : listLines) {
				iHeight+=50;
				JLabel lblType = new JLabel(objLine.getType());
				GridBagConstraints gbc_lblType = new GridBagConstraints();
				gbc_lblType.insets = new Insets(5,5,5,5);
				gbc_lblType.gridx = 0;
				gbc_lblType.gridy = iRow;
				panFields.add(lblType, gbc_lblType);
				JButton btnChange = new JButton("Chg");
				GridBagConstraints gbc_btnChange = new GridBagConstraints();
				gbc_btnChange.insets = new Insets(5,5,5,5);
				gbc_btnChange.gridx = 1;
				gbc_btnChange.gridy = iRow;
				mapCombos.put(btnChange,objLine.getType());
				panFields.add(btnChange, gbc_btnChange);
				btnChange.addActionListener(new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						JButton btnTemp = (JButton) e.getSource();
						changeTransType(mapCombos.get(btnTemp));
					}
				});
				JComboBox<String> cbTType = new JComboBox<String>(Constants.TRANSTYPES);
				GridBagConstraints gbc_cbTType = new GridBagConstraints();
				gbc_cbTType.insets = new Insets(10, 10, 10, 10);
				gbc_cbTType.gridx = 2;
				gbc_cbTType.gridy = iRow;
				cbTType.setSelectedIndex(objLine.getTranType());
				mapCombos.put(cbTType,objLine.getType());
				cbTType.addActionListener(new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						@SuppressWarnings("unchecked")
						JComboBox<String> cbTTypeT = (JComboBox<String>) e.getSource();
						String strType = mapCombos.get(cbTTypeT);
						objParms.updateTransType(strType, cbTTypeT.getSelectedIndex());
					}
				});
				panFields.add(cbTType, gbc_cbTType );
				JComboBox<String> cbAcct = new JComboBox<String>(arrCategories);
				GridBagConstraints gbc_cbAcct = new GridBagConstraints();
				gbc_cbAcct.insets = new Insets(10, 10, 10, 10);
				gbc_cbAcct.gridx = 3;
				gbc_cbAcct.gridy = iRow;
				cbAcct.setSelectedItem(objLine.getAccountName());
				mapCombos.put(cbAcct,objLine.getType());
				cbAcct.addActionListener(new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						@SuppressWarnings("unchecked")
						JComboBox<String> cbAcctT = (JComboBox<String>) e.getSource();
						String strType = mapCombos.get(cbAcctT);
						String strAcct = (String) cbAcctT.getSelectedItem();
						objParms.updateAccount(strType, strAcct, mapAccts.get(strAcct));
					}
				});
				panFields.add(cbAcct, gbc_cbAcct );	
				
				JButton btnDelete = new JButton("Delete");
				GridBagConstraints gbc_btnDelete = new GridBagConstraints();
				gbc_btnDelete.insets = new Insets(10, 10, 10, 10);
				gbc_btnDelete.gridx = 4;
				gbc_btnDelete.gridy = iRow;
				btnDelete.addActionListener(new ActionListener () {
					@Override
					public void actionPerformed(ActionEvent e) {
						deleteTransType(e);
					}
				});
				panFields.add(btnDelete,gbc_btnDelete);
				mapCombos.put(btnDelete,objLine.getType());
				
				iRow++;
			}
			btnAdd = new JButton("Add Transaction Type");
			GridBagConstraints gbc_butAdd= new GridBagConstraints();
			gbc_butAdd.insets = new Insets(10, 10, 10, 10);
			gbc_butAdd.gridx = 0;
			gbc_butAdd.gridy = iRow;
			btnAdd.addActionListener(new ActionListener () {
				@Override
				public void actionPerformed(ActionEvent e) {
					addTran();
				}
			});
			panFields.add(btnAdd, gbc_butAdd);
			panFields.revalidate();
			panFields.setPreferredSize(new Dimension(800,iHeight));
			if (iHeight>400)
				iHeight = 300;
			spFields.setPreferredSize(new Dimension(800,iHeight));

	  }
	  private void deleteTransType(ActionEvent e){
			String strType = mapCombos.get(e.getSource());
			objParms.deleteField(strType);
			panFields.removeAll();
			buildLines();
			panFields.revalidate();
			spFields.revalidate();
			JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
			topFrame.pack();
		  
	  }
	  private void changeTransType (String strType) {
		  	displayTran(strType);
	  }
	  private void addTran() {
		  	displayTran(Constants.NEWTRAN);
	  }
	  private void displayTran(String strTran) {
		    FieldLine objCurrent = null;
			JPanel panInput = new JPanel(new GridBagLayout());
			JLabel lblType = new JLabel("Enter Transaction Type");
			GridBagConstraints gbc_lblTran = new GridBagConstraints();
			gbc_lblTran.insets = new Insets(10, 10, 10, 10);
			gbc_lblTran.gridx = 0;
			gbc_lblTran.gridy = 0;
			panInput.add(lblType,gbc_lblTran);
			JTextField txtType = new JTextField();
			txtType.setColumns(20);
			GridBagConstraints gbc_txtType = new GridBagConstraints();
			gbc_txtType.insets = new Insets(10, 10, 10, 10);
			gbc_txtType.gridx = 1;
			gbc_txtType.gridy = 0;
			panInput.add(txtType,gbc_txtType);
			JLabel lblMult = new JLabel("Type");
			GridBagConstraints gbc_lblMult = new GridBagConstraints();
			gbc_lblMult.insets = new Insets(10, 10, 10, 10);
			gbc_lblMult.gridx = 2;
			gbc_lblMult.gridy = 0;
			panInput.add(lblMult,gbc_lblMult);
			JComboBox<String> cbTType = new JComboBox<String>(Constants.TRANSTYPES);
			GridBagConstraints gbc_cbTType = new GridBagConstraints();
			gbc_cbTType.insets = new Insets(10, 10, 10, 10);
			gbc_cbTType.gridx = 3;
			gbc_cbTType.gridy = 0;
			panInput.add(cbTType,gbc_cbTType);
			JComboBox<String> cbAcct = new JComboBox<String>(arrCategories);
			GridBagConstraints gbc_cbAcct = new GridBagConstraints();
			gbc_cbAcct.insets = new Insets(10, 10, 10, 10);
			gbc_cbAcct.gridx =4;
			gbc_cbAcct.gridy = 0;
			panInput.add(cbAcct,gbc_cbAcct);
		  	if (!strTran.equals(Constants.NEWTRAN)) {
		  		objCurrent = objParms.matchType(strTran);
		  		if (objCurrent != null) {
		  			txtType.setText(objCurrent.getType());
		  			cbTType.setSelectedIndex(objCurrent.getTranType());
		  			cbAcct.setSelectedItem(objCurrent.getAccountName());
		  		}
		  	}
		  	while (true) {
				int iResult = JOptionPane.showConfirmDialog(null,  panInput, 
						"Enter Exchange and Multiplier", JOptionPane.OK_CANCEL_OPTION);
				if (iResult == JOptionPane.OK_OPTION){
					String strType = txtType.getText();
					if (strType.equals("")) {
						JOptionPane.showMessageDialog(null, "Transaction Type can not be blank");
						continue;
					}
					FieldLine objTempLine = objParms.matchType(strType);
					if (objTempLine != null) {
						if (objCurrent != null && objCurrent != objTempLine)
							JOptionPane.showMessageDialog(null, "Transaction Type already defined");
							continue;
					}
					if (objCurrent != null) {
						String strOldType = objCurrent.getType();
						objCurrent.setType(strType);
						objCurrent.setTranType(cbTType.getSelectedIndex());
						objCurrent.setAccount((String)cbAcct.getSelectedItem(),
							mapAccts.get(cbAcct.getSelectedItem()));
						for (Map.Entry<Object,String> entry : mapCombos.entrySet()){
							String strValue = entry.getValue();
							if (strValue.equals(strOldType))
								entry.setValue(strType);
						}
					}
					else
						objParms.addField(strType,
							(String)cbAcct.getSelectedItem(),
							mapAccts.get(cbAcct.getSelectedItem()),
							cbTType.getSelectedIndex());
					break;
				}
				if (iResult == JOptionPane.CANCEL_OPTION)
					break;
			}
			panFields.removeAll();
			buildLines();
			panFields.revalidate();
			spFields.revalidate();
			JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
			topFrame.pack();
			return;
		
	  }
	  private void loadSecurities() {
		if (txtFileName.getText().equals("")) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"Please Select a file first");
			return;
		}
		if (cbReference.getSelectedIndex()==0 ||
			cbTicker.getSelectedIndex() == 0 ||
			cbDate.getSelectedIndex() == 0 ||
			cbValue.getSelectedIndex() == 0 ||
			cbDesc.getSelectedIndex() == 0) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"All fields must be selected");
			return;			
		}
		int [] iSelected = new int[arColumns.length];
		for (int i=0;i<iSelected.length;i++)
			iSelected[i] = 0;
		iSelected[cbReference.getSelectedIndex()] = 1;
		if (iSelected[cbTicker.getSelectedIndex()] != 0) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No field can be selected twice");
			return;	
		}
		else
			iSelected[cbTicker.getSelectedIndex()]= 1;
		if (iSelected[cbDate.getSelectedIndex()] != 0) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No field can be selected twice");
			return;	
		}
		else
			iSelected[cbDate.getSelectedIndex()]= 1;
		if (iSelected[cbDesc.getSelectedIndex()] != 0) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No field can be selected twice");
			return;	
		}
		else
			iSelected[cbDesc.getSelectedIndex()]= 1;
		if (iSelected[cbValue.getSelectedIndex()] != 0) {
			JFrame fTemp = new JFrame();
			JOptionPane.showMessageDialog(fTemp,"No field can be selected twice");
			return;	
		}
		else
			iSelected[cbValue.getSelectedIndex()]= 1;
	     //Create and set up the window.
	    JFrame frame = new JFrame("MoneyDance Load Security Transactions");
	    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    Account acct = mapNames.get((String)cbAccounts.getSelectedItem());
	    loadTickers(acct);
	    objLoadWindow = new loadPricesWindow(txtFileName,acct,objParms);
	    frame.getContentPane().add(objLoadWindow);

	    //Display the window.
	    frame.pack();
	    frame.setLocationRelativeTo(null);
	    frame.setVisible(true);

	  }
	  private void close() {
		  this.setVisible(false);
		  if (objLoadWindow != null)
			  objLoadWindow.close();
		  JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		  topFrame.dispose();
		    try {
		          UIManager.setLookAndFeel(previousLF);
		    } catch (UnsupportedLookAndFeelException e) {}
		  
	  }
		/*
		 * Create an array of Investment Accounts for combo box
		 */
	  private void loadAccounts(Account parentAcct,String strName) {
		    int sz = parentAcct.getSubAccountCount();
		    for(int i=0; i<sz; i++) {
		      Account acct = parentAcct.getSubAccount(i);
		      if (acct instanceof InvestmentAccount){
		    	  mapNames.put(acct.getAccountName(),acct);
		      }
		      String strEntry = strName.equals("")?acct.getAccountName():strName+":"+acct.getAccountName();
		      if (acct instanceof ExpenseAccount ||
		    	  acct instanceof IncomeAccount) {
		    	  listCategories.add(strEntry);
		    	  mapAccts.put(strEntry, acct);
		      }
		      loadAccounts(acct,strEntry);
		    }
	  }
	  private Image getIcon(String icon) {
	     try {
		      ClassLoader cl = getClass().getClassLoader();
		      java.io.InputStream in = 
		        cl.getResourceAsStream("/com/moneydance/modules/features/loadsectrans/"+icon);
		      if (in != null) {
		        ByteArrayOutputStream bout = new ByteArrayOutputStream(1000);
		        byte buf[] = new byte[256];
		        int n = 0;
		        while((n=in.read(buf, 0, buf.length))>=0)
		          bout.write(buf, 0, n);
		        return Toolkit.getDefaultToolkit().createImage(bout.toByteArray());
		      }
		    } 
	     catch (Throwable e) {} 
   		return null;
	  }
	/*
	 * Create table of Securities keyed by Ticker
	 */
 	  private void loadTickers(Account parentAcct) {
 		int sz = parentAcct.getSubAccountCount();
        for(int i=0; i<sz; i++) {
        	Account acct = parentAcct.getSubAccount(i);
        	if (acct instanceof SecurityAccount){
        		CurrencyType ctTicker = acct.getCurrencyType();
        		if (ctTicker != null) {
        			if (!ctTicker.getTickerSymbol().equals("")) {
        				Main.mapAccounts.put(ctTicker.getTickerSymbol(), acct);
        			}
        		}
        	}
            loadTickers(acct);
        }
    }
 }